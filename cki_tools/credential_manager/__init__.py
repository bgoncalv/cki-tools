"""Credential management."""

import argparse

from cki_lib import logger

from . import aws
from . import dogtag
from . import gitlab
from . import ldap
from . import metrics
from . import password
from . import splunk
from . import ssh

LOGGER = logger.get_logger('cki_tools.credentials_manager')


def main(args: list[str] | None = None) -> None:
    """Run main loop."""
    parser = argparse.ArgumentParser(description='Process credentials')

    subparsers = parser.add_subparsers(dest='service')

    parser_gitlab = subparsers.add_parser('gitlab')
    parser_gitlab.set_defaults(func=lambda a: gitlab.process(a.action, a.token, a.dry_run, a.force))
    parser_gitlab.add_argument('action', choices=[
        'create', 'destroy',
        'update', 'validate',
        'stats', 'rotate',
    ], help='What to do')
    parser_gitlab.add_argument('--token', help='Token name for create/rotate')
    parser_gitlab.add_argument('--dry-run', action='store_true',
                               help='Do not modify secrets or create tokens during rotation')
    parser_gitlab.add_argument('--force', action='store_true',
                               help='Force token rotation even if new enough')

    parser_ldap = subparsers.add_parser('ldap')
    parser_ldap.set_defaults(func=lambda a: ldap.process(a.action, a.token))
    parser_ldap.add_argument('action', choices=['update', 'validate'],
                             help='What to do')
    parser_ldap.add_argument('--token', help='Only operate on a single token')

    parser_aws = subparsers.add_parser('aws')
    parser_aws.set_defaults(func=lambda a: aws.process(a.action, a.token))
    parser_aws.add_argument('action', choices=['update'],
                            help='What to do')
    parser_aws.add_argument('--token', help='Only operate on a single token')

    parser_dogtag = subparsers.add_parser('dogtag')
    parser_dogtag.set_defaults(func=lambda a: dogtag.process(a.action, a.token, a.dry_run, a.force))
    parser_dogtag.add_argument('action', choices=['create', 'update', 'validate', 'rotate'],
                               help='What to do')
    parser_dogtag.add_argument('--token', help='Only operate on a single token')
    parser_dogtag.add_argument('--dry-run', action='store_true',
                               help='Do not modify secrets or create tokens during rotation')
    parser_dogtag.add_argument('--force', action='store_true',
                               help='Force token rotation even if new enough')

    parser_splunk = subparsers.add_parser('splunk')
    parser_splunk.set_defaults(func=lambda a: splunk.process(a.action, a.token))
    parser_splunk.add_argument('action', choices=['validate'],
                               help='What to do')
    parser_splunk.add_argument('--token', help='Only operate on a single token')

    parser_password = subparsers.add_parser('password')
    parser_password.set_defaults(func=lambda a: password.process(
        a.action, a.token, a.dry_run, a.force))
    parser_password.add_argument('action', choices=['create', 'validate', 'rotate'],
                                 help='What to do')
    parser_password.add_argument('--token', help='Only operate on a single token')
    parser_password.add_argument('--dry-run', action='store_true',
                                 help='Do not modify secrets or create tokens during rotation')
    parser_password.add_argument('--force', action='store_true',
                                 help='Force token rotation even if new enough')

    parser_ssh = subparsers.add_parser('ssh')
    parser_ssh.set_defaults(func=lambda a: ssh.process(a.action, a.token, a.dry_run, a.force))
    parser_ssh.add_argument('action', choices=['create', 'update', 'validate', 'rotate'],
                            help='What to do')
    parser_ssh.add_argument('--token', help='Only operate on a single token')
    parser_ssh.add_argument('--dry-run', action='store_true',
                            help='Do not modify secrets or create tokens during rotation')
    parser_ssh.add_argument('--force', action='store_true',
                            help='Force token rotation even if new enough')

    parser_metrics = subparsers.add_parser('metrics')
    parser_metrics.set_defaults(func=lambda _: metrics.process())

    parsed_args = parser.parse_args(args)
    if result := parsed_args.func(parsed_args):
        print(result.strip())


if __name__ == '__main__':
    main()
