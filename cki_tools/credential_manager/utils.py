"""Various utility methods."""

from collections import abc
import datetime
import os
import typing

from cki_lib import misc
from cki_lib import yaml

DEFAULT_DELTA = datetime.timedelta(days=30)
DEFAULT_INTERVAL = datetime.timedelta(days=365)


def all_tokens(token_types: abc.Container[str]) -> list[str]:
    """Return names of secrets for given token types."""
    return [
        k for k, v in yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE')).items()
        if misc.get_nested_key(v, 'meta/token_type') in token_types
    ]


def tz(token_data: dict[str, typing.Any], field: str) -> datetime.datetime | None:
    """Return a timestamp for the given field."""
    timestamp = misc.get_nested_key(token_data, f'meta/{field}')
    return misc.datetime_fromisoformat_tz_utc(timestamp) if timestamp else None


def too_old(
    timestamp: datetime.datetime | None,
    interval: datetime.timedelta = datetime.timedelta(0),
) -> bool:
    """Check if the token is too old."""
    if not timestamp:
        return False
    return timestamp + interval < misc.now_tz_utc() + DEFAULT_DELTA
