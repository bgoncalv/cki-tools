"""SSH key management."""
import collections
import os
import sys

from cki_lib import misc
from cki_lib import yaml
from cki_lib.logger import get_logger
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa

from cki.deployment_tools import secrets

from . import utils

LOGGER = get_logger(__name__)

MIN_KEY_SIZE = 4096


def update(single_token: str) -> None:
    """Update the meta information."""
    for token in misc.flattened(single_token or utils.all_tokens({'ssh_private_key'})):
        LOGGER.info('Processing %s', token)
        token_secret = secrets.secret(f'{token}:')
        meta = secrets.secret(f'{token}#')
        private_key = serialization.load_ssh_private_key(
            token_secret['private_key'].encode('ascii'), None)
        public_bytes = private_key.public_key().public_bytes(
            serialization.Encoding.OpenSSH,
            serialization.PublicFormat.OpenSSH
        ).decode('ascii').strip() + ' ' + meta['comment']
        meta.update({
            'key_size': private_key.key_size,
        })
        secrets.edit(f'{token}#', meta)
        if public_bytes != token_secret.get('public_key'):
            secrets.edit(f'{token}:public_key', public_bytes)


def create(token: str) -> None:
    """Create an SSH key."""
    if not token:
        print('create requires --token', file=sys.stderr)
        return

    LOGGER.info('Processing %s', token)
    meta = secrets.secret(f'{token}#')
    if (key_size := meta['key_size']) < MIN_KEY_SIZE:
        raise Exception(f'Key size of {token} too small: {key_size} < {MIN_KEY_SIZE}')
    private_key = rsa.generate_private_key(65537, key_size)
    secrets.edit(f'{token}:private_key', private_key.private_bytes(
        serialization.Encoding.PEM,
        serialization.PrivateFormat.OpenSSH,
        serialization.NoEncryption()
    ).decode('ascii').strip())
    meta.update({
        'active': True,
        'created_at': misc.now_tz_utc().isoformat(),
        'deployed': True,
    })
    secrets.edit(f'{token}#', meta)
    update(token)


def rotate(single_token: str, dry_run: bool, force: bool) -> None:
    # pylint: disable=too-many-locals
    """Rotate tokens."""
    if force and not dry_run and not single_token:
        print('--force requires --token', file=sys.stderr)
        return

    secrets_data = yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE'))
    tokens = {k.split('/')[0] for k in misc.flattened(single_token or list(secrets_data.keys()))}
    for token in tokens:
        if not (metas := {
            k: v.get('meta', {})
            for k, v in secrets_data.items()
            if (k == token or k.startswith(f'{token}/'))
            and misc.get_nested_key(v, 'meta/token_type') == 'ssh_private_key'
        }):
            if single_token:
                print(f'Unable to find rotatable secrets for {single_token}', file=sys.stderr)
            else:
                LOGGER.debug('Not processing %s - unable to find rotatable secrets', token)
            continue

        old_name, old_meta = next((i for i in sorted(
            metas.items(), key=lambda i: misc.datetime_fromisoformat_tz_utc(i[1]['created_at']),
            reverse=True,
        )), (None, {}))

        if not force and (
            not (created_at := old_meta.get('created_at')) or
            misc.now_tz_utc() + utils.DEFAULT_DELTA <
                misc.datetime_fromisoformat_tz_utc(created_at) + utils.DEFAULT_INTERVAL
        ):
            LOGGER.debug('Not processing %s - new enough', token)
            continue

        new_name = f'{token}/{int(misc.now_tz_utc().timestamp())}'

        if dry_run:
            print(f'Would rotate token {old_name} into {new_name}', file=sys.stderr)
        else:
            print(f'Rotating token {old_name} into {new_name}', file=sys.stderr)
            for secret_name, secret_meta in metas.items():
                if secret_meta.get('deployed', True):
                    secrets.edit(f'{secret_name}#deployed', False)
            secrets.edit(f'{new_name}#', old_meta)
            create(new_name)


def validate(single_token: str) -> None:
    """Validate tokens."""
    secrets_data = yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE'))
    children = collections.defaultdict(set)
    for key in secrets_data.keys():
        children[key.split('/')[0]].add(key)
    for token in misc.flattened(single_token or utils.all_tokens({'ssh_private_key'})):
        LOGGER.info('Processing %s', token)
        token_secret = secrets.secret(f'{token}:')
        meta = secrets.secret(f'{token}#')
        deployed = meta.get('deployed', True)

        if not deployed:
            LOGGER.info('Not checking validity of undeployed token %s', token)
            continue
        parent_token = token.split('/')[0]
        if 'deployed' not in meta:
            raise Exception(f'Rotatable {token} missing deployed field')
        if 'active' not in meta:
            raise Exception(f'Rotatable {token} missing active field')
        if (count := len([m for m in children[parent_token]
                          if misc.get_nested_key(secrets_data[m], 'meta/deployed')])) != 1:
            raise Exception(
                f'Rotatable {parent_token} is deployed {count} times')
        if (count := len([m for m in children[parent_token]
                          if misc.get_nested_key(secrets_data[m], 'meta/active')])) not in {1, 2}:
            raise Exception(
                f'Rotatable {parent_token} has {count} != {1, 2} active children')

        private_key = serialization.load_ssh_private_key(
            token_secret['private_key'].encode('ascii'), None)
        public_key = serialization.load_ssh_public_key(
            token_secret['public_key'].encode('ascii'), None)
        if private_key.public_key().public_bytes(
            serialization.Encoding.OpenSSH,
            serialization.PublicFormat.OpenSSH
        ) != public_key.public_bytes(
            serialization.Encoding.OpenSSH,
            serialization.PublicFormat.OpenSSH
        ):
            raise Exception('Public key does not match private key')


def process(action: str, token: str = '', dry_run: bool = False, force: bool = False) -> None:
    """Process actions."""
    match action:
        case 'create':
            create(token)
        case 'update':
            update(token)
        case 'rotate':
            rotate(token, dry_run, force)
        case 'validate':
            validate(token)
