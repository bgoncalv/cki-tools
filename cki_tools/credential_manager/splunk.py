"""Splunk credential management."""
import json
from urllib import parse

from cki_lib import logger
from cki_lib import misc
from cki_lib import session

from cki.deployment_tools import secrets

from . import utils

LOGGER = logger.get_logger(__name__)
SESSION = session.get_session(__name__)


def validate(single_token: str) -> None:
    """Validate the secrets."""
    for token in misc.flattened(single_token or utils.all_tokens({'splunk_hec_token'})):
        LOGGER.info('Processing %s', token)
        token_secret = secrets.secret(token)
        meta = secrets.secret(f'{token}#')
        data = json.dumps({'index': index}) if (index := meta.get('index')) else ''
        result = SESSION.post(parse.urljoin(meta['endpoint_url'], 'services/collector'), data=data,
                              headers={'Authorization': f'Splunk {token_secret}'})
        if (result.json() or {}).get('code') not in {5, 12}:
            result.raise_for_status()


def process(action: str, token: str = '') -> None:
    """Process actions."""
    match action:
        case 'validate':
            validate(token)
