"""Credential metrics."""

import os

from cki_lib import misc
from cki_lib import yaml
import prometheus_client

from . import gitlab

TOKEN_TYPES = {
    'aws_secret_access_key',
    'bugzilla_token',
    'dogtag_certificate',
    'password',
    'ssh_private_key',
} | gitlab.GitLabCredentials.token_types


def process() -> str:
    """Return token metrics."""
    registry = prometheus_client.CollectorRegistry()
    metric_created_at = prometheus_client.Gauge(
        'cki_token_created_at',  'timestamp when a token was created',
        ['name', 'active', 'deployed'], registry=registry,
    )
    metric_expires_at = prometheus_client.Gauge(
        'cki_token_expires_at', 'timestamp when validity of token ends',
        ['name', 'active', 'deployed'], registry=registry,
    )

    for token, data in yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE')).items():
        token_type = misc.get_nested_key(data, 'meta/token_type')
        active = misc.booltostr(misc.get_nested_key(data, 'meta/active', True))
        deployed = misc.booltostr(misc.get_nested_key(data, 'meta/deployed', True))
        created_at = misc.get_nested_key(data, 'meta/created_at')
        expires_at = misc.get_nested_key(data, 'meta/expires_at')

        if created_at and token_type in TOKEN_TYPES:
            metric_created_at.labels(
                token, active, deployed
            ).set(misc.datetime_fromisoformat_tz_utc(created_at).timestamp())
        if expires_at:
            metric_expires_at.labels(
                token, active, deployed
            ).set(misc.datetime_fromisoformat_tz_utc(expires_at).timestamp())
    return prometheus_client.generate_latest(registry).decode('utf8')
