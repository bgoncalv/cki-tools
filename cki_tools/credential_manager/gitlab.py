"""GitLab credential management."""

import collections
import datetime
import os
import sys
import typing

from cki_lib import logger
from cki_lib import misc
from cki_lib import yaml
from cki_lib.gitlab import get_instance
from cki_lib.gitlab import parse_gitlab_url
from gitlab import client
from gitlab.v4 import objects

from cki.deployment_tools import secrets

from . import utils

LOGGER = logger.get_logger(__name__)

TIMEDELTA_UNUSED = datetime.timedelta(hours=24)
TIMEDELTA_EXPIRY = datetime.timedelta(days=365)


class GitLabCredentials:
    """GitLab credential management."""

    token_types = {
        'gitlab_project_token',
        'gitlab_group_token',
        'gitlab_personal_token',
        'gitlab_project_deploy_token',
        'gitlab_group_deploy_token',
        'gitlab_runner_authentication_token',
    }

    def __init__(self, token: str, dry_run: bool, force: bool) -> None:
        """AWS credential management."""
        self.token = token
        self.dry_run = dry_run
        self.force = force

        all_tokens = {
            k: v for k, v in yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE')).items()
            if misc.get_nested_key(v, 'meta/token_type') in self.token_types
        }
        self.cached_tokens = self.get_active_token_data(all_tokens, token)
        self.cached_token_names = {k.split('/')[0] for k in self.cached_tokens.keys()}

    @staticmethod
    def get_active_token_data(
        all_tokens: dict[str, typing.Any],
        single_token: str,
    ) -> dict[str, typing.Any]:
        """Return active matching tokens."""
        return {
            k: v for k, v in all_tokens.items()
            if (not single_token or k == single_token or k.startswith(f'{single_token}/'))
            and misc.get_nested_key(v, 'meta/active')
        }

    @staticmethod
    def _create_personal_token(
        gl_instance: client.Gitlab,
        gl_owner: objects.Project | objects.Group,
        meta: dict[str, typing.Any],
    ) -> str:
        gl_personal_token = gl_owner.access_tokens.create({
            'name': meta['token_name'],
            'expires_at': (misc.now_tz_utc() + TIMEDELTA_EXPIRY).date().isoformat(),
            'scopes': meta['scopes'],
            'access_level': meta['access_level'],
        })
        gl_user = gl_instance.users.get(gl_personal_token.user_id)
        meta.update({
            'token_id': gl_personal_token.id,
            'created_at': gl_personal_token.created_at,
            'expires_at': gl_personal_token.expires_at,
            'revoked': gl_personal_token.revoked,
            'active': gl_personal_token.active,
            'user_id': gl_personal_token.user_id,
            'user_name': gl_user.username,
            'deployed': False,
        })
        return typing.cast(str, gl_personal_token.token)

    @staticmethod
    def _create_deploy_token(
        gl_owner: objects.Project | objects.Group,
        meta: dict[str, typing.Any],
    ) -> str:
        gl_deploy_token = gl_owner.deploytokens.create({
            'name': meta['token_name'],
            'expires_at': meta.get('expires_at'),
            'scopes': meta['scopes'],
            # the '+' in the default gitlab+deploy-token-{n} breaks repo mirroring
            'username': misc.now_tz_utc().strftime('gitlab-deploy-token-%f'),
        })
        meta.update({
            'token_id': gl_deploy_token.id,
            'created_at': misc.now_tz_utc().isoformat(),
            'expires_at': gl_deploy_token.expires_at,
            'revoked': gl_deploy_token.revoked,
            'active': not gl_deploy_token.expired,
            'user_name': gl_deploy_token.username,
            'deployed': False,
        })
        return typing.cast(str, gl_deploy_token.token)

    def create(self) -> None:
        """Create tokens."""
        if not self.token:
            print('create requires a single token')
            return
        print(f'Creating {self.token}')
        self.create_token(self.token)

    def create_token(self, token: str) -> None:
        """Create a token."""
        LOGGER.info('Processing %s', token)
        meta = secrets.secret(f'{token}#')
        match meta['token_type']:
            case 'gitlab_project_token':
                gl_instance, gl_owner = parse_gitlab_url(meta['project_url'])
                token_secret = self._create_personal_token(gl_instance, gl_owner, meta)
            case 'gitlab_group_token':
                gl_instance, gl_owner = parse_gitlab_url(meta['group_url'])
                token_secret = self._create_personal_token(gl_instance, gl_owner, meta)
            case 'gitlab_project_deploy_token':
                gl_instance, gl_owner = parse_gitlab_url(meta['project_url'])
                token_secret = self._create_deploy_token(gl_owner, meta)
            case 'gitlab_group_deploy_token':
                gl_instance, gl_owner = parse_gitlab_url(meta['group_url'])
                token_secret = self._create_deploy_token(gl_owner, meta)
            case token_type:
                raise Exception(f'Unable to create {token_type} {token}')
        secrets.edit(f'{token}#', meta)
        secrets.edit(f'{token}', token_secret)

    def destroy(self) -> None:
        """Destroy tokens."""
        if not self.token:
            print('destroy requires a single token')
            return
        print(f'Destroying {self.token}')
        self.destroy_token(self.token)

    @staticmethod
    def destroy_token(token: str) -> None:
        """Destroy a token."""
        meta = secrets.secret(f'{token}#')

        if meta['deployed']:
            print('Not destroying deployed version %s', token)
            return

        match meta['token_type']:
            case 'gitlab_project_token':
                _, gl_owner = parse_gitlab_url(meta['project_url'])
                gl_owner.access_tokens.delete(meta['token_id'])
            case 'gitlab_group_token':
                _, gl_owner = parse_gitlab_url(meta['group_url'])
                gl_owner.access_tokens.delete(meta['token_id'])
            case 'gitlab_personal_token':
                secret_token = secrets.secret(token)
                gl_instance = get_instance(meta['instance_url'], token=secret_token)
                gl_instance.personal_access_tokens.delete('self')
            case 'gitlab_project_deploy_token':
                _, gl_owner = parse_gitlab_url(meta['project_url'])
                gl_owner.deploytokens.delete(meta['token_id'])
            case 'gitlab_group_deploy_token':
                _, gl_owner = parse_gitlab_url(meta['group_url'])
                gl_owner.deploytokens.delete(meta['token_id'])
            case token_type:
                raise Exception(f'Unable to destroy {token_type} {token}')

        secrets.edit(f'{token}#active', False)

    @staticmethod
    def _self_token(instance_url: str, secret_token: str) -> objects.PersonalAccessToken:
        return get_instance(instance_url, token=secret_token).personal_access_tokens.get('self')

    def _update_project_token(self, meta: dict[str, str], secret_token: str) -> None:
        gl_instance, gl_project = parse_gitlab_url(meta['project_url'])
        if 'token_id' not in meta:
            gl_self_token = self._self_token(gl_instance.url, secret_token)
            meta['token_id'] = gl_self_token.id
        gl_project_token = gl_project.access_tokens.get(meta['token_id'])
        gl_member = gl_project.members.get(gl_project_token.user_id)
        meta.update({
            'scopes': gl_project_token.scopes,
            'access_level': gl_member.access_level,
            'token_name': gl_project_token.name,
            'token_id': gl_project_token.id,
            'created_at': gl_project_token.created_at,
            'expires_at': gl_project_token.expires_at,
            'revoked': gl_project_token.revoked,
            'active': gl_project_token.active,
            'user_id': gl_project_token.user_id,
            'user_name': gl_member.username,
        })

    def _update_group_token(self, meta: dict[str, str], secret_token: str) -> None:
        gl_instance, gl_group = parse_gitlab_url(meta['group_url'])
        if 'token_id' not in meta:
            gl_self_token = self._self_token(gl_instance.url, secret_token)
            meta['token_id'] = gl_self_token.id
        gl_group_token = gl_group.access_tokens.get(meta['token_id'])
        gl_member = gl_group.members.get(gl_group_token.user_id)
        meta.update({
            'scopes': gl_group_token.scopes,
            'access_level': gl_member.access_level,
            'token_name': gl_group_token.name,
            'token_id': gl_group_token.id,
            'created_at': gl_group_token.created_at,
            'expires_at': gl_group_token.expires_at,
            'revoked': gl_group_token.revoked,
            'active': gl_group_token.active,
            'user_id': gl_group_token.user_id,
            'user_name': gl_member.username,
        })

    def _update_personal_token(
        self,
        meta: dict[str, typing.Any],
        secret_token: str,
    ) -> None:
        if 'token_id' not in meta:
            gl_self_token = self._self_token(meta['instance_url'], secret_token)
            meta['token_id'] = gl_self_token.id
            meta['user_id'] = gl_self_token.user_id
            meta['scopes'] = gl_self_token.scopes
        if set(meta['scopes']) & {'api', 'read_api'} and meta.get('active', True):
            gl_instance = get_instance(meta['instance_url'], token=secret_token)
        else:
            api_token = self.get_api_token_for_pat(meta, {'api', 'read_api'})
            gl_instance = get_instance(meta['instance_url'], token=secrets.secret(api_token))
        gl_personal_token = gl_instance.personal_access_tokens.get(meta['token_id'])
        gl_user = gl_instance.users.get(gl_personal_token.user_id)
        meta.update({
            'scopes': gl_personal_token.scopes,
            'token_name': gl_personal_token.name,
            'token_id': gl_personal_token.id,
            'created_at': gl_personal_token.created_at,
            'expires_at': gl_personal_token.expires_at,
            'revoked': gl_personal_token.revoked,
            'active': gl_personal_token.active,
            'user_id': gl_personal_token.user_id,
            'user_name': gl_user.username,
        })

    @staticmethod
    def _update_project_deploy_token(meta: dict[str, typing.Any]) -> None:
        _, gl_project = parse_gitlab_url(meta['project_url'])
        gl_deploy_token = gl_project.deploytokens.get(meta['token_id'])
        meta.update({
            'scopes': gl_deploy_token.scopes,
            'token_name': gl_deploy_token.name,
            'token_id': gl_deploy_token.id,
            'expires_at': gl_deploy_token.expires_at,
            'revoked': gl_deploy_token.revoked,
            'active': not gl_deploy_token.expired,
            'user_name': gl_deploy_token.username,
        })

    @staticmethod
    def _update_group_deploy_token(meta: dict[str, typing.Any]) -> None:
        _, gl_group = parse_gitlab_url(meta['group_url'])
        gl_deploy_token = gl_group.deploytokens.get(meta['token_id'])
        meta.update({
            'scopes': gl_deploy_token.scopes,
            'token_name': gl_deploy_token.name,
            'token_id': gl_deploy_token.id,
            'expires_at': gl_deploy_token.expires_at,
            'revoked': gl_deploy_token.revoked,
            'active': not gl_deploy_token.expired,
            'user_name': gl_deploy_token.username,
        })

    @staticmethod
    def _update_runner_token(meta: dict[str, typing.Any], secret_token: str) -> None:
        gl_instance = get_instance(meta['instance_url'])
        response = gl_instance.http_post(
            '/runners/verify', post_data={"token": secret_token})
        meta.update({
            'token_id': response['id'],
        })
        if expires_at := response['token_expires_at']:
            meta['expires_at'] = expires_at
            meta['active'] = misc.now_tz_utc() < misc.datetime_fromisoformat_tz_utc(expires_at)
        else:
            meta['active'] = True

    def update(self) -> None:
        """Update the secret meta information about tokens."""
        for token in self.cached_tokens.keys():
            print(f'Updating {token}')
            self.update_token(token)

    def update_token(self, token: str) -> None:
        """Update the secret meta information about tokens."""
        LOGGER.info('Processing %s', token)
        meta = secrets.secret(f'{token}#')
        token_type = meta['token_type']
        LOGGER.debug('Processing %s - token type: %s', token, token_type)
        match token_type:
            case 'gitlab_project_token':
                self._update_project_token(meta, secrets.secret(token))
            case 'gitlab_group_token':
                self._update_group_token(meta, secrets.secret(token))
            case 'gitlab_personal_token':
                self._update_personal_token(meta, secrets.secret(token))
            case 'gitlab_project_deploy_token':
                self._update_project_deploy_token(meta)
            case 'gitlab_group_deploy_token':
                self._update_group_deploy_token(meta)
            case 'gitlab_runner_authentication_token':
                self._update_runner_token(meta, secrets.secret(token))
            case _:
                LOGGER.info('Token meta update for %s %s not supported', token_type, token)
        secrets.edit(f'{token}#', meta)

    def _validate_rotatable_token(
        self,
        token_type: str,
        token: str,
        meta: dict[str, typing.Any],
        children_names: collections.abc.Iterable[str],
    ) -> None:
        """Validate number and setup of rotatable tokens."""
        parent_token = token.split('/')[0]
        if 'deployed' not in meta:
            raise Exception(f'Rotatable {token_type} {token} missing deployed field')
        if 'active' not in meta:
            raise Exception(f'Rotatable {token_type} {token} missing active field')
        if (count := len([m for m in children_names
                          if misc.get_nested_key(self.cached_tokens[m], 'meta/deployed')])) != 1:
            raise Exception(f'Rotatable {token_type} {parent_token} is deployed {count} times')
        if (count := len([m for m in children_names
                          if misc.get_nested_key(self.cached_tokens[m], 'meta/active')])) < 2:
            raise Exception(
                f'Rotatable {token_type} {parent_token} has {count} < 2 active children')

    def validate(self) -> None:
        # pylint: disable=too-many-branches,too-many-locals
        """Check validity of the token."""
        for token in self.cached_tokens.keys():
            print(f'Validating {token}')
            self.validate_token(token)

    def validate_token(self, token: str) -> None:
        """Check validity of a token."""
        LOGGER.info('Processing %s', token)
        token_secret = secrets.secret(token)
        meta = secrets.secret(f'{token}#')
        token_type = meta['token_type']
        deployed = meta.get('deployed', True)
        LOGGER.debug('Processing %s - token type: %s', token, token_type)
        children_names = self.get_active_token_data(self.cached_tokens, token.split('/')[0]).keys()
        match deployed, token_type:
            case False, _:
                LOGGER.info('Not checking validity of undeployed token %s', token)
            case _, 'gitlab_project_token':
                gl_instance = get_instance(meta['project_url'], token=token_secret)
                gl_project_token = gl_instance.personal_access_tokens.get('self')
                if gl_project_token.revoked or not gl_project_token.active:
                    raise Exception(f'Revoked token {token}')
                self._validate_rotatable_token(token_type, token, meta, children_names)
            case _, 'gitlab_group_token':
                gl_instance = get_instance(meta['group_url'], token=token_secret)
                gl_group_token = gl_instance.personal_access_tokens.get('self')
                if gl_group_token.revoked or not gl_group_token.active:
                    raise Exception(f'Revoked token {token}')
                self._validate_rotatable_token(token_type, token, meta, children_names)
            case _, 'gitlab_personal_token':
                gl_instance = get_instance(meta['instance_url'], token=token_secret)
                gl_personal_token = gl_instance.personal_access_tokens.get('self')
                if gl_personal_token.revoked or not gl_personal_token.active:
                    raise Exception(f'Revoked token {token}')
                self._validate_rotatable_token(token_type, token, meta, children_names)
            case _, 'gitlab_project_deploy_token':
                gl_instance, gl_project = parse_gitlab_url(meta['project_url'])
                gl_deploy_token = gl_project.deploytokens.get(meta['token_id'])
                if gl_deploy_token.revoked or gl_deploy_token.expired:
                    raise Exception(f'Revoked or expired token {token}')
            case _, 'gitlab_group_deploy_token':
                gl_instance, gl_group = parse_gitlab_url(meta['group_url'])
                gl_deploy_token = gl_group.deploytokens.get(meta['token_id'])
                if gl_deploy_token.revoked or gl_deploy_token.expired:
                    raise Exception(f'Revoked or expired token {token}')
            case _, 'gitlab_runner_authentication_token':
                gl_instance = get_instance(meta['instance_url'])
                gl_instance.http_post('/runners/verify', post_data={"token": token_secret})
            case _:
                LOGGER.info('Token validation for %s %s not supported', token_type, token)

    def get_api_token_for_pat(
        self,
        pat_meta: dict[str, typing.Any],
        scopes: set[str],
    ) -> str:
        """Find a (preferably deployed) token with enough permissions to rotate the PAT."""
        return next(
            k for k, v in sorted(self.cached_tokens.items(), reverse=True,
                                 key=lambda i: misc.get_nested_key(i[1], 'meta/deployed', True))
            if misc.get_nested_key(v, 'meta/token_type') == pat_meta['token_type']
            and misc.get_nested_key(v, 'meta/instance_url') == pat_meta['instance_url']
            and misc.get_nested_key(v, 'meta/user_id') == pat_meta['user_id']
            and misc.get_nested_key(v, 'meta/active', True)
            and scopes & set(misc.get_nested_key(v, 'meta/scopes', []))
        )

    @staticmethod
    def check_needs_rotate(
        active_token_data: dict[str, typing.Any],
    ) -> str | None:
        """Return whether the token needs rotation because of age/expiry."""
        deployed_token = next(v for v in active_token_data.values()
                              if misc.get_nested_key(v, 'meta/deployed'))
        if utils.too_old(utils.tz(deployed_token, 'created_at'), utils.DEFAULT_INTERVAL):
            return 'Token too old'
        if utils.too_old(utils.tz(deployed_token, 'expires_at')):
            return 'Token close to expiry'
        return None

    @staticmethod
    def check_needs_prepare(
        active_token_data: dict[str, typing.Any],
    ) -> str | None:
        """Check whether the token needs preparation before it can be rotated.."""
        first_token = next(iter(active_token_data.values()))
        if misc.get_nested_key(first_token, 'meta/token_type') not in {
            'gitlab_project_token', 'gitlab_group_token',
            'gitlab_personal_token',
            'gitlab_project_deploy_token', 'gitlab_group_deploy_token',
        }:
            # other token types cannot be rotated
            return None
        if (count := len(active_token_data)) != 2:
            return f'Found {count} versions instead of 2'
        token_active, token_deployed = list(active_token_data.values())
        if misc.get_nested_key(token_active, 'meta/deployed'):
            token_active, token_deployed = token_deployed, token_active
        if misc.get_nested_key(token_active, 'meta/deployed'):
            return 'Both versions are deployed'
        active_created_at = utils.tz(token_active, 'created_at')
        deployed_created_at = utils.tz(token_deployed, 'created_at')
        if not active_created_at or not deployed_created_at:
            return 'Creation dates missing'
        if deployed_created_at > active_created_at:
            return 'Deployed version is newer than undeployed version'
        return None

    @staticmethod
    def check_needs_clean(
        active_token_data: dict[str, typing.Any],
    ) -> str | None:
        """Check whether the token needs cleaning from rotation.."""
        first_token = next(iter(active_token_data.values()))
        match misc.get_nested_key(first_token, 'meta/token_type'):
            case 'gitlab_project_token' | 'gitlab_group_token' | 'gitlab_personal_token':
                if (count := len(active_token_data)) != 2:
                    return f'Found {count} active versions instead of 2'
            case _:
                if (count := len(active_token_data)) != 1:
                    return f'Found {count} active versions instead of 1'
        if (count := len([t for t in active_token_data.values()
                          if misc.get_nested_key(t, 'meta/deployed')])) != 1:
            return f'Found {count} deployed versions instead of 1?'
        return None

    def stats(self) -> None:
        """Print statistics about tokens."""
        print(f'Supported token versions: {len(self.cached_tokens)}')
        for token_name in self.cached_tokens.keys():
            LOGGER.debug('%s', token_name)
        print(f'Supported tokens: {len(self.cached_token_names)}')
        needs_rotate_all, needs_prepare_all, needs_clean_all = set(), set(), set()
        for token_name in self.cached_token_names:
            token_data = self.get_active_token_data(self.cached_tokens, token_name)
            needs_rotate = self.check_needs_rotate(token_data)
            needs_prepare = self.check_needs_prepare(token_data)
            needs_clean = self.check_needs_clean(token_data)
            LOGGER.debug('%s: needs_rotate=%s needs_prepare=%s needs_clean=%s',
                         token_name, needs_rotate, needs_prepare, needs_clean)
            needs_rotate_all |= {token_name} if needs_rotate else set()
            needs_prepare_all |= {token_name} if needs_prepare else set()
            needs_clean_all |= {token_name} if needs_clean else set()
        # by default, only show tokens that need to be prepared if they also need to be rotated
        if not self.force:
            needs_prepare_all &= needs_rotate_all
        print(f'Tokens that need to be rotated: {list(needs_rotate_all)}')
        print(f'Tokens that need to be prepared: {list(needs_prepare_all)}')
        print(f'Tokens that need to be cleaned: {list(needs_clean_all)}')

    def rotate(self) -> None:
        """Rotate tokens."""
        if self.force and not self.dry_run and not self.token:
            print('--force requires --token', file=sys.stderr)
            return
        for token in self.cached_tokens.keys():
            print(f'Rotating {token}')
            self.rotate_token(token)

    def rotate_token(self, token: str) -> None:
        """Rotate a single token."""
        if not (metas := {
            k: v.get('meta', {})
            for k, v in
                self.get_active_token_data(self.cached_tokens, token).items()
                if misc.get_nested_key(v, 'meta/token_type') in
                {'gitlab_project_token', 'gitlab_group_token', 'gitlab_personal_token'}
        }):
            print(f'Unable to find rotatable secrets for {token}', file=sys.stderr)
            return

        old_name, old_meta = next((
            i for i in metas.items() if i[1].get('active', True) and not i[1].get('deployed', True)
        ), (None, {}))

        if not old_name:
            LOGGER.debug('Not processing %s - no undeployed version', token)
            return

        if not self.force and (
            not (expires_at := old_meta.get('expires_at')) or
            misc.now_tz_utc() + utils.DEFAULT_DELTA < misc.datetime_fromisoformat_tz_utc(expires_at)
        ):
            LOGGER.debug('Not processing %s - new enough', token)
            return

        new_name = f'{token}/{int(misc.now_tz_utc().timestamp())}'

        match old_meta['token_type']:
            case 'gitlab_project_token':
                api_token = 'GITLAB_TOKENS'
                _, gl_project = parse_gitlab_url(old_meta['project_url'])
                gl_token = gl_project.access_tokens.get(old_meta['token_id'])
            case 'gitlab_group_token':
                api_token = 'GITLAB_TOKENS'
                _, gl_group = parse_gitlab_url(old_meta['group_url'])
                gl_token = gl_group.access_tokens.get(old_meta['token_id'])
            case 'gitlab_personal_token':
                api_token = self.get_api_token_for_pat(old_meta, {'api'})
                gl_instance = get_instance(
                    old_meta['instance_url'], token=secrets.secret(api_token))
                gl_token = gl_instance.personal_access_tokens.get(old_meta['token_id'])
            case _:
                LOGGER.info('Token rotation for %s not supported', token)
                return

        if not self.force and gl_token.last_used_at and (
            used := (misc.now_tz_utc() - misc.datetime_fromisoformat_tz_utc(gl_token.last_used_at))
        ) < TIMEDELTA_UNUSED:
            print(f'Not rotating {old_name} as used {used} ago', file=sys.stderr)
            return

        if self.dry_run:
            print(
                f'Would rotate token {old_name} into {new_name} via {api_token}', file=sys.stderr)
        else:
            print(f'Rotating token {old_name} into {new_name} via {api_token}', file=sys.stderr)
            for secret_name, secret_meta in metas.items():
                if secret_meta.get('deployed', True):
                    secrets.edit(f'{secret_name}#deployed', False)
            gl_token.rotate(expires_at=(misc.now_tz_utc() +
                            TIMEDELTA_EXPIRY).date().isoformat())
            secrets.edit(f'{new_name}', gl_token.token)
            secrets.edit(f'{new_name}#', {**old_meta,
                         'deployed': True, 'token_id': gl_token.id})
            self.update_token(new_name)
            secrets.edit(f'{old_name}#active', False)
            secrets.edit(f'{old_name}#revoked', True)


def process(action: str, token: str = '', dry_run: bool = False, force: bool = False) -> None:
    """Process gitlab actions."""
    credentials = GitLabCredentials(token, dry_run, force)

    match action:
        case 'create':
            credentials.create()
        case 'destroy':
            credentials.destroy()

        case 'update':
            credentials.update()
        case 'validate':
            credentials.validate()

        case 'stats':
            credentials.stats()
        case 'rotate':
            credentials.rotate()
