"""Dogtag certificate management."""
import os
import sys
from urllib import parse

from cki_lib import misc
from cki_lib import yaml
from cki_lib.logger import get_logger
from cki_lib.session import get_session
from cryptography import x509
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.x509.oid import NameOID

from cki.deployment_tools import secrets

from . import utils

LOGGER = get_logger(__name__)
SESSION = get_session(__name__, headers={'Accept': 'application/json'}, raise_for_status=True)


def update(single_token: str) -> None:
    """Update the meta information."""
    for token in misc.flattened(single_token or utils.all_tokens({'dogtag_certificate'})):
        LOGGER.info('Processing %s', token)
        token_secret = secrets.secret(f'{token}:')
        meta = secrets.secret(f'{token}#')
        cert = SESSION.get(parse.urljoin(meta['server_url'], f'ca/rest/certs/{meta["id"]}')).json()
        certificate = cert['Encoded'].strip()
        meta.update({
            'id': cert['id'],
            'created_at': misc.datetime_fromisoformat_tz_utc(cert['NotBefore']).isoformat(),
            'expires_at': misc.datetime_fromisoformat_tz_utc(cert['NotAfter']).isoformat(),
            'active': cert['Status'] == 'VALID',
            'IssuerDN': cert['IssuerDN'],
            'SubjectDN': cert['SubjectDN'],
        })
        secrets.edit(f'{token}#', meta)
        if certificate != token_secret.get('certificate'):
            secrets.edit(f'{token}:certificate', certificate)


def create(token: str) -> None:
    """Create Dogtag certificates."""
    if not token:
        print('create requires --token', file=sys.stderr)
        return

    secrets_data = yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE'))
    LOGGER.info('Processing %s', token)
    meta = secrets.secret(f'{token}#')
    uid = next(p.split('=', 1)[1] for p in meta['SubjectDN'].split(',')
               if p.lower().startswith('uid='))
    password = secrets.secret(next(
        k for k, v in secrets_data.items()
        if misc.get_nested_key(v, 'meta/token_type') == 'ldap_password'
        and misc.get_nested_key(v, 'meta/uid') == uid))
    private_key = rsa.generate_private_key(65537, 4096)
    secrets.edit(f'{token}:private_key', private_key.private_bytes(
        serialization.Encoding.PEM,
        serialization.PrivateFormat.PKCS8,
        serialization.NoEncryption()).decode('ascii').strip())
    csr = x509.CertificateSigningRequestBuilder().subject_name(x509.Name([
        x509.NameAttribute(NameOID.USER_ID, uid),
    ])).sign(private_key, hashes.SHA256()).public_bytes(serialization.Encoding.PEM).decode('ascii')
    certrequest_url = SESSION.post(parse.urljoin(meta['server_url'], 'ca/rest/certrequests'), json={
        'ProfileID': 'caDirAppUserCert',
        'Input': [{'Attribute': [
            {'name': 'cert_request_type', 'Value': 'pkcs10'},
            {'name': 'cert_request', 'Value': csr},
        ]}],
        'Attributes': {'Attribute': [
            {'name': 'uid', 'value': uid},
            {'name': 'pwd', 'value': password},
        ]},
    }).json()['entries'][0]['requestURL']
    meta.update({
        'deployed': True,
        'id': SESSION.get(certrequest_url).json()['certId'],
    })
    secrets.edit(f'{token}#', meta)
    update(token)


def rotate(single_token: str, dry_run: bool, force: bool) -> None:
    # pylint: disable=too-many-locals
    """Rotate tokens."""
    if force and not dry_run and not single_token:
        print('--force requires --token', file=sys.stderr)
        return

    secrets_data = yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE'))
    tokens = {k.split('/')[0] for k in misc.flattened(single_token or list(secrets_data.keys()))}
    for token in tokens:
        if not (metas := {
            k: v.get('meta', {})
            for k, v in secrets_data.items()
            if (k == token or k.startswith(f'{token}/'))
            and misc.get_nested_key(v, 'meta/token_type') == 'dogtag_certificate'
        }):
            if single_token:
                print(f'Unable to find rotatable secrets for {single_token}', file=sys.stderr)
            else:
                LOGGER.debug('Not processing %s - unable to find rotatable secrets', token)
            continue

        old_name, old_meta = next((i for i in sorted(
            metas.items(), key=lambda i: misc.datetime_fromisoformat_tz_utc(i[1]['expires_at']),
            reverse=True,
        )), (None, {}))

        if not force and (
            not (expires_at := old_meta.get('expires_at')) or
            misc.now_tz_utc() + utils.DEFAULT_DELTA < misc.datetime_fromisoformat_tz_utc(expires_at)
        ):
            LOGGER.debug('Not processing %s - new enough', token)
            continue

        new_name = f'{token}/{int(misc.now_tz_utc().timestamp())}'

        if dry_run:
            print(f'Would rotate token {old_name} into {new_name}', file=sys.stderr)
        else:
            print(f'Rotating token {old_name} into {new_name}', file=sys.stderr)
            for secret_name, secret_meta in metas.items():
                if secret_meta.get('deployed', True):
                    secrets.edit(f'{secret_name}#deployed', False)
            secrets.edit(f'{new_name}#', old_meta)
            create(new_name)


def validate(single_token: str) -> None:
    """Validate tokens."""
    for token in misc.flattened(single_token or utils.all_tokens({'dogtag_certificate'})):
        LOGGER.info('Processing %s', token)
        token_secret = secrets.secret(f'{token}:')
        meta = secrets.secret(f'{token}#')

        if not meta.get('deployed', True):
            LOGGER.info('Not checking validity of undeployed token %s', token)
            continue

        private_key = serialization.load_pem_private_key(
            token_secret['private_key'].encode('ascii'), None)
        cert = x509.load_pem_x509_certificate(token_secret['certificate'].encode('ascii'))
        if private_key.public_key().public_bytes(
            serialization.Encoding.PEM,
            serialization.PublicFormat.SubjectPublicKeyInfo,
        ) != cert.public_key().public_bytes(
            serialization.Encoding.PEM,
            serialization.PublicFormat.SubjectPublicKeyInfo,
        ):
            raise Exception('Certificate does not match private key')

        if misc.datetime_fromisoformat_tz_utc(meta['expires_at']) < misc.now_tz_utc():
            raise Exception(f'Certificate expired at {meta["expires_at"]}')


def process(action: str, token: str = '', dry_run: bool = False, force: bool = False) -> None:
    """Process actions."""
    match action:
        case 'create':
            create(token)
        case 'update':
            update(token)
        case 'rotate':
            rotate(token, dry_run, force)
        case 'validate':
            validate(token)
