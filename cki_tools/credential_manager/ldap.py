"""LDAP credential management."""
import functools
import typing

from cki_lib import logger
from cki_lib import misc
import ldap

from cki.deployment_tools import secrets

from . import utils

LOGGER = logger.get_logger(__name__)


def ldap_clean_attributes(
    ldap_server: str,
    attributes: dict[str, list[bytes]],
) -> dict[str, str | list[str]]:
    """Clean the LDAP server response.

    The following changes are made:
    - all (binary) values are decoded as UTF-8
    - dynamic Rover groups that can be detected by the existence of the
      rhatRoverGroupMemberQuery attribute on the group are removed from memberOf
    - single-value lists are reduced to the value itself
    """
    result: dict[str, str | list[str]] = {}
    for key, value in attributes.items():
        decoded_value = [v.decode('utf8') for v in value]
        if key == 'memberOf':
            decoded_value = [
                v for v in decoded_value
                if not ldap_search(ldap_server, v, attrlist=('rhatRoverGroupMemberQuery',))
            ]
        result[key] = decoded_value[0] if len(decoded_value) == 1 else decoded_value
    return result


@functools.cache
def ldap_search(ldap_server: str, dn: str, **kwargs: typing.Any) -> dict[str, str | list[str]]:
    """Query an LDAP server for an object."""
    LOGGER.debug('Querying %s for %s', ldap_server, dn)
    return ldap_clean_attributes(
        # pylint: disable=no-member
        ldap_server, ldap.initialize(ldap_server).search_s(dn, ldap.SCOPE_BASE, **kwargs)[0][1]
    )


def ldap_login(ldap_server: str, dn: str, password: str) -> None:
    """Query an LDAP server for an object."""
    LOGGER.debug('Logging into %s with %s', ldap_server, dn)
    ldap_object = ldap.initialize(ldap_server)
    ldap_object.start_tls_s()
    ldap_object.simple_bind_s(dn, password)


def update(single_token: str) -> None:
    """Update the secret meta information about GitLab tokens."""
    for token in misc.flattened(single_token or utils.all_tokens({'ldap_keytab', 'ldap_password'})):
        LOGGER.info('Processing %s', token)
        meta = secrets.secret(f'{token}#')
        meta.update(ldap_search(f'ldap://{meta["ldap_server"]}', meta['dn']))
        secrets.edit(f'{token}#', meta)


def validate(single_token: str) -> None:
    """Update the secret meta information about GitLab tokens."""
    for token in misc.flattened(single_token or utils.all_tokens({'ldap_password'})):
        LOGGER.info('Processing %s', token)
        token_secret = secrets.secret(token)
        meta = secrets.secret(f'{token}#')
        ldap_login(f'ldap://{meta["ldap_server"]}', meta['dn'], token_secret)


def process(action: str, token: str = '') -> None:
    """Process actions."""
    match action:
        case 'update':
            update(token)
        case 'validate':
            validate(token)
