"""AWS credential management."""

import boto3
from cki_lib import logger
from cki_lib import misc

from cki.deployment_tools import secrets

from . import utils

LOGGER = logger.get_logger(__name__)

BOTO_SESSION = boto3.Session()


def update(single_token: str) -> None:
    """Update the secret meta information about AWS tokens."""
    current_account = BOTO_SESSION.client('sts').get_caller_identity()['Account']
    iam_client = BOTO_SESSION.client('iam')
    for token in misc.flattened(single_token or utils.all_tokens({'aws_secret_access_key'})):
        LOGGER.info('Processing %s', token)
        meta = secrets.secret(f'{token}#')
        if endpoint_url := meta.get('endpoint_url'):
            LOGGER.info('Token meta update for %s not supported', endpoint_url)
            continue
        if not meta.get('account') or not meta.get('arn'):
            response = BOTO_SESSION.client(
                'sts',
                aws_access_key_id=meta['access_key_id'],
                aws_secret_access_key=secrets.secret(f'{token}'),
            ).get_caller_identity()
            meta.update({
                'account': response['Account'],
                'arn': response['Arn'],
            })
        if meta['account'] != current_account:
            LOGGER.info('Token account %s != %s', meta['account'], current_account)
            continue
        if not meta.get('user_name'):
            response = iam_client.get_access_key_last_used(AccessKeyId=meta['access_key_id'])
            meta['user_name'] = response['UserName']
        response = next(
            r for r in iam_client.list_access_keys(UserName=meta['user_name'])['AccessKeyMetadata']
            if r['AccessKeyId'] == meta['access_key_id'])
        meta.update({
            'active': response['Status'] == 'Active',
            'created_at': misc.ensure_tz_utc(response['CreateDate']).isoformat(),
        })
        secrets.edit(f'{token}#', meta)


def process(action: str, token: str = '') -> None:
    """Process actions."""
    match action:
        case 'update':
            update(token)
