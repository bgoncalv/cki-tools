"""Monitor queues and scale up/down services."""
from cki_lib import misc
import sentry_sdk

from . import main

misc.sentry_init(sentry_sdk)
main()
