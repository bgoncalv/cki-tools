"""Submit KCIDB data to DataWarehouse."""
from cki_lib import misc
import sentry_sdk

from . import main

misc.sentry_init(sentry_sdk)
main()
