#!/bin/bash

set -Eeuo pipefail
shopt -s inherit_errexit

# shellcheck disable=SC1091
. cki_utils.sh

if (( $# < 3 )); then
    echo "Usage: ${0##*/} <name> <group> <rotation>"
    exit 1
fi

cki_say "backup"

cki_echo_yellow "Backup of $1"
echo "  Type: $2"
echo "  Rotation: $3"

timestamp=$(date +'%F-%H-%M')
if ! openstack server backup create "$1" \
    --name "$1-backup-${timestamp}-$2" \
    --type "$2" \
    --rotate "$3" \
    --wait; then
    cki_echo_red "Backup failed"
fi

cki_echo_green "Backup completed successfully"
