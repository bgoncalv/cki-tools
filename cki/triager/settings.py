"""Settings file."""
import os

from cki_lib import misc
from datawarehouse import Datawarehouse

DATAWAREHOUSE_URL = os.environ.get('DATAWAREHOUSE_URL')
DATAWAREHOUSE_TOKEN = os.environ.get('DATAWAREHOUSE_TOKEN_TRIAGER')
DW_CLIENT = Datawarehouse(DATAWAREHOUSE_URL, DATAWAREHOUSE_TOKEN)

# When regex matching takes more than the expected time, an error is logged
REGEX_EXPECTED_TIME = misc.parse_timedelta(os.environ.get('REGEX_EXPECTED_TIME', '1s'))
# When regex matching takes long enough, an expection is raised
REGEX_TIMEOUT = misc.parse_timedelta(os.environ.get('REGEX_TIMEOUT', '60s'))

# Max content-length, in bytes, the triager is using of a file, ie. the file is truncated
MAX_CONTENT_LENGTH = misc.get_env_int("MAX_CONTENT_LENGTH", 100_000_000)

DOWNLOAD_LOG_RETRIES = misc.get_env_int("DOWNLOAD_LOG_RETRIES", 0)
DOWNLOAD_LOG_TIMEOUT = misc.get_env_int("DOWNLOAD_LOG_TIMEOUT", 10)
