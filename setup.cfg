[metadata]
name = cki-tools
description = "CKI command line tools"
long_description = file: README.md
version = 1
author = Red Hat, Inc.
license = GPLv2+

[options]
packages = find:
# Parse the MANIFEST.in file and include those files, too.
include_package_data = True
install_requires =
    boto3
    cki-lib @ git+https://gitlab.com/cki-project/cki-lib.git@production
    datawarehouse-api-lib @ git+https://gitlab.com/cki-project/datawarehouse-api-lib.git@production
    GitPython
scripts =
    shell-scripts/cki_deployment_acme.sh
    shell-scripts/cki_deployment_clean_docker_images.sh
    shell-scripts/cki_deployment_codeowners_mr.sh
    shell-scripts/cki_deployment_grafana_backup.sh
    shell-scripts/cki_deployment_grafana_mr.sh
    shell-scripts/cki_deployment_osp_backup.sh
    shell-scripts/cki_deployment_pgsql_backup.sh
    shell-scripts/cki_deployment_pgsql_restore.sh
    shell-scripts/cki_deployment_git_s3_sync.sh
    shell-scripts/cki_tools_git_cache_updater.sh
    shell-scripts/cki_tools_kernel_config_updater.sh
    shell-scripts/cki_tools_scip_python.sh
    shell-scripts/cki_tools_sync_polarion_id_mr.sh
    shell-scripts/cki_tools_update_ystream_composes_mr.sh

[options.extras_require]
dev =
    cryptography
    freezegun
    responses
    types-PyYAML
    # If sentry-sdk is already installed, the [flask] extra dependencies are not installed.
    # Workaround by forcing the dependencies to be installed.
    Flask
    blinker
webhook_receiver =
    # If sentry-sdk is already installed, the [flask] extra dependencies are not installed.
    # Workaround by forcing the dependencies to be installed.
    Flask
    gunicorn
    sentry-sdk[flask]
koji_trigger =
    koji
message_trigger =
    jmespath
datawarehouse_kcidb_forwarder =
    kcidb @ git+https://gitlab.com/cki-project/mirror/kcidb.git@production
deployment_tools =
    Jinja2
    requests-gssapi
    toml
amqp_bridge =
    cli-proton-python
    cki-lib[crypto] @ git+https://gitlab.com/cki-project/cki-lib.git@production
    # If cki-lib is already installed, the [crypto] extra dependencies are not installed.
    # Workaround by forcing cryptography to be installed.
    cryptography
service_metrics =
    crontab
    mysql-connector-python
beaker_broken_machines =
    mysql-connector-python
credential_manager =
    cryptography
    diceware
    python-ldap
gitlab_sso_login =
    requests-gssapi
datawarehouse_utils =
    python-bugzilla
pipeline_herder =
    responses
slack_bot =
    Flask
    gunicorn
    sentry-sdk[flask]
umb =
    # If cki-lib is already installed, the [crypto,umb] extra dependencies are not installed.
    # Workaround by forcing cryptography/stomp to be installed.
    cki-lib[crypto,umb] @ git+https://gitlab.com/cki-project/cki-lib.git@production
    cryptography
    prometheus-client
    stomp.py

[options.packages.find]
exclude =
    tests*
    inttests*

[options.entry_points]
console_scripts =
    cki_aws_login = cki_tools.aws:login_cli
    cki_edit_secret = cki.deployment_tools.secrets:edit_cli
    cki_gitlab_yaml_shellcheck = cki_tools.gitlab_yaml_shellcheck:main
    cki_secret = cki.deployment_tools.secrets:secret_cli
    cki_secrets_login = cki.deployment_tools.secrets:login_cli
    cki_secrets_logout = cki.deployment_tools.secrets:logout_cli
    cki_secrets_validate = cki.deployment_tools.secrets:validate_cli
    cki_variable = cki.deployment_tools.secrets:variable_cli

[tox:tox]
envlist = full

[testenv]
commands =
    cki_lint.sh cki cki_tools
    cki_test.sh cki cki_tools

[testenv:full]
extras =
    amqp_bridge
    autoscaler
    credential_manager
    datawarehouse_kcidb_forwarder
    datawarehouse_utils
    deployment_tools
    dev
    gitlab_sso_login
    koji_trigger
    pipeline_herder
    service_metrics
    slack_bot
    umb
    webhook_receiver

[testenv:lint]
extras = {[testenv:full]extras}
commands = cki_lint.sh cki cki_tools

[testenv:test]
extras = {[testenv:full]extras}
commands = cki_test.sh cki cki_tools

[testenv:inttests]
extras =
    deployment_tools
    dev
set_env =
    CKI_COVERAGE_CHECK_ENABLED=false
    CKI_DISABLED_LINTERS=all
    CKI_PYTEST_IGNORELIST=tests/ inttests/images/

[testenv:image]
extras =
    dev
set_env =
    CKI_COVERAGE_ENABLED=false
    CKI_DISABLED_LINTERS=all
    CKI_PYTEST_ARGS=--verbose -r s inttests/images/{env:IMAGE_NAME}

[testenv:pipeline]
extras = {[testenv:full]extras}
set_env =
    CKI_COVERAGE_CHECK_ENABLED=false
    CKI_DISABLED_LINTERS=all
install_command = python -I -m pip install --constraint constraints.txt {opts} {packages}

[testenv:scip]
extras = {[testenv:full]extras}
commands = cki_tools_scip_python.sh

[flake8]
exclude=.direnv,.git,.tox*

[pylint.MAIN]
extension-pkg-whitelist=falcon

[pylint.SIMILARITIES]
ignore-imports=yes
disable = duplicate-code

[coverage:report]
exclude_lines =
    if typing.TYPE_CHECKING:
    if __name__ == .__main__.:
    pragma: no cover
