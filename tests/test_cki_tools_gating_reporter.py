"""Tests for OSCI gating messages."""
from importlib import resources
import json
import os
from pathlib import Path
import unittest
from unittest import mock

from cki_lib import yaml
from freezegun import freeze_time
import responses

from cki_tools import gating_reporter

from . import assets
from .utils import b64decode_and_decompress

CHECKOUT_ID = "redhat:123"

DATAWAREHOUSE_ENVIRON = {
    "DATAWAREHOUSE_URL": "https://host",
    "DATAWAREHOUSE_TOKEN_GATING_REPORTER": "datawarehouse_token",
}


class TestGating(unittest.TestCase):
    """Tests for OSCI gating messages."""

    def test_compress_and_b64encode(self):
        """Test for compress_and_b64encode."""
        test_cases = ['foo', 'bar', 'baz', '123']
        for data in test_cases:
            result = gating_reporter.compress_and_b64encode(data)
            self.assertTrue(isinstance(result, str))
            self.assertEqual(b64decode_and_decompress(result), data)

    def test_process_message(self):
        """Verify ready_to_report messages are handled correctly."""
        cases = (
            ('ready_to_report', {
                'status': 'ready_to_report',
                'object_type': 'checkout',
                'object': {'id': CHECKOUT_ID},
            }, 'complete'),
            ('checkout_issueoccurrences_changed', {
                'status': 'checkout_issueoccurrences_changed',
                'object_type': 'checkout',
                'object': {'id': CHECKOUT_ID},
            }, 'complete'),
            ('build_setups_finished', {
                'status': 'build_setups_finished',
                'object_type': 'checkout',
                'object': {'id': CHECKOUT_ID},
            }, 'running'),
            ('unknown status', {
                'status': 'foo',
                'object_type': 'checkout',
                'object': {'id': CHECKOUT_ID},
            }, False),
            ('no checkout', {
                'status': 'ready_to_report',
                'object_type': 'build',
                'object': {'id': 'redhat:15'},
            }, False),
            ('retriggered', {
                'status': 'ready_to_report',
                'object_type': 'checkout',
                'object': {'id': CHECKOUT_ID, 'misc': {'retrigger': True}},
            }, False),
        )

        for description, payload, reports in cases:
            with (self.subTest(description),
                  mock.patch('cki_tools.gating_reporter.report') as mock_report):
                gating_reporter.process_message(body=payload)
                if reports:
                    mock_report.assert_called_once_with(reports, CHECKOUT_ID)
                else:
                    mock_report.assert_not_called()

    def test__status_to_osci_dashboard_format(self):
        """Test for _status_to_osci_dashboard_format."""
        with self.subTest("FAIL returns failed"):
            self.assertEqual('failed', gating_reporter._status_to_osci_dashboard_format('FAIL'))
        with self.subTest("Any other input value than 'FAIL' returns 'passed'"):
            self.assertEqual('passed', gating_reporter._status_to_osci_dashboard_format('PASS'))
            self.assertEqual('passed', gating_reporter._status_to_osci_dashboard_format('foo'))

    def test_make_xunit_string(self):
        """Test for make_xunit_string."""
        self.maxDiff = None
        assets_dir = Path(__file__).resolve().parent / "assets"

        with self.subTest("Return None if no tests"):
            self.assertIsNone(gating_reporter.make_xunit_string([], 'FAIL'))

        # Mock the kcidb tests
        with open(assets_dir / "test_results.json", encoding='utf-8') as file:
            kcidb_tests_mocks = [mock.MagicMock(**test) for test in json.load(file)]
            # Parse results into objects to simulate dw-api-lib instances
            for test in kcidb_tests_mocks:
                test.misc["results"] = [mock.MagicMock(**r) for r in test.misc["results"]]

        # Main test case
        with self.subTest("Test generated xunit XML matches expected"):
            xunit_data = gating_reporter.make_xunit_string(kcidb_tests_mocks, "FAIL")
            xunit_xml = b64decode_and_decompress(xunit_data).strip().replace("\n", "")

            # Expected xunit XML
            with open(assets_dir / "expected_xunit.xml", encoding="utf-8") as file:
                expected_xunit_xml = file.read().strip().replace("\t", "").replace("\n", "")

            self.assertEqual(xunit_xml, expected_xunit_xml)

        with self.subTest("Test without results should still show as failure"):
            for test in kcidb_tests_mocks:
                test.misc["results"] = []

            xunit_data = gating_reporter.make_xunit_string(kcidb_tests_mocks, "FAIL")
            xunit_xml = b64decode_and_decompress(xunit_data).strip().replace("\n", "")

            # Expected xunit XML
            with open(assets_dir / "expected_xunit_no_results.xml", encoding="utf-8") as file:
                expected_xunit_xml = file.read().strip().replace("\t", "").replace("\n", "")
            self.assertEqual(xunit_xml, expected_xunit_xml)

        # Subtest for the case when `make_xunit_string` fails
        with self.subTest("Test make_xunit_string fails and logs error"):
            with self.assertLogs(gating_reporter.LOGGER, 'ERROR') as log:
                xunit_data = gating_reporter.make_xunit_string('foo', 'FAIL')
                self.assertIn('An error occurred while generating the xunit string',
                              log.output[-1])
                self.assertIsNone(xunit_data)

    @responses.activate
    @mock.patch.dict(os.environ, {**DATAWAREHOUSE_ENVIRON})
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @freeze_time('2021-01-01T00:00:00.0+00:00')
    @mock.patch('cki_tools.gating_reporter.SUPPORTED_KOJI_INSTANCES', [{
        'web_url': 'https://brew', 'topic': 'brew-build', 'artifact_type': 'brew-build',
    }])
    @mock.patch("cki_lib.stomp.StompClient.send_message")
    def test_gating_messages(self, send_message):
        """Tests for the actual messages."""
        self.maxDiff = None

        checkout_url = f"https://host/api/1/kcidb/checkouts/{CHECKOUT_ID}"
        checkout_all_url = f"{checkout_url}/all"
        responses.get(checkout_url, json={"id": CHECKOUT_ID, "misc": {"iid": 1}})

        complete_nvr = {
            "kernel_version": "123.notatest",
            "source_package_name": "source-package-name",
            "source_package_version": "4.18.0",
            "source_package_release": "513.24.1.el9_2",
        }
        incomplete_nvr = {
            "kernel_version": "123.notatest",
        }

        cases = (
            ("complete", "complete", "osci-complete.yml", 123456, "https://brew/foo", complete_nvr),
            ("no task id", "complete", None, None, "https://brew/bar", complete_nvr),
            ("wrong provenance", "complete", None, 123456, "https://koji/baz", complete_nvr),
            ("incomplete nvr", "complete", None, 123456, "https://brew/foo", incomplete_nvr),
            ("running", "running", "osci-running.yml", 123456, "https://brew/qux", complete_nvr),
        )

        builds = [
            {
                "architecture": "x86_64",
                "id": "redhat:11",
                "misc": {"iid": 12347, "package_name": "kernel", "kpet_tree_name": "rhel92-z"},
            },
            {
                "architecture": "s390x",
                "id": "redhat:10",
                "misc": {
                    "iid": 12346,
                    "package_name": "kernel-rt",
                    "kpet_tree_name": "rhel92-z",
                    "debug": True,
                },
            },
            {
                "architecture": "zzz",
                "id": "redhat:15",
                "misc": {
                    "iid": 12346,
                    "package_name": "kernel-rt",
                    "kpet_tree_name": "rhel92-z",
                    "debug": False,
                },
            },
            {
                "architecture": "x86_64",
                "id": "redhat:12",
                "misc": {
                    "iid": 12348,
                    "package_name": "kernel",
                    "kpet_tree_name": "rhel92-z",
                    "debug": True,
                },
            },
        ]
        tests = [
            {
                "id": "redhat:10_upt_3",
                "build_id": "redhat:10",
                "status": "FAIL",
                "comment": "Boot test",
                "waived": False,
                "misc": {"iid": 1421},
            },
            {
                "id": "redhat:15_upt_8",
                "build_id": "redhat:15",
                "status": "PASS",
                "comment": "Reboot test",
                "waived": False,
                "misc": {"iid": 1459},
            },
            {
                "id": "redhat:12_upt_7",
                "build_id": "redhat:12",
                "status": "ERROR",
                "comment": "Remaster test",
                "waived": False,
                "misc": {"iid": 2412},
            },
        ]
        testresults = [
            {
                "id": "redhat:10_upt_3_1",
                "test_id": "redhat:10_upt_3",
                "status": "PASS",
                "comment": "distribution/kpkginstall/depmod-check",
                "misc": {"iid": 436152},
            },
            {
                "id": "redhat:10_upt_3_2",
                "test_id": "redhat:10_upt_3",
                "status": "FAIL",
                "comment": "Panic",
                "misc": {"iid": 562141},
            },
        ]
        issueoccurrences = []

        for description, message_type, asset, brew_task_id, web_url, nvr in cases:
            send_message.reset_mock()
            data = {
                "checkouts": [
                    {
                        "id": "redhat:checkout",
                        "misc": {
                            "iid": 1112,
                            "scratch": False,
                            "provenance": [{"url": web_url}],
                            "brew_task_id": brew_task_id,
                            **nvr,
                        },
                        "contacts": ["joe@example.email"],
                    }
                ],
                "builds": builds,
                "tests": tests,
                "testresults": testresults,
                "issueoccurrences": issueoccurrences,
            }
            responses.upsert(responses.GET, checkout_all_url, json=data)

            with self.subTest(description):
                gating_reporter.report(message_type, CHECKOUT_ID)
                if not asset:
                    send_message.assert_not_called()
                else:
                    messages = [c.args[0] for c in send_message.mock_calls]

                    path_to_expected = resources.files(assets).joinpath(asset)
                    expected = yaml.load(contents=path_to_expected.read_text("utf8"))
                    self.assertEqual(messages, expected)
