"""Tests for the UMB messenger."""

from importlib import resources
import unittest
from unittest import mock

from cki_lib import yaml
from freezegun import freeze_time
import responses

from cki_tools import umb_messenger
from cki_tools.umb_messenger import jira

from . import assets
from .utils import mock_attrs


class TestUmbMessenger(unittest.TestCase):
    """Tests for the UMB messenger."""

    maxDiff = None

    def test_process_message(self):
        """Verify messages are sent correctly."""
        cases = (
            ('post_test', {
                'status': 'ready_to_report',
                'object_type': 'checkout',
                'object': {'id': 'redhat:123'},
            }, True),
            ('pre_test', {
                'status': 'build_setups_finished',
                'object_type': 'checkout',
                'object': {'id': 'redhat:123'},
            }, True),
            ('unknown status', {
                'status': 'foo',
                'object_type': 'checkout',
                'object': {'id': 'redhat:123'},
            }, False),
            ('no checkout', {
                'status': 'ready_to_report',
                'object_type': 'build',
                'object': {'id': 'redhat:15'},
            }, False),
            ('retriggered', {
                'status': 'ready_to_report',
                'object_type': 'checkout',
                'object': {'id': 'redhat:123', 'data': 'something', 'misc': {'retrigger': True}},
            }, False),
        )

        for description, payload, reports in cases:
            with (self.subTest(description),
                  mock.patch('cki_tools.umb_messenger.report') as mock_report):
                umb_messenger.process_message(body=payload)
                if reports:
                    mock_report.assert_called_with(description, 'redhat:123')
                else:
                    mock_report.assert_not_called()

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch.dict('os.environ', {
        'DATAWAREHOUSE_URL': 'https://host.url',
        'JIRA_SERVER': 'https://this.is.jira',
        'JIRA_TOKEN_AUTH': 'token',
        "DATAWAREHOUSE_TOKEN_UMB_MESSENGER": "token"
    })
    @freeze_time('2021-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_messages(self):
        """Tests for the actual messages."""
        checkout_id = "redhat:1"
        checkout_url = f"https://host.url/api/1/kcidb/checkouts/{checkout_id}"
        checkout_all_url = checkout_url + "/all"

        responses.get(checkout_url, json={"id": checkout_id, "misc": {"iid": 1112}})

        responses.get(
            "https://host/api/v4/projects/g%2Fp/merge_requests/8",
            json={
                "iid": 123,
                "description": (
                    "Draft: my mr\nBugzilla: https://bz\nrandom line"
                    "with a Bugzilla: do-not-extract\nBugzilla:"
                    "http://extract-this  \nSigned-off-by: myself\n"
                    "JIRA: https://this.is.jira/RHEL-123"
                ),
                "labels": [
                    "randomlabel",
                    "Bugzilla::NeedsReview",
                    "Acks::NeedsReview",
                    "Subsystem:scsi",
                    "Subsystem:qla2xxx",
                ],
                "work_in_progress": True,
            },
        )
        responses.get('https://this.is.jira/rest/api/2/search', json={'issues': [{
            'fields': {'customfield_12315948': {'emailAddress': 'qa@contact.com'}},
        }]})

        cases = (
            ('pre_test', 'pre_test', [], 'pre-test.yml'),
            ('pre_test', 'pre_test', ['mr'], 'pre-test-mr.yml'),
            ('not valid', 'pre_test', ['invalid'],  None),
            ('no builds', 'pre_test', ['nobuilds'], None),
            ('no output files', 'pre_test', ['nooutput'], None),
            ('no vr', 'pre_test', ['novr'], 'pre-test-novr.yml'),
            ('post_test', 'post_test', [], 'post-test.yml'),
            ('post_test no tests', 'post_test', ['notests'], 'post-test.yml'),
            ('post_test error', 'post_test', ['error'], 'post-test-error.yml'),
            ('post_test error waived', 'post_test', ['error', 'waived'], 'post-test.yml'),
            ('post_test fail', 'post_test', ['fail'], 'post-test-fail.yml'),
            ('post_test fail waived', 'post_test', ['fail', 'waived'], 'post-test.yml'),
            ('post_test boot', 'post_test', ['error', 'boot'], 'post-test-fail.yml'),
        )

        for description, message_type, deviations, asset in cases:
            checkout = mock_attrs(
                contacts=["joe@example.email"],
                git_repository_branch="main",
                id=checkout_id,
                misc={
                    "kernel_version": "123.test",
                    "source_package_name": "kernel-source",
                    "iid": 1112,
                    "patchset_modified_files": [
                        {"path": "list"},
                        {"path": "of"},
                        {"path": "files"},
                    ],
                }
                | (
                    {}
                    if "novr" in deviations
                    else {
                        "source_package_version": "6.7.0",
                        "source_package_release": "0.rc8.something.fc39",
                    }
                ),
                valid="invalid" not in deviations,
            )
            if 'mr' in deviations:
                checkout["misc"]["related_merge_request"] = {
                    "url": "https://host/g/p/-/merge_requests/8",
                    "diff_url": "https://link.to.diff",
                }

            builds = (
                []
                if "nobuilds" in deviations
                else [
                    mock_attrs(
                        architecture="s390x",
                        id="redhat:10",
                        checkout_id=checkout_id,
                        output_files=None
                        if "nooutput" in deviations
                        else [{"name": "kernel_package_url", "url": "link"}],
                        misc={
                            "iid": 12346,
                            "package_name": "kernel-debug",
                            "kpet_tree_name": "fedora",
                            "debug": True,
                        }
                        | (
                            {}
                            if "novr" in deviations
                            else {
                                "package_version": "6.7.0",
                                "package_release": "0.rc8.something.fc39",
                            }
                        ),
                    ),
                    mock_attrs(
                        architecture="x86_64",
                        id="redhat:12",
                        checkout_id=checkout_id,
                        output_files=[
                            {"name": "kernel_package_url", "url": "link2"},
                        ],
                        misc={
                            "iid": 12348,
                            "package_name": "kernel",
                            "kpet_tree_name": "fedora",
                        }
                        | (
                            {}
                            if "novr" in deviations
                            else {
                                "package_version": "6.7.0",
                                "package_release": "0.rc8.something.fc39",
                            }
                        ),
                    ),
                    mock_attrs(
                        architecture="ppc64le",
                        id="redhat:13",
                        checkout_id=checkout_id,
                        output_files=[
                            {"name": "kernel_package_url", "url": "link3"},
                        ],
                        misc={
                            "iid": 12347,
                            "package_name": "kernel",
                            "kpet_tree_name": "fedora",
                            "testing_skipped_reason": "unsupported",
                        }
                        | (
                            {}
                            if "novr" in deviations
                            else {
                                "package_version": "6.7.0",
                                "package_release": "0.rc8.something.fc39",
                            }
                        ),
                    ),
                    mock_attrs(
                        architecture="noarch",
                        id="redhat:14",
                        checkout_id=checkout_id,
                        output_files=[],
                        misc={
                            "iid": 12349,
                            "package_name": "kernel",
                            "kpet_tree_name": "fedora",
                        },
                    ),
                ]
            )

            tests = (
                []
                if "notests" in deviations
                else [
                    mock_attrs(
                        id="redhat:15-1",
                        build_id="redhat:15",
                        status="ERROR" if "error" in deviations else "pass",
                        comment="Boot test" if "boot" in deviations else "Some test",
                        waived="waived" in deviations,
                    ),
                    mock_attrs(
                        id="redhat:16-1",
                        build_id="redhat:16",
                        status="FAIL" if "fail" in deviations else "pass",
                        waived="waived" in deviations,
                    ),
                ]
            )
            mocked_all = {
                "checkouts": [checkout],
                "builds": builds,
                "tests": tests,
                "testresults": [],
                "issueoccurrences": [],
            }
            responses.upsert(responses.GET, checkout_all_url, json=mocked_all)

            with (
                self.subTest(description),
                mock.patch("cki_lib.stomp.StompClient.send_message") as send_message,
            ):
                log_assertion = self.assertLogs if 'nooutput' in deviations else self.assertNoLogs
                with log_assertion(logger=umb_messenger.LOGGER, level="WARNING") as log_ctx:
                    umb_messenger.report(message_type, checkout_id)
                if asset is not None:
                    self.assertEqual(send_message.mock_calls[0].args[0], yaml.load(
                        contents=resources.files(assets).joinpath(asset).read_text('utf8')))
                else:
                    send_message.assert_not_called()

                if 'nooutput' in deviations:
                    expected_log = (
                        f"WARNING:{umb_messenger.LOGGER.name}:No data available for redhat:1!"
                    )
                    self.assertIn(expected_log, log_ctx.output)

    @responses.activate
    def test_qa_contacts(self) -> None:
        """Tests for the QA contacts."""
        responses.get('https://this.is.jira/rest/api/2/search', json={'issues': [{
            'fields': {'customfield_12315948': {'emailAddress': 'qa@contact.com'}},
        }]})
        cases: list[tuple[str, dict[str, str], list[str], list[str]]] = [
            ('default', {
                'JIRA_SERVER': 'https://this.is.jira',
                'JIRA_TOKEN_AUTH': 'token',
            }, ['https://this.is.jira/RHEL-123'], ['qa@contact.com']),
            ('no token', {
                'JIRA_SERVER': 'https://this.is.jira',
            }, ['https://this.is.jira/RHEL-123'], []),
            ('no server', {
                'JIRA_TOKEN_AUTH': 'token',
            }, ['https://this.is.jira/RHEL-123'], []),
            ('different project', {
                'JIRA_SERVER': 'https://this.is.jira',
                'JIRA_TOKEN_AUTH': 'token',
            }, ['https://this.is.jira/POOH-123'], []),
        ]
        for description, env, urls, expected_contacts in cases:
            with self.subTest(description), mock.patch.dict('os.environ', env):
                self.assertEqual(jira.qa_contacts(urls), expected_contacts)
