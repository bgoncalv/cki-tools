"""Tests for update ystream compose in kpet-db."""
from importlib import resources
import json
import pathlib
import tempfile
import unittest
from unittest import mock

from cki_lib import yaml
import responses

from cki_tools import update_ystream_composes
from tests.assets import update_ystream_composes as assets


class TestYstreamCompose(unittest.TestCase):
    """Tests for update ystream compose in kpet-db."""

    def test_update_tree(self):
        """Test update_ystream_composes.update_tree."""
        with tempfile.TemporaryDirectory() as directory:
            path = pathlib.Path(directory) / 'tree-before.yml.j2'
            path.write_text(
                (resources.files(assets) / 'tree-before.yml.j2').read_text('utf8'), 'utf8')
            update_ystream_composes.update_tree(path, {
                'distro_name': 'RHEL-new.test',
                'buildroot_name': 'BUILDROOT-new.test',
            })
            self.assertEqual(path.read_text('utf8'),
                             (resources.files(assets) / 'tree-after.yml.j2').read_text('utf8'))

    @mock.patch('subprocess.run')
    @mock.patch.dict('os.environ', {'CTS_URL': 'https://cts'})
    @responses.activate
    def test_main_update(self, mock_run):
        """Test for update_ystream_composes."""
        mock_bkr_out = (resources.files(assets) / 'beaker-output.json').read_text('utf8')
        mock_run.return_value = mock.MagicMock(returncode=0, stdout=mock_bkr_out)
        responses.get('https://cts/api/1/composes/compose-new.test',
                      json={'children': ['BUILDROOT-new.test']})

        trees = ('rhel-test.j2', 'eln-test.j2')
        with tempfile.TemporaryDirectory() as directory:
            trees_dir = pathlib.Path(directory) / 'trees'
            trees_dir.mkdir()
            for tree in trees:
                (trees_dir / tree).write_text(
                    (resources.files(assets) / tree).read_text('utf8'), 'utf8')

            update_ystream_composes.main([
                '--config', (resources.files(assets) / 'config.yaml').read_text('utf8'),
                '--kpet-db', directory,
                'update',
            ])
            bkr_rhel_params = ['bkr', 'distros-list', '--limit', '1', '--format', 'json', '--name',
                               'RHEL-new', '--tag=CTS_NIGHTLY', '--tag=CTS_integration',
                               '--tag=CTS_beaker-available']
            bkr_eln_params = ['bkr', 'distros-list', '--limit', '1', '--format', 'json', '--name',
                              'ELN-new']
            mock_run.assert_any_call(bkr_rhel_params, capture_output=True, check=True)
            mock_run.assert_any_call(bkr_eln_params, capture_output=True, check=True)
            for tree in trees:
                self.assertEqual((trees_dir / tree).read_text('utf8'),
                                 (resources.files(assets) / f'{tree}-after').read_text('utf8'))

    @mock.patch.dict('os.environ', {'CTS_URL': 'https://cts'})
    @responses.activate
    def test_main_tag(self):
        """Test for update_ystream_composes."""

        for is_production in (True, False):
            with (self.subTest(is_production=is_production),
                  tempfile.TemporaryDirectory() as directory,
                  mock.patch('cki_lib.misc.is_production', return_value=is_production),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                rsps.get('https://cts/api/1/composes/?tag=pooh-bear',
                         json=yaml.load(file_path=resources.files(assets) / 'list-composes.yml'))

                if is_production:
                    patch_eln = rsps.patch('https://cts/api/1/composes/eln-compose')
                    patch_rhel = rsps.patch('https://cts/api/1/composes/rhel-compose')
                    patch_one = rsps.patch('https://cts/api/1/composes/one-tagged-compose')
                    patch_another = rsps.patch('https://cts/api/1/composes/another-tagged-compose')

                trees = ('rhel-test.j2', 'eln-test.j2')
                trees_dir = pathlib.Path(directory) / 'trees'
                trees_dir.mkdir()
                for tree in trees:
                    (trees_dir / tree).write_text(
                        (resources.files(assets) / tree).read_text('utf8'), 'utf8')

                update_ystream_composes.main([
                    '--config', (resources.files(assets) / 'config.yaml').read_text('utf8'),
                    '--kpet-db', directory,
                    'tag', 'pooh-bear',
                    '--user-data', 'reason',
                ])

                if is_production:
                    self.assertEqual(json.loads(patch_eln.calls[0].request.body),
                                     {'action': 'tag', 'tag': 'pooh-bear', 'user_data': 'reason'})
                    self.assertEqual(json.loads(patch_rhel.calls[0].request.body),
                                     {'action': 'tag', 'tag': 'pooh-bear', 'user_data': 'reason'})
                    self.assertEqual(json.loads(patch_one.calls[0].request.body),
                                     {'action': 'untag', 'tag': 'pooh-bear', 'user_data': 'reason'})
                    self.assertEqual(json.loads(patch_another.calls[0].request.body),
                                     {'action': 'untag', 'tag': 'pooh-bear', 'user_data': 'reason'})

    @responses.activate
    def test_main_validate(self):
        """Test for schema validation."""

        config = yaml.load(file_path=resources.files(assets) / 'config.yaml')
        with self.subTest('valid'):
            update_ystream_composes.main(['--config', json.dumps(config), 'validate'])

        invalid_config = config | {'invalid': {}}
        with self.subTest('invalid'), self.assertRaises(Exception):
            update_ystream_composes.main(['--config', json.dumps(invalid_config), 'validate'])
