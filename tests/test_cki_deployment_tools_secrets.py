"""Tests for the cki.deployment_tools.secrets module."""
import contextlib
import io
import json
import os
import pathlib
import tempfile
import typing
import unittest
from unittest import mock

from cki_lib import yaml
from freezegun import freeze_time
import requests
import responses

from cki.deployment_tools import secrets


@freeze_time('2000-01-01T00:00:00.0+00:00')
class TestSecrets(unittest.TestCase):
    """Test yaml utils."""

    @staticmethod
    @contextlib.contextmanager
    def _setup_secrets(
        variables: typing.Dict[str, typing.Any],
    ) -> typing.Iterator[str]:
        secrets._read_secrets_file.cache_clear()  # pylint: disable=protected-access
        with tempfile.TemporaryDirectory() as directory:
            for name, values in variables.items():
                data = yaml.dump(values) if isinstance(values, dict) else values
                pathlib.Path(directory, name).write_text(data, encoding='utf8')
            yield directory

    def test_variables(self) -> None:
        """Check that basic variable processing works."""
        with self._setup_secrets({
                'internal.yml': {'bear': 0, 'pooh': 'qux', 'sec': 'none'},
        }) as directory, mock.patch.dict(os.environ, {
            'CKI_VARS_FILE': f'{directory}/internal.yml',
        }):
            self.assertEqual(secrets.variable('bear'), 0)
            self.assertEqual(secrets.variable('pooh'), 'qux')
            self.assertRaises(Exception, secrets.variable, 'crow')

    @mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://host', 'VAULT_TOKEN': 'token'})
    @responses.activate
    def test_secrets(self) -> None:
        """Check that basic secret processing works."""
        responses.get('https://host/v1/apps/data/cki/pooh',
                      json={'data': {'data': {'value': 'bear', 'field': 'crow'}}})
        responses.get('https://host/v1/apps/data/cki/qux',
                      json={'data': {'data': {'value': 'bear', 'field': 'crow'}}})

        cases = (
            ('all', 'pooh:', {'value': 'bear', 'field': 'crow'}, {'pooh': {}}),
            ('meta key', 'pooh#pooh', 'lis', {'pooh': {'meta': {'pooh': 'lis'}}}),
            ('meta all', 'pooh#', {'pooh': 'lis'}, {'pooh': {'meta': {'pooh': 'lis'}}}),
            ('hv default key', 'pooh', 'bear', {'pooh': {'backend': 'hv'}}),
            ('hv specified key', 'pooh:field', 'crow', {'pooh': {'backend': 'hv'}}),
            ('hv hash', 'pooh:', {'value': 'bear', 'field': 'crow'}, {'pooh': {'backend': 'hv'}}),
            ('hv unknown key', 'pooh:unknown-field', None, {'pooh': {'backend': 'hv'}}),
            ('hv missing meta', 'qux', None, {}),
            ('hv ignored explicit yaml', 'pooh', None, {'pooh': {'backend': 'yaml'}}),
            ('unknown backend', 'pooh', None, {'pooh': {'backend': 'other'}}),
        )

        for description, key, expected, data in cases:
            with (
                self.subTest(description),
                self._setup_secrets({'secrets.yml': data}) as directory,
                mock.patch.dict(os.environ, {'CKI_SECRETS_FILE': f'{directory}/secrets.yml'}),
            ):
                if expected is None:
                    self.assertRaises(Exception, secrets.secret, key)
                else:
                    self.assertEqual(secrets.secret(key), expected)

    @mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://host', 'VAULT_TOKEN': 'token'})
    @responses.activate
    def test_secrets_filtering(self) -> None:
        """Check that secrets filtering works."""
        responses.get('https://host/v1/apps/data/cki/sec/a',
                      json={'data': {'data': {'value': 'a'}}})
        responses.get('https://host/v1/apps/data/cki/sec/b',
                      json={'data': {'data': {'value': 'b'}}})
        responses.get('https://host/v1/apps/data/cki/sec/c',
                      json={'data': {'data': {'value': 'c'}}})
        cases = (
            ('empty', 'sec[]', ['a', 'b'],
             {'a': {'active': True}, 'b': {'active': False}}),
            ('simple', 'sec[active]', ['a'],
             {'a': {'active': True}, 'b': {'active': False}}),
            ('negated', 'sec[!active]', ['b'],
             {'a': {'active': True}, 'b': {'active': False}}),
            ('negated default', 'sec[!active]', ['b'],
             {'a': {'active': True}, 'b': {}}),
            ('multiple', 'sec[active]', ['a', 'b'],
             {'a': {'active': True}, 'b': {'active': True}, 'c': {}}),
            ('multiple conditions', 'sec[active,!revoked]', ['a'],
             {'a': {'active': True}, 'b': {'active': True, 'revoked': True}, 'c': {}}),
            ('meta', 'sec[active]#id', [1],
             {'a': {'active': True, 'id': 1}, 'b': {}}),
        )
        for description, key, expected, metas in cases:
            with self.subTest(description), self._setup_secrets({'secrets.yml': {
                f'sec/{k}': {'meta': v} for k, v in metas.items()
            }}) as directory, mock.patch.dict(os.environ, {
                'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
            }):
                self.assertEqual(secrets.secret(key), expected)

    def test_edit_secrets(self) -> None:
        """Check that secrets (metadata) can be edited."""
        cases = (
            ('no file', 'key#pooh', 'qux',
             None,
             {'key': {'meta': {'pooh': 'qux'}}}),
            ('no env var', 'key', 'bear',
             None,
             None),
            ('meta', 'key#pooh', 'qux',
             {'key': {}},
             {'key': {'meta': {'pooh': 'qux'}}}),
            ('meta dict', 'key#', '{pooh: bear, crow: bear}',
             {'key': {}},
             {'key': {'meta': {'crow': 'bear', 'pooh': 'bear'}}}),
            ('delete meta via empty string', 'key#', None,
             {'key': {'backend': 'hv', 'meta': {'pooh': 'qux'}}},
             {'key': {'backend': 'hv', }}),
            ('delete meta via null string', 'key#', 'null',
             {'key': {'backend': 'hv', 'meta': {'pooh': 'qux'}}},
             {'key': {'backend': 'hv', }}),
            ('delete meta via None', 'key#', None,
             {'key': {'backend': 'hv', 'meta': {'pooh': 'qux'}}},
             {'key': {'backend': 'hv', }}),
            ('reset meta via {}', 'key#', {},
             {'key': {'meta': {'pooh': 'qux'}}},
             {'key': {'meta': {}}}),
            ('delete all via deleting data', 'key:', '',
             {'key': {'backend': 'hv'}},
             {}),
            ('delete all via deleting meta', 'key#', '',
             {'key': {'meta': {'pooh': 'qux'}}},
             {}),
            ('unknown backend', 'key', 'bear',
             {'key': {'backend': 'other'}},
             Exception),
        )
        for description, key, value, before, after in cases:
            with self.subTest(description), self._setup_secrets({
                    'secrets.yml': before,
            } if before is not None else {}) as directory, mock.patch.dict(os.environ, {
                'CKI_SECRETS_FILE': f'{directory}/secrets.yml' if after is not None else '',
            }):
                if after == Exception or after is None:
                    self.assertRaises(Exception, secrets.edit, key, value)
                else:
                    secrets.edit(key, value)
                    self.assertEqual(yaml.load(file_path=pathlib.Path(f'{directory}/secrets.yml')),
                                     after)

    @mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://host', 'VAULT_TOKEN': 'token'})
    @responses.activate
    def test_edit_secrets_hv(self) -> None:
        """Check that HV secrets can be edited."""
        cases = (
            ('default', {'key': {'backend': 'hv'}}, 'key', 'bear',
             1, {'other': 'pooh', 'value': 'bear'}, {'key': {
                 'backend': 'hv',
                 'meta': {'created_at': '2000-01-01T00:00:00+00:00'},
             }}),
            ('field', {'key': {'backend': 'hv'}}, 'key:field', 'bear',
             1, {'other': 'pooh', 'field': 'bear'}, {'key': {
                 'backend': 'hv',
                 'meta': {'created_at': '2000-01-01T00:00:00+00:00'},
             }}),
            ('override', {'key': {'backend': 'hv'}}, 'key:other', 'bear',
             1, {'other': 'bear'}, {'key': {
                 'backend': 'hv',
                 'meta': {'created_at': '2000-01-01T00:00:00+00:00'},
             }}),
            ('existing creation date', {'key': {
                'backend': 'hv',
                'meta': {'created_at': '1999-01-01T00:00:00+00:00'},
            }}, 'key:other', 'bear', 1, {'other': 'bear'}, {'key': {
                'backend': 'hv',
                'meta': {'created_at': '1999-01-01T00:00:00+00:00'},
            }}),
            ('404', {'key': {'backend': 'hv'}}, 'key', 'bear',
             None, {'value': 'bear'}, {'key': {
                 'backend': 'hv',
                 'meta': {'created_at': '2000-01-01T00:00:00+00:00'},
             }}),
            ('replace', {'key': {'backend': 'hv'}}, 'key:', {'field': 'bear'},
             0, {'field': 'bear'}, {'key': {
                 'backend': 'hv',
                 'meta': {'created_at': '2000-01-01T00:00:00+00:00'},
             }}),
            ('replace string', {'key': {'backend': 'hv'}}, 'key:', "{'field': 'bear'}",
             0, {'field': 'bear'}, {'key': {
                 'backend': 'hv',
                 'meta': {'created_at': '2000-01-01T00:00:00+00:00'},
             }}),
            ('remove', {'key': {'backend': 'hv'}}, 'key:', '',
             0, None, {}),
            ('missing', {}, 'key', 'bear',
             1, {'other': 'pooh', 'value': 'bear'}, {'key': {
                 'backend': 'hv',
                 'meta': {'created_at': '2000-01-01T00:00:00+00:00'},
             }}),
        )
        for (description, data, key, value, get_call_count, put_contents, after) in cases:
            with (self.subTest(description),
                  self._setup_secrets({'secrets.yml': data}) as directory,
                  mock.patch.dict(os.environ, {'CKI_SECRETS_FILE': f'{directory}/secrets.yml'}),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                if get_call_count is None:
                    get_url = rsps.get('https://host/v1/apps/data/cki/key', status=404)
                else:
                    get_url = rsps.get('https://host/v1/apps/data/cki/key',
                                       json={'data': {'data': {'other': 'pooh'}}})
                put_url = rsps.put('https://host/v1/apps/data/cki/key')

                secrets.edit(key, value)
                self.assertEqual(yaml.load(file_path=pathlib.Path(f'{directory}/secrets.yml')),
                                 after)
                self.assertEqual(len(get_url.calls),
                                 1 if get_call_count is None else get_call_count)
                if get_call_count:
                    self.assertEqual(get_url.calls[0].request.headers['X-Vault-Token'], 'token')
                self.assertEqual(len(put_url.calls), 1 if put_contents else 0)
                if put_contents:
                    self.assertEqual(put_url.calls[0].request.headers['X-Vault-Token'], 'token')
                    self.assertEqual(json.loads(put_url.calls[0].request.body)[
                                     'data'], put_contents)

    def test_secrets_invalid(self) -> None:
        """Check that invalid secret files fail."""
        with self._setup_secrets({
                'internal.yml': "['bear', 'crow']",
        }) as directory, mock.patch.dict(os.environ, {
            'CKI_VARS_FILE': f'{directory}/internal.yml',
        }):
            with self.assertRaises(Exception, msg='invalid'):
                secrets.variable('bear')

    @mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://host', 'VAULT_TOKEN': 'token'})
    @responses.activate
    def test_validate(self) -> None:
        """Check that HV secrets can be validated."""
        data = {
            'pooh/bear/both': {'backend': 'hv'},
            'pooh/meta': {'backend': 'hv'},
        }
        responses.add('LIST', 'https://host/v1/apps/metadata/cki/',
                      json={'data': {'keys': ['pooh/', 'hv']}})
        responses.add('LIST', 'https://host/v1/apps/metadata/cki/pooh/',
                      json={'data': {'keys': ['bear/']}})
        responses.add('LIST', 'https://host/v1/apps/metadata/cki/pooh/bear/',
                      json={'data': {'keys': ['both']}})
        with (self._setup_secrets({'secrets.yml': data}) as directory,
              mock.patch.dict(os.environ, {'CKI_SECRETS_FILE': f'{directory}/secrets.yml'})):

            missing_hv, missing_meta, missing_required_meta = secrets.validate()
            self.assertEqual(missing_hv, {'pooh/meta'})
            self.assertEqual(missing_meta, {'hv'})
            self.assertEqual(missing_required_meta, {'pooh/bear/both', 'pooh/meta'})

    @mock.patch.dict(os.environ, {
        'VAULT_ADDR': 'https://host',
    })
    @mock.patch('uuid.uuid4', mock.Mock(return_value='1234'))
    def test_login_oidc(self) -> None:
        """Check that logging in via OIDC works."""
        cases = (
            ('default duration', ['login', '--oidc'], 36000),
            ('custom duration', ['login', '--oidc', '--duration', '2h'], 7200),
        )

        for description, args, expected_duration in cases:
            with (self.subTest(description),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps,
                  tempfile.NamedTemporaryFile() as token, mock.patch(
                      'cki.deployment_tools.secrets.VAULT_TOKEN_PATH', pathlib.Path(token.name))):
                get_auth_url = rsps.put(
                    'https://host/v1/auth/oidc/oidc/auth_url',
                    json={'data': {'auth_url': 'https://auth_url'}})
                auth_url = rsps.get(
                    'https://auth_url', status=302,
                    headers={'Location': 'http://localhost:8250/oidc/callback?code=567&state=890'})
                callback = rsps.get(
                    'https://host/v1/auth/oidc/oidc/callback?client_nonce=1234&code=567&state=890',
                    json={'auth': {'client_token': 'token'}})
                renew = rsps.put('https://host/v1/auth/token/renew-self')

                secrets.main(args)

                self.assertEqual(json.loads(get_auth_url.calls[0].request.body), {
                    'client_nonce': '1234',
                    'redirect_uri': 'http://localhost:8250/oidc/callback',
                })
                self.assertEqual(len(auth_url.calls), 1)
                self.assertEqual(len(callback.calls), 1)
                self.assertEqual(json.loads(renew.calls[0].request.body), {
                    'increment': expected_duration,
                })
                self.assertEqual(renew.calls[0].request.headers['X-Vault-Token'], 'token')
                self.assertEqual(secrets.VAULT_TOKEN_PATH.read_text(encoding='utf8'), 'token')

    @mock.patch.dict(os.environ, {
        'VAULT_ADDR': 'https://host',
        'VAULT_APPROLE_SECRET_ID': 'approle-secret-id',
    })
    def test_login_approle(self) -> None:
        """Check that logging in via an approle works."""
        cases = (
            ('default duration', ['--approle', 'somerole'], 36000),
            ('custom duration', ['--approle', 'somerole', '--duration', '2h'], 7200),
        )

        for description, args, expected_duration in cases:
            with (self.subTest(description),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps,
                  tempfile.NamedTemporaryFile() as token, mock.patch(
                      'cki.deployment_tools.secrets.VAULT_TOKEN_PATH', pathlib.Path(token.name))):
                get_auth_url = rsps.put('https://host/v1/auth/approle/login',
                                        json={'auth': {'client_token': 'token'}})
                renew = rsps.put('https://host/v1/auth/token/renew-self')

                secrets.login_cli(args)

                self.assertEqual(json.loads(get_auth_url.calls[0].request.body), {
                    'role_id': 'somerole',
                    'secret_id': 'approle-secret-id',
                })
                self.assertEqual(json.loads(renew.calls[0].request.body), {
                    'increment': expected_duration,
                })
                self.assertEqual(renew.calls[0].request.headers['X-Vault-Token'], 'token')
                self.assertEqual(secrets.VAULT_TOKEN_PATH.read_text(encoding='utf8'), 'token')

    @mock.patch.dict(os.environ, {
        'VAULT_ADDR': 'https://host',
        'VAULT_APPROLE_SECRET_ID': 'approle-secret-id',
    })
    def test_logout(self) -> None:
        """Check that logging out works."""
        cases = (
            ('token file', 'token', None),
            ('token env', None, 'token'),
            ('wrong token', None, 'wrong token'),
            ('no token', None, None),
        )
        for description, token_file, token_env in cases:
            with (self.subTest(description),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps,
                  mock.patch.dict('os.environ', {'VAULT_TOKEN': token_env} if token_env else {}),
                  tempfile.NamedTemporaryFile() as token, mock.patch(
                      'cki.deployment_tools.secrets.VAULT_TOKEN_PATH', pathlib.Path(token.name))):
                if token_file:
                    secrets.VAULT_TOKEN_PATH.write_text(token_file, encoding='utf8')
                else:
                    secrets.VAULT_TOKEN_PATH.unlink()
                revoke = rsps.put(
                    'https://host/v1/auth/token/revoke-self',
                    status=200 if token_file == 'token' or token_env == 'token' else 400)

                secrets.logout_cli([])

                if token_file or token_env:
                    self.assertEqual(revoke.calls[0].request.headers['X-Vault-Token'],
                                     token_file or token_env)
                else:
                    self.assertEqual(len(revoke.calls), 0)
                self.assertFalse(secrets.VAULT_TOKEN_PATH.exists())

                # make NamedTemporaryFile happy
                secrets.VAULT_TOKEN_PATH.write_text('', encoding='utf8')

    @mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://host', 'VAULT_TOKEN': 'token'})
    @responses.activate
    def test_cli(self) -> None:
        """Check that CLI processing works."""
        responses.add('LIST', 'https://host/v1/apps/metadata/cki/', json={'data': {'keys': ['hv']}})
        responses.get('https://host/v1/apps/data/cki/sec',
                      json={'data': {'data': {'value': 'ure'}}})
        responses.put('https://host/v1/apps/data/cki/sec')
        cases = (
            ('var int', 'variable_cli', ['int'], '0\n'),
            ('var int json', 'variable_cli', ['int', '--json'], '0\n'),
            ('var string', 'variable_cli', ['str'], 'qux\n'),
            ('var string json', 'variable_cli', ['str', '--json'], '"qux"\n'),
            ('var list', 'variable_cli', ['list'], 'a\nb\n'),
            ('var list json', 'variable_cli', ['list', '--json'], '["a", "b"]\n'),
            ('secret', 'secret_cli', ['sec'], 'ure\n'),
            ('secret json', 'secret_cli', ['sec', '--json'], '"ure"\n'),
            ('secret list', 'secret_cli', ['sec[]'], 'ure\n'),
            ('secret list json', 'secret_cli', ['sec[]', '--json'], '["ure"]\n'),
            ('edit secret', 'edit_cli', ['sec', 'int'], ''),
            ('validate', 'validate_cli', [],
             'Missing in HashiCorp Vault: sec\n'
             'Missing in secrets meta data: hv\n'),
            ('main var int', 'main', ['variable', 'int'], '0\n'),
            ('main var int json', 'main', ['variable', 'int', '--json'], '0\n'),
            ('main var string', 'main', ['variable', 'str'], 'qux\n'),
            ('main var string json', 'main', ['variable', 'str', '--json'], '"qux"\n'),
            ('main secret', 'main', ['secret', 'sec'], 'ure\n'),
            ('main edit secret', 'main', ['edit', 'sec', 'int'], ''),
            ('main validate', 'main', ['validate'],
             'Missing in HashiCorp Vault: sec\n'
             'Missing in secrets meta data: hv\n'),
        )
        for description, method, args, expected in cases:
            with self.subTest(description), self._setup_secrets({
                    'internal.yml': {'int': 0, 'str': 'qux', 'list': ['a', 'b']},
                    'secrets.yml': {'sec': {
                        'backend': 'hv',
                        'meta': {'created_at': '2000-01-01T00:00:00+00:00'},
                    }},
            }) as directory, mock.patch.dict(os.environ, {
                'CKI_VARS_FILE': f'{directory}/internal.yml',
                'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
            }):
                with contextlib.redirect_stdout(output := io.StringIO()):
                    getattr(secrets, method)(args)
                self.assertEqual(output.getvalue(), expected)

    @mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://host', 'VAULT_TOKEN': 'token'})
    def test_cli_validate(self) -> None:
        """Check that CLI validate processing works."""
        cases = (
            ('matching', ['all'], {'all': {
                'backend': 'hv',
                'meta': {'created_at': '2000-01-01T00:00:00+00:00'},
            }},
                0, ''),
            ('missing in meta', ['vault'], {},
             1, 'Missing in secrets meta data: vault\n'),
            ('missing in vault', [], {'meta': {
                'backend': 'hv',
                'meta': {'created_at': '2000-01-01T00:00:00+00:00'},
            }},
                2, 'Missing in HashiCorp Vault: meta\n'),
            ('missing in both', ['vault'], {'meta': {
                'backend': 'hv',
                'meta': {'created_at': '2000-01-01T00:00:00+00:00'},
            }}, 2, 'Missing in HashiCorp Vault: meta\nMissing in secrets meta data: vault\n'),
            ('missing required', ['created'], {'created': {'backend': 'hv'}},
             2, 'Missing required meta data: created\n'),
        )
        for description, vault, meta, expected_result, expected_output in cases:
            with self.subTest(description), self._setup_secrets({
                    'secrets.yml': meta,
            }) as directory, mock.patch.dict(os.environ, {
                'CKI_VARS_FILE': f'{directory}/internal.yml',
                'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
            }), responses.RequestsMock(assert_all_requests_are_fired=False) as rsps:
                rsps.add('LIST', 'https://host/v1/apps/metadata/cki/',
                         json={'data': {'keys': vault}})
                with contextlib.redirect_stdout(output := io.StringIO()):
                    result = secrets.main(['validate'])
                self.assertEqual(output.getvalue(), expected_output)
                self.assertEqual(result, expected_result)

    @mock.patch.dict(os.environ, {
        'VAULT_ADDR': 'https://host',
        'VAULT_TOKEN': 'token',
    })
    def test_vault_retries(self) -> None:
        """Check that retries work as expected."""
        cases = (
            ('ok', [200], 0),
            ('retries', [403, 403, 403, 200], 0),
            ('mix with normal retries', [500, 403, 403, 500, 403, 200], 0),
            ('fails', [403, 403, 403, 403], 403),
        )
        for description, codes, expected in cases:
            with (self.subTest(description),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                for code in codes:
                    rsps.get('https://host/v1/apps/data/cki/pooh', status=code)
                if expected:
                    with self.assertRaises(requests.exceptions.HTTPError) as e:
                        secrets.vault_query('GET', 'pooh')
                    self.assertEqual(e.exception.response.status_code, expected)
                else:
                    secrets.vault_query('GET', 'pooh')
