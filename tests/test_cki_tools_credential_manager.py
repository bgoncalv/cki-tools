"""Tests for credential management."""

import contextlib
import datetime
from http import HTTPStatus
import io
import json
import os
import pathlib
import tempfile
import typing
import unittest
from unittest import mock

from cki_lib import config_tree
from cki_lib import misc
from cki_lib import yaml
from cryptography import x509
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from freezegun import freeze_time
from ldap import INVALID_CREDENTIALS
import responses

from cki.deployment_tools import secrets
from cki_tools.credential_manager import aws
from cki_tools.credential_manager import dogtag
from cki_tools.credential_manager import gitlab
from cki_tools.credential_manager import ldap
from cki_tools.credential_manager import metrics
from cki_tools.credential_manager import password
from cki_tools.credential_manager import splunk
from cki_tools.credential_manager import ssh
from cki_tools.credential_manager import utils


@mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'vault_token'})
class TestCredentialManager(unittest.TestCase):
    """Tests for credential management."""

    @staticmethod
    @contextlib.contextmanager
    def _setup_secrets(
        variables: typing.Dict[str, typing.Any],
    ) -> typing.Iterator:
        secrets._read_secrets_file.cache_clear()  # pylint: disable=protected-access
        with tempfile.TemporaryDirectory() as directory, mock.patch.dict(os.environ, {
            'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
        }):
            pathlib.Path(directory, 'secrets.yml').write_text(yaml.dump(variables), encoding='utf8')
            yield

    def test_all_tokens(self) -> None:
        """Test behavior of all_tokens."""
        cases = (
            ('default', ['t1', 't2'], {'t1'}, ['0']),
            ('multiple', ['t1'], {'t1', 't2'}, ['0']),
        )
        for description, tokens, types, expected in cases:
            with self.subTest(description), self._setup_secrets({
                str(i): {'meta': {'token_type': t}} for i, t in enumerate(tokens)
            }):
                self.assertEqual(utils.all_tokens(types), expected)

    def test_metrics(self) -> None:
        """Test behavior of metrics.process."""
        cases = (
            ('default', [
                {'token_type': 'gitlab_group_token', 'created_at': '2001-01-01'},
                {'token_type': 'gitlab_group_token', 'expires_at': '2000-01-01'},
            ], {
                'cki_token_created_at{active="true",deployed="true",name="0"} 9.783072e+08',
                'cki_token_expires_at{active="true",deployed="true",name="1"} 9.466848e+08',
            }),
            ('not deployed', [
                {'token_type': 'gitlab_group_token', 'created_at': '2001-01-01', 'deployed': False},
                {'token_type': 'gitlab_group_token', 'expires_at': '2000-01-01', 'deployed': False},
            ], {
                'cki_token_created_at{active="true",deployed="false",name="0"} 9.783072e+08',
                'cki_token_expires_at{active="true",deployed="false",name="1"} 9.466848e+08',
            }),
            ('not active', [
                {'token_type': 'gitlab_group_token', 'created_at': '2001-01-01', 'active': False},
                {'token_type': 'gitlab_group_token', 'expires_at': '2000-01-01', 'active': False},
            ], {
                'cki_token_created_at{active="false",deployed="true",name="0"} 9.783072e+08',
                'cki_token_expires_at{active="false",deployed="true",name="1"} 9.466848e+08',
            }),
            ('no field', [
                {'token_type': 'gitlab_group_token'},
                {'token_type': 'gitlab_group_token', 'created_at': '2001-01-01'},
                {'token_type': 'gitlab_group_token', 'expires_at': '2000-01-01'},
            ], {
                'cki_token_created_at{active="true",deployed="true",name="1"} 9.783072e+08',
                'cki_token_expires_at{active="true",deployed="true",name="2"} 9.466848e+08',
            }),
            ('wrong type', [
                {'token_type': 'wrong_type', 'created_at': '2011-01-01'},
                {'token_type': 'gitlab_group_token', 'created_at': '2001-01-01'},
                {'token_type': 'wrong_type', 'expires_at': '2010-01-01'},
                {'token_type': 'gitlab_group_token', 'expires_at': '2000-01-01'},
            ], {
                'cki_token_created_at{active="true",deployed="true",name="1"} 9.783072e+08',
                'cki_token_expires_at{active="true",deployed="true",name="2"} 1.262304e+09',
                'cki_token_expires_at{active="true",deployed="true",name="3"} 9.466848e+08',
            }),
        )
        for description, metas, expected in cases:
            with self.subTest(description), self._setup_secrets({
                str(i): {'meta': m} for i, m in enumerate(metas)
            }):
                self.assertEqual({
                    m for m in metrics.process().split('\n') if m.startswith('cki_')
                }, expected)

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_gitlab_create(self) -> None:
        """Test behavior of gitlab.create."""
        responses.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
        responses.get('https://instance/api/v4/groups/g', json={'id': 4})
        responses.post('https://instance/api/v4/projects/1/access_tokens', json={
            'id': 3, 'user_id': 2, 'created_at': '2000-01-01', 'expires_at': '2010-01-01',
            'revoked': False, 'active': True, 'token': 'secret',
        })
        responses.post('https://instance/api/v4/projects/1/deploy_tokens', json={
            'id': 5, 'username': 'user', 'expires_at': '2010-01-01',
            'revoked': False, 'expired': False, 'token': 'secret',
        })
        responses.post('https://instance/api/v4/groups/4/access_tokens', json={
            'id': 3, 'user_id': 2, 'created_at': '2000-01-01', 'expires_at': '2010-01-01',
            'revoked': False, 'active': True, 'token': 'secret',
        })
        responses.post('https://instance/api/v4/groups/4/deploy_tokens', json={
            'id': 6, 'username': 'user', 'expires_at': '2010-01-01',
            'revoked': False, 'expired': False, 'token': 'secret',
        })
        responses.get('https://instance/api/v4/users/2', json={
            'username': 'user'
        })
        vault_url = responses.put('https://vault/v1/apps/data/cki/secret_token')
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'secret'}}})

        cases = (
            ('project access token', 'secret_token', {
                'token_type': 'gitlab_project_token',
                'project_url': 'https://instance/g/p',
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
            }, {
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
                'deployed': False,
            }),
            ('group access token', 'secret_token', {
                'token_type': 'gitlab_group_token',
                'group_url': 'https://instance/groups/g',
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
            }, {
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
                'deployed': False,
            }),
            ('project deploy token', 'secret_token', {
                'token_type': 'gitlab_project_deploy_token',
                'project_url': 'https://instance/g/p',
                'scopes': ['scope'],
                'token_name': 'name',
            }, {
                'token_id': 5,
                'created_at': '2000-01-01T00:00:00+00:00',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_name': 'user',
                'deployed': False,
            }),
            ('group deploy token', 'secret_token', {
                'token_type': 'gitlab_group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'scopes': ['scope'],
                'token_name': 'name',
            }, {
                'token_id': 6,
                'created_at': '2000-01-01T00:00:00+00:00',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_name': 'user',
                'deployed': False,
            }),
            ('unsupported token type', 'secret_token', {
                'token_type': 'bugzilla_token',
                'group_url': 'https://instance/groups/g',
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
            }, None),
            ('unsupported gitlab token type', 'secret_token', {
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'scopes': ['scope'],
                'token_name': 'name',
            }, None),
        )
        for description, token, meta, expected in cases:
            with self.subTest(description), self._setup_secrets({token: {'meta': meta}}):
                vault_url.calls.reset()
                if expected is None:
                    with self.assertRaises(Exception):
                        gitlab.process('create', token)
                else:
                    gitlab.process('create', token)
                    self.assertEqual(json.loads(vault_url.calls[0].request.body),
                                     {'data': {'value': 'secret'}})
                    self.assertEqual(secrets.secret(f'{token}#'), {**meta, **expected})

    def test_gitlab_destroy(self) -> None:
        """Test behavior of gitlab.destroy."""
        cases = (
            ('project access token', {
                'active': True,
                'deployed': False,
                'project_url': 'https://instance/g/p',
                'token_id': 3,
                'token_type': 'gitlab_project_token',
            }, [{
                'url': 'https://instance/api/v4/projects/1/access_tokens/3',
                'json': {},
            }], False),
            ('deployed project access token', {
                'active': True,
                'deployed': True,
                'project_url': 'https://instance/g/p',
                'token_id': 3,
                'token_type': 'gitlab_project_token',
            }, [], True),
            ('group access token', {
                'active': True,
                'deployed': False,
                'group_url': 'https://instance/groups/g',
                'token_id': 3,
                'token_type': 'gitlab_group_token',
            }, [{
                'url': 'https://instance/api/v4/groups/4/access_tokens/3',
                'json': {},
            }], False),
            ('personal token', {
                'active': True,
                'deployed': False,
                'instance_url': 'https://instance',
                'token_id': 3,
                'token_type': 'gitlab_personal_token',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {},
            }], False),
            ('project deploy token', {
                'active': True,
                'deployed': False,
                'project_url': 'https://instance/g/p',
                'token_id': 3,
                'token_type': 'gitlab_project_deploy_token',
            }, [{
                'url': 'https://instance/api/v4/projects/1/deploy_tokens/3',
                'json': {},
            }], False),
            ('group deploy token', {
                'active': True,
                'deployed': False,
                'group_url': 'https://instance/groups/g',
                'token_id': 3,
                'token_type': 'gitlab_group_deploy_token',
            }, [{
                'url': 'https://instance/api/v4/groups/4/deploy_tokens/3',
                'json': {},
            }], False),
        )
        for description, secrets_data, url_mocks, expected in cases:
            with (self.subTest(description),
                  self._setup_secrets({
                    'secret_token': {'meta': secrets_data},
                  }),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                for url_mock in url_mocks:
                    rsps.add(**{'method': 'DELETE', **url_mock})
                rsps.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
                rsps.get('https://instance/api/v4/groups/g', json={'id': 4})
                rsps.get('https://vault/v1/apps/data/cki/secret_token',
                         json={'data': {'data': {'value': 'token'}}})
                gitlab.process('destroy', 'secret_token')
                self.assertEqual(secrets.secret('secret_token#active'), expected)

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    def test_gitlab_update(self) -> None:
        """Test behavior of gitlab.update."""
        cases = (
            ('project access token', 'secret_token', {
                'token_type': 'gitlab_project_token',
                'project_url': 'https://instance/g/p',
                'token_id': 3,
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/projects/1/access_tokens/3',
                'json': {
                    'id': 3,
                    'user_id': 2,
                    'name': 'name',
                    'scopes': ['scope'],
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'active': True,
                }
            }, {
                'url': 'https://instance/api/v4/projects/1/members/2',
                'json': {'username': 'user', 'access_level': 10},
            }], {
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
            }),
            ('project access token without token_id', 'secret_token', {
                'token_type': 'gitlab_project_token',
                'project_url': 'https://instance/g/p',
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'id': 3},
            }, {
                'url': 'https://instance/api/v4/projects/1/access_tokens/3',
                'json': {
                    'id': 3,
                    'user_id': 2,
                    'name': 'name',
                    'scopes': ['scope'],
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'active': True,
                },
            }, {
                'url': 'https://instance/api/v4/projects/1/members/2',
                'json': {'username': 'user', 'access_level': 10},
            }], {
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
            }),
            ('group access token', 'secret_token', {
                'token_type': 'gitlab_group_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 3,
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/groups/4/access_tokens/3',
                'json': {
                    'id': 3,
                    'user_id': 2,
                    'name': 'name',
                    'scopes': ['scope'],
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'active': True,
                },
            }, {
                'url': 'https://instance/api/v4/groups/4/members/2',
                'json': {'username': 'user', 'access_level': 10},
            }], {
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
            }),
            ('group access token without token_id', 'secret_token', {
                'token_type': 'gitlab_group_token',
                'group_url': 'https://instance/groups/g',
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'id': 3},
            }, {
                'url': 'https://instance/api/v4/groups/4/access_tokens/3',
                'json': {
                    'id': 3,
                    'user_id': 2,
                    'name': 'name',
                    'scopes': ['scope'],
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'active': True,
                },
            }, {
                'url': 'https://instance/api/v4/groups/4/members/2',
                'json': {'username': 'user', 'access_level': 10},
            }], {
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
            }),
            ('personal token', 'secret_token', {
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'scopes': ['api'],
                'token_id': 3,
                'user_id': 2,
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/3',
                'json': {
                    'id': 3,
                    'user_id': 2,
                    'name': 'name',
                    'scopes': ['api'],
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'active': True,
                },
            }, {
                'url': 'https://instance/api/v4/users/2',
                'json': {'username': 'user'},
            }], {
                'scopes': ['api'],
                'token_name': 'name',
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
            }),
            ('personal token without token_id', 'secret_token', {
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'scopes': ['api'],
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'id': 3, 'user_id': 2, 'scopes': ['api']},
            }, {
                'url': 'https://instance/api/v4/personal_access_tokens/3',
                'json': {
                    'id': 3,
                    'user_id': 2,
                    'name': 'name',
                    'scopes': ['api'],
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'active': True,
                },
            }, {
                'url': 'https://instance/api/v4/users/2',
                'json': {'username': 'user'},
            }], {
                'scopes': ['api'],
                'token_name': 'name',
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
            }),
            ('project deploy token', 'secret_token', {
                'token_type': 'gitlab_project_deploy_token',
                'project_url': 'https://instance/g/p',
                'token_id': 5,
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/projects/1/deploy_tokens/5',
                'json': {
                    'id': 5,
                    'username': 'user',
                    'name': 'name',
                    'scopes': ['scope'],
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'expired': False,
                }
            }], {
                'scopes': ['scope'],
                'token_name': 'name',
                'token_id': 5,
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_name': 'user',

            }),
            ('group deploy token', 'secret_token', {
                'token_type': 'gitlab_group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 6,
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/groups/4/deploy_tokens/6',
                'json': {
                    'id': 6,
                    'username': 'user',
                    'name': 'name',
                    'scopes': ['scope'],
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'expired': False,
                },
            }], {
                'scopes': ['scope'],
                'token_name': 'name',
                'token_id': 6,
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_name': 'user',
            }),
            ('runner authentication token', 'secret_token', {
                'token_type': 'gitlab_runner_authentication_token',
                'instance_url': 'https://instance',
                'active': True,
            }, [{
                'method': 'POST',
                'url': 'https://instance/api/v4/runners/verify',
                'json': {'id': 5, 'token_expires_at': '2020-01-01'},
            }], {
                'token_id': 5,
                'expires_at': '2020-01-01',
                'active': True,
            }),
            ('unknown token', 'secret_token', {
                'token_type': 'some_token',
                'active': True,
            }, [], {}),
        )
        for description, token, meta, url_mocks, expected in cases:
            with (self.subTest(description), self._setup_secrets({token: {'meta': meta}}),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                for url_mock in url_mocks:
                    rsps.add(**{'method': 'GET', **url_mock})
                rsps.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
                rsps.get('https://instance/api/v4/groups/g', json={'id': 4})
                rsps.get('https://vault/v1/apps/data/cki/secret_token',
                         json={'data': {'data': {'value': 'token'}}})
                if expected is None:
                    with self.assertRaises(Exception):
                        gitlab.process('update', token)
                else:
                    gitlab.process('update', token)
                    self.assertEqual(secrets.secret(f'{token}#'), {**meta, **expected})

    def test_gitlab_validate(self) -> None:
        """Test behavior of gitlab.validate."""
        cases = (
            ('project access token', {}, {
                'token_type': 'gitlab_project_token',
                'project_url': 'https://instance/g/p',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True}
            }], True),
            ('invalid project access token', {}, {
                'token_type': 'gitlab_project_token',
                'project_url': 'https://instance/g/p',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'status': HTTPStatus.UNAUTHORIZED,
            }], False),
            ('revoked project access token', {}, {
                'token_type': 'gitlab_project_token',
                'project_url': 'https://instance/g/p',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': True, 'active': True}
            }], False),
            ('inactive project access token', {}, {
                'token_type': 'gitlab_project_token',
                'project_url': 'https://instance/g/p',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': False}
            }], False),
            ('undeployed project access token', {}, {
                'token_type': 'gitlab_project_token',
                'project_url': 'https://instance/g/p',
                'active': True,
                'deployed': False,
            }, [], True),
            ('project access token with missing deployed field', {}, {
                'token_type': 'gitlab_project_token',
                'project_url': 'https://instance/g/p',
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True}
            }], False),
            ('double deployed project access token', {'secret_token/B': {'meta': {
                'token_type': 'gitlab_project_token',
                'project_url': 'https://instance/g/p',
                'active': True,
                'deployed': True,
            }}}, {
                'token_type': 'gitlab_project_token',
                'project_url': 'https://instance/g/p',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True}
            }], False),
            ('lonely project access token', {'secret_token/B': {'meta': {
                'token_type': 'gitlab_project_token',
                'project_url': 'https://instance/g/p',
                'active': False,
                'deployed': False,
            }}}, {
                'token_type': 'gitlab_project_token',
                'project_url': 'https://instance/g/p',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True}
            }], False),
            ('group access token', {}, {
                'token_type': 'gitlab_group_token',
                'group_url': 'https://instance/groups/g',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True},
            }], True),
            ('invalid group access token', {}, {
                'token_type': 'gitlab_group_token',
                'group_url': 'https://instance/groups/g',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'status': HTTPStatus.UNAUTHORIZED,
            }], False),
            ('revoked group access token', {}, {
                'token_type': 'gitlab_group_token',
                'group_url': 'https://instance/groups/g',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': True, 'active': True},
            }], False),
            ('inactive group access token', {}, {
                'token_type': 'gitlab_group_token',
                'group_url': 'https://instance/groups/g',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': False},
            }], False),
            ('undeployed group access token', {}, {
                'token_type': 'gitlab_group_token',
                'group_url': 'https://instance/groups/g',
                'active': True,
                'deployed': False,
            }, [], True),
            ('group access token with missing deployed field', {}, {
                'token_type': 'gitlab_group_token',
                'group_url': 'https://instance/groups/g',
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True},
            }], False),
            ('double deployed group access token', {'secret_token/B': {'meta': {
                'token_type': 'gitlab_group_token',
                'group_url': 'https://instance/groups/g',
                'active': True,
                'deployed': True,
            }}}, {
                'token_type': 'gitlab_group_token',
                'group_url': 'https://instance/groups/g',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True},
            }], False),
            ('lonely group access token', {'secret_token/B': {'meta': {
                'token_type': 'gitlab_group_token',
                'group_url': 'https://instance/groups/g',
                'active': False,
                'deployed': False,
            }}}, {
                'token_type': 'gitlab_group_token',
                'group_url': 'https://instance/groups/g',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True},
            }], False),
            ('personal token', {}, {
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True},
            }], True),
            ('invalid personal token', {}, {
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'status': HTTPStatus.UNAUTHORIZED,
            }], False),
            ('revoked personal token', {}, {
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': True, 'active': True},
            }], False),
            ('inactive personal token', {}, {
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': False},
            }], False),
            ('undeployed personal token', {}, {
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': False,
            }, [], True),
            ('personal token with missing deployed field', {}, {
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True},
            }], False),
            ('double deployed personal token', {'secret_token/B': {'meta': {
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
            }}}, {
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True},
            }], False),
            ('lonely personal token', {'secret_token/B': {'meta': {
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'active': False,
                'deployed': False,
            }}}, {
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True},
            }], False),
            ('project deploy token', {}, {
                'token_type': 'gitlab_project_deploy_token',
                'project_url': 'https://instance/g/p',
                'token_id': 5,
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/projects/1/deploy_tokens/5',
                'json': {'revoked': False, 'expired': False}
            }], True),
            ('invalid project deploy token', {}, {
                'token_type': 'gitlab_project_deploy_token',
                'project_url': 'https://instance/g/p',
                'token_id': 5,
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/projects/1/deploy_tokens/5',
                'status': HTTPStatus.NOT_FOUND,
            }], False),
            ('revoked project deploy token', {}, {
                'token_type': 'gitlab_project_deploy_token',
                'project_url': 'https://instance/g/p',
                'token_id': 5,
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/projects/1/deploy_tokens/5',
                'json': {'revoked': True, 'expired': False}
            }], False),
            ('inactive project deploy token', {}, {
                'token_type': 'gitlab_project_deploy_token',
                'project_url': 'https://instance/g/p',
                'token_id': 5,
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/projects/1/deploy_tokens/5',
                'json': {'revoked': False, 'expired': True}
            }], False),
            ('undeployed project deploy token', {}, {
                'token_type': 'gitlab_project_deploy_token',
                'project_url': 'https://instance/g/p',
                'token_id': 5,
                'active': True,
                'deployed': False,
            }, [], True),
            ('group deploy token', {}, {
                'token_type': 'gitlab_group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 6,
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/groups/4/deploy_tokens/6',
                'json': {'revoked': False, 'expired': False},
            }], True),
            ('invalid group deploy token', {}, {
                'token_type': 'gitlab_group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 6,
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/groups/4/deploy_tokens/6',
                'status': HTTPStatus.NOT_FOUND,
            }], False),
            ('revoked group deploy token', {}, {
                'token_type': 'gitlab_group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 6,
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/groups/4/deploy_tokens/6',
                'json': {'revoked': True, 'expired': False},
            }], False),
            ('inactive group deploy token', {}, {
                'token_type': 'gitlab_group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 6,
                'active': True,
            }, [{
                'url': 'https://instance/api/v4/groups/4/deploy_tokens/6',
                'json': {'revoked': False, 'expired': True},
            }], False),
            ('undeployed group deploy token', {}, {
                'token_type': 'gitlab_group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 6,
                'active': True,
                'deployed': False,
            }, [], True),
            ('runner authentication token', {}, {
                'token_type': 'gitlab_runner_authentication_token',
                'instance_url': 'https://instance',
                'active': True,
            }, [{
                'method': 'POST',
                'url': 'https://instance/api/v4/runners/verify',
                'json': {'id': 5, 'token_expires_at': '2020-01-01'},
            }], True),
            ('invalid runner authentication token', {}, {
                'token_type': 'gitlab_runner_authentication_token',
                'instance_url': 'https://instance',
                'active': True,
            }, [{
                'method': 'POST',
                'url': 'https://instance/api/v4/runners/verify',
                'status': HTTPStatus.UNAUTHORIZED,
            }], False),
            ('undeployed runner authentication token', {}, {
                'token_type': 'gitlab_runner_authentication_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': False,
            }, [], True),
            ('unknown token', {}, {
                'token_type': 'some_token',
                'active': True,
            }, [], True),
        )
        for description, other_data, secrets_data, url_mocks, expected in cases:
            with (self.subTest(description),
                  self._setup_secrets({
                    'secret_token': {'meta': secrets_data},
                  } | (other_data or {
                    'secret_token/B': {'meta': {**secrets_data, 'deployed': False}},
                  })),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                for url_mock in url_mocks:
                    rsps.add(**{'method': 'GET', **url_mock})
                rsps.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
                rsps.get('https://instance/api/v4/groups/g', json={'id': 4})
                rsps.get('https://vault/v1/apps/data/cki/secret_token',
                         json={'data': {'data': {'value': 'token'}}})
                rsps.get('https://vault/v1/apps/data/cki/secret_token/B',
                         json={'data': {'data': {'value': 'token'}}})
                if not expected:
                    with self.assertRaises(Exception):
                        gitlab.process('validate', 'secret_token')
                else:
                    gitlab.process('validate', 'secret_token')

    @freeze_time('2000-01-03T00:00:00.0+00:00')
    @responses.activate
    def test_gitlab_rotate_token(self) -> None:
        """Test behavior of gitlab.rotate."""
        responses.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
        responses.post('https://instance/api/v4/projects/1/access_tokens/3/rotate',
                       json={'id': 4, 'token': 'new-secret'})
        responses.get('https://instance/api/v4/projects/1/members/2',
                      json={'username': 'user', 'access_level': 10})
        responses.get('https://instance/api/v4/groups/g', json={'id': 4})
        responses.post('https://instance/api/v4/groups/4/access_tokens/3/rotate',
                       json={'id': 4, 'token': 'new-secret'})
        responses.get('https://instance/api/v4/groups/4/members/2',
                      json={'username': 'user', 'access_level': 10})
        responses.post('https://instance/api/v4/personal_access_tokens/3/rotate',
                       json={'id': 4, 'token': 'new-secret'})
        responses.get('https://instance/api/v4/users/2', json={'username': 'user'})

        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'secret'}}})
        responses.get('https://vault/v1/apps/data/cki/secret_token/B',
                      json={'data': {'data': {'value': 'deployed-secret'}}})
        responses.put('https://vault/v1/apps/data/cki/secret_token/946857600')
        responses.get('https://vault/v1/apps/data/cki/secret_token/946857600',
                      json={'data': {'data': {'value': 'new-secret'}}})

        token_types = (
            ('project token', {
                'token_type': 'gitlab_project_token',
                'project_url': 'https://instance/g/p',
                'access_level': 10,
            }),
            ('group token', {
                'token_type': 'gitlab_group_token',
                'group_url': 'https://instance/groups/g',
                'access_level': 10,
            }),
            ('personal token', {
                'token_type': 'gitlab_personal_token',
                'instance_url': 'https://instance',
            }),
        )

        for token_type, token_data in token_types:
            orig_meta = {
                'active': True,
                'deployed': False,
                'expires_at': '2000-01-02T00:00:00.0+00:00',
                'scopes': ['api'],
                'token_id': 3,
                'user_id': 2,
                **token_data,
            }
            new_meta = {
                'active': True,
                'created_at': '2000-01-01',
                'deployed': True,
                'expires_at': '2010-01-01',
                'revoked': False,
                'scopes': ['api'],
                'token_id': 4,
                'token_name': 'name',
                'user_id': 2,
                'user_name': 'user',
                **token_data,
            }
            cases = (
                ('default token', {}, {},
                 {'secret_token': {'backend': 'hv', 'meta': {**orig_meta}}},
                 {'secret_token': {'backend': 'hv', 'meta': {
                     **orig_meta, 'active': False, 'revoked': True}},
                    'secret_token/946857600': {'backend': 'hv', 'meta': {**new_meta}}},
                 ),
                ('multiple tokens', {}, {},
                 {'secret_token': {'backend': 'hv', 'meta': {**orig_meta}},
                    'secret_token/B': {'backend': 'hv', 'meta': {**orig_meta, 'deployed': True}}},
                 {'secret_token': {'backend': 'hv', 'meta': {
                     **orig_meta, 'active': False, 'revoked': True}},
                    'secret_token/B': {'backend': 'hv', 'meta': {**orig_meta}},
                    'secret_token/946857600': {'backend': 'hv', 'meta': {**new_meta}}},
                 ),
                ('only deployed', {}, {},
                 {'secret_token': {'backend': 'hv', 'meta': {**orig_meta, 'deployed': True}}},
                 {'secret_token': {'backend': 'hv', 'meta': {**orig_meta, 'deployed': True}}},
                 ),
                ('new enough', {}, {},
                 {'secret_token': {'backend': 'hv', 'meta': {
                     **orig_meta, 'expires_at': '2000-03-01T00:00:00.0+00:00'}}},
                 {'secret_token': {'backend': 'hv', 'meta': {
                     **orig_meta, 'expires_at': '2000-03-01T00:00:00.0+00:00'}}},
                 ),
                ('new enough but forced', {'force': True, 'token': 'secret_token'}, {},
                 {'secret_token': {'backend': 'hv', 'meta': {
                     **orig_meta, 'expires_at': '2000-03-01T00:00:00.0+00:00'}}},
                 {'secret_token': {'backend': 'hv', 'meta': {
                     **orig_meta, 'expires_at': '2000-03-01T00:00:00.0+00:00',
                     'active': False, 'revoked': True}},
                 'secret_token/946857600': {'backend': 'hv', 'meta': {**new_meta}}},
                 ),
                ('recently used', {}, {'last_used_at': '2000-01-03'},
                 {'secret_token': {'backend': 'hv', 'meta': {**orig_meta}}},
                 {'secret_token': {'backend': 'hv', 'meta': {**orig_meta}}},
                 ),
                ('dry run', {'dry_run': True}, {},
                 {'secret_token': {'backend': 'hv', 'meta': {**orig_meta}}},
                 {'secret_token': {'backend': 'hv', 'meta': {**orig_meta}}},
                 ),
            )

            for description, args, token_attrs, before_secrets, expected_secrets in cases:
                with (self.subTest(description, token_type=token_type),
                        self._setup_secrets(before_secrets)):
                    for url in {'projects/1/access_tokens',
                                'groups/4/access_tokens',
                                'personal_access_tokens'}:
                        responses.upsert('GET', f'https://instance/api/v4/{url}/3', json={
                            'active': True,
                            'created_at': '2000-01-01',
                            'expires_at': '2010-01-01',
                            'last_used_at': '2000-01-01',
                            'id': 3,
                            'name': 'name',
                            'revoked': False,
                            'scopes': ['api'],
                            'user_id': 2,
                            **token_attrs,
                        })
                        responses.upsert('GET', f'https://instance/api/v4/{url}/4', json={
                            'active': True,
                            'created_at': '2000-01-01',
                            'expires_at': '2010-01-01',
                            'last_used_at': '2000-01-01',
                            'id': 4,
                            'name': 'name',
                            'revoked': False,
                            'scopes': ['api'],
                            'user_id': 2,
                            **token_attrs,
                        })
                    gitlab.process('rotate', **args)
                    self.assertEqual(yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE')),
                                     expected_secrets)

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_gitlab_check_needs_rotate(self) -> None:
        """Test behavior of gitlab.check_needs_rotate."""
        cases = (
            ('no dates', {}, {
                'token_type': 'gitlab_runner_authentication_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
            }, r'\[\]'),
            ('too old', {}, {
                'token_type': 'gitlab_runner_authentication_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
            }, 'secret_token'),
            ('new enough', {}, {
                'token_type': 'gitlab_runner_authentication_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
                'created_at': '2000-01-01T00:00:00.0+00:00',
            }, r'\[\]'),
            ('expires', {}, {
                'token_type': 'gitlab_runner_authentication_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
            }, 'secret_token'),
            ('does not expire yet', {}, {
                'token_type': 'gitlab_runner_authentication_token',
                'instance_url': 'https://instance',
                'active': True,
                'deployed': True,
                'expires_at': '2001-01-01T00:00:00.0+00:00',
            }, r'\[\]'),
        )

        for description, args, secrets_data, expected_regex in cases:
            with (self.subTest(description),
                  self._setup_secrets({'secret_token': {'backend': 'hv', 'meta': secrets_data}})):
                with contextlib.redirect_stdout(buf := io.StringIO()):
                    gitlab.process('stats', **args)
                self.assertRegex(buf.getvalue(), f'rotated:.*{expected_regex}')

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_gitlab_check_needs_prepare(self) -> None:
        """Test behavior of gitlab.check_needs_prepare."""
        cases = (
            ('wrong type', {}, [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_runner_authentication_token',
            }], r'\[\]'),
            ('not needed', {}, [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], r'\[\]'),
            ('only one', {}, [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], 'secret_token'),
            ('new enough', {}, [{
                'active': True,
                'created_at': '2000-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2001-01-01T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], r'\[\]'),
            ('new enough but forced', {'force': True}, [{
                'active': True,
                'created_at': '2000-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2001-01-01T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], 'secret_token'),
            ('both deployed', {}, [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], 'secret_token'),
            ('creation dates missing', {}, [{
                'active': True,
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'deployed': False,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], 'secret_token'),
            ('deployed newer than active', {}, [{
                'active': True,
                'created_at': '1999-01-02T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], 'secret_token'),
        )

        for description, args, secrets_data, expected_regex in cases:
            with (self.subTest(description),
                  self._setup_secrets({f'secret_token/{i}': {'backend': 'hv', 'meta': d}
                                       for i, d in enumerate(secrets_data)})):
                with contextlib.redirect_stdout(buf := io.StringIO()):
                    gitlab.process('stats', **args)
                self.assertRegex(buf.getvalue(), f'prepared:.*{expected_regex}')

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    def test_gitlab_check_needs_clean(self) -> None:
        """Test behavior of gitlab.check_needs_clean."""
        cases = (
            ('not needed one', {}, [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_project_deploy_token',
            }], r'\[\]'),
            ('two deploy tokens', {}, [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_project_deploy_token',
            }, {
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_project_deploy_token',
            }], 'secret_token'),
            ('two personal tokens', {}, [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], r'\[\]'),
            ('three personal tokens', {}, [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': False,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], 'secret_token'),
            ('two deployed tokens', {}, [{
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }, {
                'active': True,
                'created_at': '1999-01-01T00:00:00.0+00:00',
                'deployed': True,
                'expires_at': '2000-01-15T00:00:00.0+00:00',
                'instance_url': 'https://instance',
                'token_type': 'gitlab_personal_token',
            }], 'secret_token'),
        )

        for description, args, secrets_data, expected_regex in cases:
            with (self.subTest(description),
                  self._setup_secrets({f'secret_token/{i}': {'backend': 'hv', 'meta': d}
                                       for i, d in enumerate(secrets_data)})):
                with contextlib.redirect_stdout(buf := io.StringIO()):
                    gitlab.process('stats', **args)
                self.assertRegex(buf.getvalue(), f'cleaned:.*{expected_regex}')

    @mock.patch('ldap.initialize')
    def test_ldap_update(self, ldap_initialize) -> None:
        """Test behavior of ldap.update."""
        def mock_search(dn, *_, **__):
            match dn:
                case 'some=dn':
                    return [(dn, {
                        'memberOf': [b'foo=1', b'bar=2', b'baz=3'],
                        'cn': [b'common'],
                    })]
                case 'foo=1':
                    return [(dn, {'rhatRoverGroupMemberQuery': [b'query']})]
                case 'bar=2':
                    return [(dn, {})]
                case 'baz=3':
                    return [(dn, {})]
        ldap_initialize.return_value.search_s.side_effect = mock_search

        with self._setup_secrets({'secret_token': {'meta': {
            'dn': 'some=dn',
            'ldap_server': 'ldap.server',
            'token_type': 'ldap_keytab',
        }}}):
            ldap.process('update', 'secret_token')
            self.assertEqual(yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE')), {
                'secret_token': {'meta': {
                    'cn': 'common',
                    'dn': 'some=dn',
                    'ldap_server': 'ldap.server',
                    'memberOf': ['bar=2', 'baz=3'],
                    'token_type': 'ldap_keytab',
                }},
            })

    @responses.activate
    @mock.patch('ldap.initialize')
    def test_ldap_validate(self, ldap_initialize) -> None:
        """Test behavior of ldap.update."""
        def mock_bind(dn, *_, **__):
            match dn:
                case 'dn=working':
                    return
                case 'dn=failing':
                    raise INVALID_CREDENTIALS(dn)

        ldap_initialize.return_value.simple_bind_s.side_effect = mock_bind
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'secret'}}})

        cases = (
            ('dn=working', True),
            ('dn=failing', False),
        )
        for dn, expected in cases:
            with self.subTest(dn), self._setup_secrets({'secret_token': {'meta': {
                'dn': dn,
                'ldap_server': 'ldap.server',
                'token_type': 'ldap_password',
            }}}):
                if expected:
                    ldap.process('validate', 'secret_token')
                else:
                    with self.assertRaises(INVALID_CREDENTIALS):
                        ldap.process('validate', 'secret_token')

    @responses.activate
    @mock.patch('cki_tools.credential_manager.aws.BOTO_SESSION')
    def test_aws_update(self, boto_session) -> None:
        """Test behavior of aws.update."""
        create_date = datetime.datetime(2010, 1, 1)
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'secret'}}})
        boto_session.client.return_value.get_access_key_last_used.return_value = {
            'UserName': 'username',
        }
        boto_session.client.return_value.list_access_keys.return_value = {'AccessKeyMetadata': [
            {'AccessKeyId': 'aki', 'Status': 'Active', 'CreateDate': create_date},
        ]}

        cases = (
            ('default', 'account', {
            }, {
                'account': 'account',
                'arn': 'arn',
                'active': True,
                'user_name': 'username',
                'created_at': '2010-01-01T00:00:00+00:00',
            }),
            ('known account, arn', 'other-account', {
                'account': 'other-account',
                'arn': 'other-arn',
            }, {
                'account': 'other-account',
                'arn': 'other-arn',
                'user_name': 'username',
                'active': True,
                'created_at': '2010-01-01T00:00:00+00:00',
            }),
            ('mismatched account ignored', 'account', {
                'account': 'other-account',
                'arn': 'other-arn',
            }, {
                'account': 'other-account',
                'arn': 'other-arn',
            }),
            ('known account, arn, user_name', 'other-account', {
                'account': 'other-account',
                'arn': 'other-arn',
                'user_name': 'other-username',
            }, {
                'account': 'other-account',
                'arn': 'other-arn',
                'user_name': 'other-username',
                'active': True,
                'created_at': '2010-01-01T00:00:00+00:00',
            }),
            ('custom endpoint', 'account', {
                'endpoint_url': 'endpoint',
            }, {
                'endpoint_url': 'endpoint',
            }),
        )

        for description, account, meta, expected in cases:
            with self.subTest(description), self._setup_secrets({'secret_token': {
                    'backend': 'hv',
                    'meta': {
                        'token_type': 'aws_secret_access_key',
                        'access_key_id': 'aki',
                        **meta,
                    },
            }}):
                boto_session.client.return_value.get_caller_identity.return_value = {
                    'Account': account,
                    'Arn': 'arn',
                }
                aws.process('update', 'secret_token')
                self.assertEqual(yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE')), {
                    'secret_token': {'backend': 'hv', 'meta': {
                        'token_type': 'aws_secret_access_key',
                        'access_key_id': 'aki',
                        **expected,
                    }},
                })

    @responses.activate
    def test_dogtag_update(self) -> None:
        """Test behavior of dogtag.update."""
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'secret'}}})
        token_put = responses.put('https://vault/v1/apps/data/cki/secret_token')

        responses.get('http://dogtag:8443/ca/rest/certs/pooh', json={
            'Encoded': 'certificate\n',
            'id': 'bear',
            'IssuerDN': 'issuer',
            'NotAfter': '2010-01-10T00:00:00+00:00',
            'NotBefore': '2010-01-01T00:00:00+00:00',
            'Status': 'VALID',
            'SubjectDN': 'subject',
        })

        with self._setup_secrets({'secret_token': {
                'backend': 'hv',
                'meta': {
                    'token_type': 'dogtag_certificate',
                    'id': 'pooh',
                    'server_url': 'http://dogtag:8443',
                },
        }}):
            dogtag.process('update', 'secret_token')
            self.assertEqual(yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE')), {
                'secret_token': {'backend': 'hv', 'meta': {
                    'active': True,
                    'created_at': '2010-01-01T00:00:00+00:00',
                    'expires_at': '2010-01-10T00:00:00+00:00',
                    'id': 'bear',
                    'IssuerDN': 'issuer',
                    'server_url': 'http://dogtag:8443',
                    'SubjectDN': 'subject',
                    'token_type': 'dogtag_certificate',
                }},
            })
            self.assertEqual(json.loads(token_put.calls[0].request.body), {'data': {
                'value': 'secret', 'certificate': 'certificate',
            }})

    @responses.activate
    def test_dogtag_create(self) -> None:
        """Test behavior of dogtag.create."""
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'value': 'secret'}}})
        responses.get('https://vault/v1/apps/data/cki/ldap',
                      json={'data': {'data': {'value': 'password'}}})
        token_put = responses.put('https://vault/v1/apps/data/cki/secret_token')

        cert_post = responses.post(
            'http://dogtag:8443/ca/rest/certrequests', json={'entries': [{
                'requestURL': 'http://dogtag:8443/ca/rest/certrequests/1',
            }]})
        responses.get('http://dogtag:8443/ca/rest/certrequests/1', json={'certId': 'pooh'})
        responses.get('http://dogtag:8443/ca/rest/certs/pooh', json={
            'Encoded': 'certificate\n',
            'id': 'bear',
            'IssuerDN': 'issuer',
            'NotAfter': '2010-01-10T00:00:00+00:00',
            'NotBefore': '2010-01-01T00:00:00+00:00',
            'Status': 'VALID',
            'SubjectDN': 'subject',
        })

        with self._setup_secrets({
            'secret_token': {
                'backend': 'hv',
                'meta': {
                    'token_type': 'dogtag_certificate',
                    'server_url': 'http://dogtag:8443',
                    'SubjectDN': 'uid=subject,something=else',
                },
            },
            'ldap': {'backend': 'hv', 'meta': {'token_type': 'ldap_password', 'uid': 'subject'}},
        }):
            dogtag.process('create', 'secret_token')
            self.assertEqual(secrets.secret('secret_token#'), {
                'active': True,
                'created_at': '2010-01-01T00:00:00+00:00',
                'deployed': True,
                'expires_at': '2010-01-10T00:00:00+00:00',
                'id': 'bear',
                'IssuerDN': 'issuer',
                'server_url': 'http://dogtag:8443',
                'SubjectDN': 'subject',
                'token_type': 'dogtag_certificate',
            })
            self.assertEqual(len(cert_post.calls), 1)
            self.assertEqual(len(token_put.calls), 2)
            certrequest = json.loads(cert_post.calls[0].request.body)
            misc.set_nested_key(certrequest, 'Input/0/Attribute/1/Value', 'request')
            self.assertEqual(certrequest, {
                'ProfileID': 'caDirAppUserCert',
                'Attributes': {'Attribute': [
                    {'name': 'uid', 'value': 'subject'},
                    {'name': 'pwd', 'value': 'password'},
                ]},
                'Input': [{'Attribute': [
                    {'name': 'cert_request_type', 'Value': 'pkcs10'},
                    {'name': 'cert_request', 'Value': 'request'},
                ]}],
            })

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    def test_dogtag_rotate(self) -> None:
        """Test behavior of dogtag.rotate."""
        cases = (
            ('refresh', {'token': 'secret_token', 'force': True}, {}, {
                'secret_token': {'meta': {'deployed': False}},
                'secret_token/946684800': {'backend': 'hv', 'meta': {
                    'active': True,
                    'created_at': '2010-01-01T00:00:00+00:00',
                    'deployed': True,
                    'expires_at': '2010-01-10T00:00:00+00:00',
                    'id': 'bear',
                    'IssuerDN': 'issuer',
                    'server_url': 'http://dogtag:8443',
                    'SubjectDN': 'subject',
                    'token_type': 'dogtag_certificate',
                }},
            }),
            ('wrong type', {'token': 'secret_token', 'force': True}, {
                'secret_token': {'meta': {'token_type': 'pooh'}},
            }, {}),
            ('dry run', {'token': 'secret_token', 'force': True, 'dry_run': True}, {}, {}),
            ('new-enough', {'token': 'secret_token'}, {}, {}),
        )

        for description, args, before, expected in cases:
            before_secrets = config_tree.merge_dicts({
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'dogtag_certificate',
                    'server_url': 'http://dogtag:8443',
                    'SubjectDN': 'uid=subject,something=else',
                    'expires_at': '2010-01-10T00:00:00+00:00',
                    'deployed': True,
                }},
                'ldap': {'backend': 'hv', 'meta': {'token_type': 'ldap_password', 'uid': 'subject'}}
            }, before)

            with (self.subTest(description),
                  self._setup_secrets(before_secrets),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                rsps.get('https://vault/v1/apps/data/cki/secret_token/946684800',
                         json={'data': {'data': {}}})
                rsps.get('https://vault/v1/apps/data/cki/ldap',
                         json={'data': {'data': {'value': 'password'}}})
                token_put = rsps.put('https://vault/v1/apps/data/cki/secret_token/946684800')

                cert_post = rsps.post(
                    'http://dogtag:8443/ca/rest/certrequests', json={'entries': [{
                        'requestURL': 'http://dogtag:8443/ca/rest/certrequests/1',
                    }]})
                rsps.get('http://dogtag:8443/ca/rest/certrequests/1', json={'certId': 'pooh'})
                rsps.get('http://dogtag:8443/ca/rest/certs/pooh', json={
                    'Encoded': 'certificate\n',
                    'id': 'bear',
                    'IssuerDN': 'issuer',
                    'NotAfter': '2010-01-10T00:00:00+00:00',
                    'NotBefore': '2010-01-01T00:00:00+00:00',
                    'Status': 'VALID',
                    'SubjectDN': 'subject',
                })

                dogtag.process('rotate', **args)
                self.assertEqual(
                    yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE')),
                    config_tree.merge_dicts(before_secrets, expected),
                )
                if expected:
                    self.assertEqual(len(cert_post.calls), 1)
                    self.assertEqual(len(token_put.calls), 2)
                    certrequest = json.loads(cert_post.calls[0].request.body)
                    misc.set_nested_key(certrequest, 'Input/0/Attribute/1/Value', 'request')
                    self.assertEqual(certrequest, {
                        'ProfileID': 'caDirAppUserCert',
                        'Attributes': {'Attribute': [
                            {'name': 'uid', 'value': 'subject'},
                            {'name': 'pwd', 'value': 'password'},
                        ]},
                        'Input': [{'Attribute': [
                            {'name': 'cert_request_type', 'Value': 'pkcs10'},
                            {'name': 'cert_request', 'Value': 'request'},
                        ]}],
                    })
                else:
                    self.assertEqual(len(cert_post.calls), 0)
                    self.assertEqual(len(token_put.calls), 0)

    @responses.activate
    def test_dogtag_validate(self) -> None:
        """Test behavior of dogtag.validate."""

        created_at = misc.datetime_fromisoformat_tz_utc('1999-01-01T00:00:00.0+00:00')
        expires_at = misc.datetime_fromisoformat_tz_utc('2001-01-01T00:00:00.0+00:00')

        key = rsa.generate_private_key(65537, 4096)
        key2 = rsa.generate_private_key(65537, 4096)
        cert = x509.CertificateBuilder().subject_name(x509.Name([
        ])).public_key(
            key.public_key()
        ).issuer_name(x509.Name([
        ])).serial_number(1).not_valid_before(
            created_at
        ).not_valid_after(
            expires_at
        ).sign(key, hashes.SHA256())

        cases = (
            ('default', key, cert, '2000-01-01T00:00:00.0+00:00', True),
            ('expired', key, cert, '2002-01-01T00:00:00.0+00:00', False),
            ('key mismatch', key2, cert, '2000-01-01T00:00:00.0+00:00', False),
        )

        for description, key, cert, now, expected in cases:
            with (self.subTest(description),
                  self._setup_secrets({'secret_token': {'backend': 'hv', 'meta': {
                      'token_type': 'dogtag_certificate',
                      'expires_at': expires_at.isoformat(),
                  }}}),
                  freeze_time(now),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                rsps.get('https://vault/v1/apps/data/cki/secret_token', json={'data': {'data': {
                    'private_key': key.private_bytes(
                        serialization.Encoding.PEM,
                        serialization.PrivateFormat.PKCS8,
                        serialization.NoEncryption()).decode('ascii').strip(),
                    'certificate': cert.public_bytes(serialization.Encoding.PEM).decode('ascii')
                    .strip()
                }}})
                if expected:
                    dogtag.process('validate', 'secret_token')
                else:
                    with self.assertRaises(Exception):
                        dogtag.process('validate', 'secret_token')

    @responses.activate
    def test_splunk_validate(self) -> None:
        """Test behavior of splunk.validate."""
        cases = (
            (400, {'text': 'No data', 'code': 5}, True),
            (400, {'text': 'Event field is required', 'code': 12, 'invalid-event-number': 0}, True),
            (400, {'text': 'Incorrect index', 'code': 7, 'invalid-event-number': 1}, False),
            (401, {'text': 'Invalid authorization', 'code': 3}, False),
            (403, {'text': 'Invalid token', 'code': 4}, False),
            (404, {'text': 'The requested URL was not found on this server.', 'code': 404}, False),
        )
        for status, contents, expected in cases:
            with (self.subTest(status), self._setup_secrets({'secret_token': {'meta': {
                'endpoint_url': 'https://endpoint-url',
                'token_type': 'splunk_hec_token',
            }}}), responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                rsps.get('https://vault/v1/apps/data/cki/secret_token',
                         json={'data': {'data': {'value': 'secret'}}})
                rsps.post('https://endpoint-url/services/collector', status=status,
                          json=contents)

                if expected:
                    splunk.process('validate', 'secret_token')
                else:
                    with self.assertRaises(Exception):
                        splunk.process('validate', 'secret_token')

    @responses.activate
    def test_ssh_update(self) -> None:
        """Test behavior of ssh.update."""
        private_key = rsa.generate_private_key(65537, 4096)
        private_bytes = private_key.private_bytes(
            serialization.Encoding.PEM,
            serialization.PrivateFormat.OpenSSH,
            serialization.NoEncryption()
        ).decode('ascii').strip()
        public_bytes = private_key.public_key().public_bytes(
            serialization.Encoding.OpenSSH,
            serialization.PublicFormat.OpenSSH
        ).decode('ascii').strip() + ' pooh'
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {'private_key': private_bytes}}})
        token_put = responses.put('https://vault/v1/apps/data/cki/secret_token')

        with self._setup_secrets({'secret_token': {'backend': 'hv', 'meta': {
            'token_type': 'ssh_private_key',
            'comment': 'pooh',
            'created_at': '2000-01-01T00:00:00+00:00',
            'key_size': 2048,  # needs an update
        }}}):
            ssh.process('update', 'secret_token')
            self.assertEqual(yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE')), {
                'secret_token': {'backend': 'hv', 'meta': {
                    'comment': 'pooh',
                    'created_at': '2000-01-01T00:00:00+00:00',
                    'key_size': 4096,
                    'token_type': 'ssh_private_key',
                }},
            })
            self.assertEqual(json.loads(token_put.calls[0].request.body), {'data': {
                'private_key': private_bytes, 'public_key': public_bytes,
            }})

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    def test_ssh_create(self) -> None:
        """Test behavior of ssh.create."""
        private_key = rsa.generate_private_key(65537, 4096)
        private_bytes = private_key.private_bytes(
            serialization.Encoding.PEM,
            serialization.PrivateFormat.OpenSSH,
            serialization.NoEncryption()
        ).decode('ascii').strip()
        cases = (
            ('default', {'key_size': 4096}, True),
            ('too small', {'key_size': 2048}, False),
        )
        for description, config, expected in cases:
            with (self.subTest(description),
                  self._setup_secrets({'secret_token': {'backend': 'hv', 'meta': {
                      'token_type': 'ssh_private_key',
                      'comment': 'pooh',
                  } | config}}),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                rsps.get('https://vault/v1/apps/data/cki/secret_token',
                         json={'data': {'data': {'private_key': private_bytes}}})
                token_put = rsps.put('https://vault/v1/apps/data/cki/secret_token')

                if expected:
                    ssh.process('create', 'secret_token')
                    self.assertEqual(secrets.secret('secret_token#'), {
                        'active': True,
                        'comment': 'pooh',
                        'created_at': '2000-01-01T00:00:00+00:00',
                        'deployed': True,
                        'key_size': 4096,
                        'token_type': 'ssh_private_key',
                    })
                    self.assertEqual(json.loads(token_put.calls[0].request.body), {
                        'data': {'private_key': mock.ANY}
                    })
                else:
                    with self.assertRaises(Exception):
                        ssh.process('create', 'secret_token')
                    self.assertEqual(len(token_put.calls), 0)

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    def test_ssh_rotate(self) -> None:
        """Test behavior of ssh.rotate."""
        private_key = rsa.generate_private_key(65537, 4096)
        private_bytes = private_key.private_bytes(
            serialization.Encoding.PEM,
            serialization.PrivateFormat.OpenSSH,
            serialization.NoEncryption()
        ).decode('ascii').strip()
        cases = (
            ('refresh', {'token': 'secret_token', 'force': True}, {}, {
                'secret_token': {'meta': {'deployed': False}},
                'secret_token/946684800': {'backend': 'hv', 'meta': {
                    'active': True,
                    'comment': 'bear',
                    'created_at': '2000-01-01T00:00:00+00:00',
                    'deployed': True,
                    'key_size': 4096,
                    'token_type': 'ssh_private_key',
                }},
            }),
            ('wrong type', {'token': 'secret_token', 'force': True}, {
                'secret_token': {'meta': {'token_type': 'pooh'}},
            }, {}),
            ('dry run', {'token': 'secret_token', 'force': True, 'dry_run': True}, {}, {}),
            ('new-enough', {'token': 'secret_token'}, {}, {}),
        )

        for description, args, before, expected in cases:
            before_secrets = config_tree.merge_dicts({
                'secret_token': {'backend': 'hv', 'meta': {
                    'comment': 'bear',
                    'created_at': '2010-01-10T00:00:00+00:00',
                    'deployed': True,
                    'key_size': 4096,
                    'token_type': 'ssh_private_key',
                }},
            }, before)

            with (self.subTest(description),
                  self._setup_secrets(before_secrets),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                rsps.get('https://vault/v1/apps/data/cki/secret_token/946684800',
                         json={'data': {'data': {
                             'private_key': private_bytes,
                         }}})
                token_put = rsps.put('https://vault/v1/apps/data/cki/secret_token/946684800')

                ssh.process('rotate', **args)
                self.assertEqual(
                    yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE')),
                    config_tree.merge_dicts(before_secrets, expected),
                )
                if expected:
                    self.assertEqual(len(token_put.calls), 2)
                else:
                    self.assertEqual(len(token_put.calls), 0)

    @responses.activate
    def test_ssh_validate(self) -> None:
        """Test behavior of ssh.validate."""

        private_key = rsa.generate_private_key(65537, 4096)
        private_bytes = private_key.private_bytes(
            serialization.Encoding.PEM,
            serialization.PrivateFormat.OpenSSH,
            serialization.NoEncryption()
        ).decode('ascii').strip()
        public_bytes = private_key.public_key().public_bytes(
            serialization.Encoding.OpenSSH,
            serialization.PublicFormat.OpenSSH
        ).decode('ascii').strip() + ' pooh'
        private_key2 = rsa.generate_private_key(65537, 4096)
        private_bytes2 = private_key2.private_bytes(
            serialization.Encoding.PEM,
            serialization.PrivateFormat.OpenSSH,
            serialization.NoEncryption()
        ).decode('ascii').strip()

        cases = (
            ('default', private_bytes, public_bytes, {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'ssh_private_key',
                    'active': True,
                    'deployed': True,
                }},
            }, True),
            ('two active', private_bytes, public_bytes, {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'ssh_private_key',
                    'active': True,
                    'deployed': True,
                }},
                'secret_token/2': {'backend': 'hv', 'meta': {
                    'token_type': 'ssh_private_key',
                    'active': True,
                    'deployed': False,
                }},
            }, True),
            ('no active field', private_bytes, public_bytes, {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'ssh_private_key',
                    'deployed': True,
                }},
            }, False),
            ('no deployed field', private_bytes, public_bytes, {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'ssh_private_key',
                    'active': True,
                }},
            }, False),
            ('default', private_bytes, public_bytes, {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'ssh_private_key',
                    'active': True,
                    'deployed': True,
                }},
            }, True),
            ('two deployed', private_bytes, public_bytes, {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'ssh_private_key',
                    'active': True,
                    'deployed': True,
                }},
                'secret_token/2': {'backend': 'hv', 'meta': {
                    'token_type': 'ssh_private_key',
                    'active': True,
                    'deployed': True,
                }},
            }, False),
            ('none active', private_bytes, public_bytes, {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'ssh_private_key',
                    'active': False,
                    'deployed': True,
                }},
                'secret_token/2': {'backend': 'hv', 'meta': {
                    'token_type': 'ssh_private_key',
                    'active': False,
                    'deployed': False,
                }},
            }, False),
            ('three active', private_bytes, public_bytes, {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'ssh_private_key',
                    'active': True,
                    'deployed': True,
                }},
                'secret_token/2': {'backend': 'hv', 'meta': {
                    'token_type': 'ssh_private_key',
                    'active': True,
                    'deployed': False,
                }},
                'secret_token/3': {'backend': 'hv', 'meta': {
                    'token_type': 'ssh_private_key',
                    'active': True,
                    'deployed': False,
                }},
            }, False),
            ('wrong key', private_bytes2, public_bytes, {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'ssh_private_key',
                    'active': True,
                    'deployed': True,
                }},
            }, False),
        )

        for description, private, public, variables, expected in cases:
            with (self.subTest(description),
                  self._setup_secrets(variables),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                rsps.get('https://vault/v1/apps/data/cki/secret_token', json={'data': {'data': {
                    'private_key': private,
                    'public_key': public,
                }}})
                rsps.get('https://vault/v1/apps/data/cki/secret_token/2', json={'data': {'data': {
                    'private_key': private,
                    'public_key': public,
                }}})
                if expected:
                    ssh.process('validate', 'secret_token')
                else:
                    with self.assertRaises(Exception):
                        ssh.process('validate', 'secret_token')

    @responses.activate
    @freeze_time('2000-01-01T00:00:00.0+00:00')
    def test_password_create(self) -> None:
        """Test behavior of password.create."""
        responses.get('https://vault/v1/apps/data/cki/secret_token',
                      json={'data': {'data': {}}})
        token_put = responses.put('https://vault/v1/apps/data/cki/secret_token')

        with self._setup_secrets({'secret_token': {'backend': 'hv', 'meta': {
            'token_type': 'password',
        }}}):
            password.process('create', 'secret_token')
            self.assertEqual(secrets.secret('secret_token#'), {
                'active': True,
                'created_at': '2000-01-01T00:00:00+00:00',
                'deployed': True,
                'token_type': 'password',
            })
            self.assertEqual(json.loads(token_put.calls[0].request.body), {
                'data': {'value': mock.ANY}
            })

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    def test_password_rotate(self) -> None:
        """Test behavior of password.rotate."""
        cases = (
            ('refresh', {'token': 'secret_token', 'force': True}, {}, {
                'secret_token': {'meta': {'deployed': False}},
                'secret_token/946684800': {'backend': 'hv', 'meta': {
                    'active': True,
                    'created_at': '2000-01-01T00:00:00+00:00',
                    'deployed': True,
                    'token_type': 'password',
                }},
            }),
            ('wrong type', {'token': 'secret_token', 'force': True}, {
                'secret_token': {'meta': {'token_type': 'pooh'}},
            }, {}),
            ('dry run', {'token': 'secret_token', 'force': True, 'dry_run': True}, {}, {}),
            ('new-enough', {'token': 'secret_token'}, {}, {}),
        )

        for description, args, before, expected in cases:
            before_secrets = config_tree.merge_dicts({
                'secret_token': {'backend': 'hv', 'meta': {
                    'created_at': '2010-01-10T00:00:00+00:00',
                    'deployed': True,
                    'token_type': 'password',
                }},
            }, before)

            with (self.subTest(description),
                  self._setup_secrets(before_secrets),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                rsps.get('https://vault/v1/apps/data/cki/secret_token/946684800',
                         json={'data': {'data': {}}})
                token_put = rsps.put('https://vault/v1/apps/data/cki/secret_token/946684800')

                password.process('rotate', **args)
                self.assertEqual(
                    yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE')),
                    config_tree.merge_dicts(before_secrets, expected),
                )
                if expected:
                    self.assertEqual(len(token_put.calls), 1)
                else:
                    self.assertEqual(len(token_put.calls), 0)

    def test_password_validate(self) -> None:
        """Test behavior of password.validate."""

        cases = (
            ('default', {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'password',
                    'active': True,
                    'deployed': True,
                }},
            }, True),
            ('two active', {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'password',
                    'active': True,
                    'deployed': True,
                }},
                'secret_token/2': {'backend': 'hv', 'meta': {
                    'token_type': 'password',
                    'active': True,
                    'deployed': False,
                }},
            }, True),
            ('no active field', {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'password',
                    'deployed': True,
                }},
            }, False),
            ('no deployed field', {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'password',
                    'active': True,
                }},
            }, False),
            ('default', {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'password',
                    'active': True,
                    'deployed': True,
                }},
            }, True),
            ('two deployed', {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'password',
                    'active': True,
                    'deployed': True,
                }},
                'secret_token/2': {'backend': 'hv', 'meta': {
                    'token_type': 'password',
                    'active': True,
                    'deployed': True,
                }},
            }, False),
            ('none active', {
                'secret_token': {'backend': 'hv', 'meta': {
                    'token_type': 'password',
                    'active': False,
                    'deployed': True,
                }},
                'secret_token/2': {'backend': 'hv', 'meta': {
                    'token_type': 'password',
                    'active': False,
                    'deployed': False,
                }},
            }, False),
        )

        for description,  variables, expected in cases:
            with self.subTest(description), self._setup_secrets(variables):
                if expected:
                    password.process('validate', 'secret_token')
                else:
                    with self.assertRaises(Exception):
                        password.process('validate', 'secret_token')
