"""Tests for sync Polarion ID to kpet-db."""

from importlib import resources
import io
import json
import pathlib
import tarfile
import tempfile
import unittest
from unittest import mock
import uuid
import zipfile

import responses

from cki_tools import sync_polarion_id
from tests.assets import sync_polarion_id as assets


class TestSyncPolarionID(unittest.TestCase):
    """Tests for update ystream compose in kpet-db."""

    def test_get_origins(self) -> None:
        """Test for get_origins."""
        with tempfile.TemporaryDirectory() as kpetdbdir:
            pathlib.Path(f'{kpetdbdir}/index.yaml').write_text(json.dumps({
                'origins': ['origin1', 'origin2'],
                'variables': {
                    'origin1_url': {'default': 'https://origin1.zip'},
                    'origin2_url': {'default': 'https://origin2.zip'},
                    'not_origin':  {'default': 'not an url'},
                },
            }), 'utf8')
            origins = sync_polarion_id.get_origins(kpetdbdir)
        self.assertEqual(origins,
                         {'origin1': 'https://origin1.zip', 'origin2': 'https://origin2.zip'})

    def test_get_suitable_test(self) -> None:
        """Test for get_suitable_test."""
        tmt_tests = [{'name': '/core/test/withid', 'id': str(uuid.uuid4())},
                     {'name': '/general/another/withid', 'id': str(uuid.uuid4())}]
        self.assertEqual(sync_polarion_id.get_suitable_test('core/test/withid', tmt_tests, {}),
                         tmt_tests[0])

        # test has different IDs based on environment
        tmt_tests = [{'name': '/general/test/1/complete', 'id': str(uuid.uuid4()),
                      'environment': {}},
                     {'name': '/general/test/1/reduced', 'id': str(uuid.uuid4()),
                     'environment': {'REDUCED': '1'}}]
        self.assertEqual(sync_polarion_id.get_suitable_test('general/test/1', tmt_tests, {}),
                         tmt_tests[0])
        self.assertEqual(sync_polarion_id.get_suitable_test('general/test/1', tmt_tests,
                                                            {'REDUCED': '1'}), tmt_tests[1])
        # if kpet-db pass additional parameter it shouldn't matter
        self.assertEqual(sync_polarion_id.get_suitable_test('general/test/1', tmt_tests,
                                                            {'EXTRA_PARAM': '1'}), tmt_tests[0])
        self.assertEqual(sync_polarion_id.get_suitable_test('general/test/1', tmt_tests,
                                                            {'REDUCED': '1', 'EXTRA_PARAM': '1'}),
                         tmt_tests[1])

        # test has no environment defined on fmf kpet-db environment shouldn't matter
        tmt_tests = [{'name': '/misc/test/1', 'id': str(uuid.uuid4()),
                      'environment': {}}]
        self.assertEqual(sync_polarion_id.get_suitable_test('misc/test/1', tmt_tests,
                                                            {'SHORT': '1'}), tmt_tests[0])

        # test has environment defined on fmf, therefore kpet-db environment matter
        tmt_tests = [{'name': '/misc/multiparam/short1_test1', 'id': str(uuid.uuid4()),
                      'environment': {'SHORT': '1', 'TEST': '1'}},
                     {'name': '/misc/multiparam/short1_test2', 'id': str(uuid.uuid4()),
                     'environment': {'SHORT': '1', 'TEST': '2'}},
                     {'name': '/misc/multiparam/test1', 'id': str(uuid.uuid4()),
                     'environment': {'TEST': '1'}}]
        self.assertEqual(sync_polarion_id.get_suitable_test('misc/multiparam', tmt_tests,
                                                            {'SHORT': '1'}), None)
        self.assertEqual(sync_polarion_id.get_suitable_test('misc/multiparam', tmt_tests,
                                                            {'SHORT': '1', 'TEST': '2'}),
                         tmt_tests[1])
        self.assertEqual(sync_polarion_id.get_suitable_test('misc/multiparam', tmt_tests,
                                                            {'TEST': '1'}), tmt_tests[2])
        self.assertEqual(sync_polarion_id.get_suitable_test('misc/multiparam', tmt_tests,
                                                            {'TEST': '2'}), None)
        # directory has no fmf definition
        tmt_tests = []
        self.assertEqual(sync_polarion_id.get_test_id('test/1', tmt_tests, {}), None)

    def test_get_test_id(self) -> None:
        """Test for get test id."""
        test_id = str(uuid.uuid4())
        mock_tmt_tests = [{'name': '/test/1', 'id': test_id}]
        self.assertEqual(sync_polarion_id.get_test_id('test', mock_tmt_tests, {}), test_id)

        test_notuuid4 = str(uuid.uuid1())
        mock_tmt_tests = [{'name': '/test/1', 'id': test_notuuid4}]
        self.assertEqual(sync_polarion_id.get_test_id('test/1', mock_tmt_tests, {}), None)

        test_notuuid = '12345'
        mock_tmt_tests = [{'name': '/test/1', 'id': test_notuuid}]
        self.assertEqual(sync_polarion_id.get_test_id('test/1', mock_tmt_tests, {}), None)

        mock_tmt_tests = [{'name': '/test/1', 'id': None}]
        self.assertEqual(sync_polarion_id.get_test_id('test/1', mock_tmt_tests, {}), None)

        # test doesn't have id
        mock_tmt_tests = [{'name': '/test/1', 'environment': {}}]
        self.assertEqual(sync_polarion_id.get_test_id('test/1', mock_tmt_tests, {}), None)

    @mock.patch('cki_tools.sync_polarion_id.get_test_id')
    @mock.patch('cki_tools.sync_polarion_id.get_tmt_tests', mock.Mock(return_value=[]))
    @mock.patch('cki_tools.sync_polarion_id.list_tests', mock.Mock(return_value=[
        {"origin": "origin1", "location": "misc/test1", "environment": {}},
        {"origin": "origin1", "location": "general/test2", "environment": {"var1": "1"}},
        {"origin": "origin1", "location": "general/test3", "environment": {}},
        {"origin": "origin2", "environment": {}, "location": "test1_origin2"},
        {"origin": "origin2", "environment": {}, "location": "test1_origin2"},
        {"origin": "origin2", "environment": {"test": "1"}, "location": "test1_origin2"},
    ]))
    @mock.patch('cki_tools.sync_polarion_id.get_origins', mock.Mock(return_value={
        'origin1': 'https://origin1.zip',
        'origin2': 'https://origin2.tar.gz',
    }))
    @responses.activate
    def test_populate_tests_id(self, mock_id) -> None:
        """Test for get test id."""
        test1_id = str(uuid.uuid4())
        test2_id = str(uuid.uuid4())
        test3_id = str(uuid.uuid4())
        # Make sure that if multiple tests names in kpet-db have the same location/environment
        # it should be considered the same test, so just 1 entry in the output.
        # If has same location, but different environment it should be considered different test

        mock_id.side_effect = [test1_id, test2_id, None, test3_id, test3_id, test3_id]

        with zipfile.ZipFile(zip_buffer := io.BytesIO(), mode='w') as zip_file:
            zip_file.mkdir('root/.fmf')
        responses.get('https://origin1.zip', body=zip_buffer.getvalue())

        with tarfile.open(fileobj=(tar_buffer := io.BytesIO()), mode='w:gz') as tar_file:
            tarinfo = tarfile.TarInfo(name='root/.fmf')
            tarinfo.type = tarfile.DIRTYPE
            tar_file.addfile(tarinfo)
        responses.get('https://origin2.tar.gz', body=tar_buffer.getvalue())

        with tempfile.TemporaryDirectory() as kpetdbdir:
            populated_tests = sync_polarion_id.populate_tests_id(kpetdbdir)

        self.assertEqual(populated_tests, {
            'origin1': [
                {'id': test1_id, 'location': 'misc/test1', 'environment': {}},
                {'id': test2_id, 'location': 'general/test2', 'environment': {'var1': '1'}},
                {'location': 'general/test3', 'environment': {}},
            ],
            'origin2': [
                {'id': test3_id, 'location': 'test1_origin2', 'environment': {}},
                {'id': test3_id, 'location': 'test1_origin2', 'environment': {'test': '1'}},
            ],
        })

    def test_generate_external_j2(self) -> None:
        """Test to generate kpet-db external.j2 file."""
        test1_id = "c49733bc-ab68-4842-afc2-cea140cfd75"
        test2_id = "80bc9cca-6aa5-48bc-9630-765fc3c38c24"
        test3_id = "c316b2c4-0860-4c51-a900-7dca0103bf8a"
        mocked_tests = {
            'origin1': [
                {'id': test1_id, 'location': 'misc/test1'},
                {'id': test2_id, 'location': 'general/test2'},
                {'location': 'general/test3'},
            ],
            'origin2': [
                {'id': test3_id, 'location': 'test_origin2'},
            ],
            'origin3': [
                {'location': 'test_origin3'},
            ],
        }

        expected_output = (resources.files(assets) / 'external.j2').read_text('utf8')
        with tempfile.TemporaryDirectory() as tempdir:
            sync_polarion_id.generate_external_j2(mocked_tests, tempdir)
            output = pathlib.Path(f'{tempdir}/external.j2').read_text('utf8')
        self.assertEqual(output, expected_output)
