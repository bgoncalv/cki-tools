"""Test regexes."""
from datetime import timedelta
import re
import textwrap
import unittest
from unittest import mock
from unittest.mock import patch

from cki_lib.timeout import FunctionTimeout
from datawarehouse import Datawarehouse
import requests.exceptions
import responses

from cki.triager import cache
from cki.triager import checkers
from cki.triager import compiledregex
from cki.triager import dwobject

from ..utils import mock_attrs

MOCKED_CONSOLE_LOG_DICT = {'name': 'console.log', 'url': 'http://server/console.log'}
MOCKED_CONSOLE_LOG_TEXT = textwrap.dedent(
    """
    [   1234.0] Boot starting 123
    [   1235.5] Some weird string to look for 12345 !# -- ,, !
    """
)
MOCKED_TEXT_MATCH = r"Some weird string to look for 12345 !# -- ,,"
MOCKED_FILE_NAME_MATCH = r"console\.log"

MOCKED_REGEX_URL = 'http://datawarehouse/api/1/issue/-/regex'
MOCKED_BUILD_URL = 'http://datawarehouse/api/1/kcidb/builds/1'
MOCKED_CHECKOUT_URL = 'http://datawarehouse/api/1/kcidb/checkouts/1'
MOCKED_ISSUE = {
    "id": 64,
    "kind": {
        "id": 1,
        "description": "Kernel bug",
        "tag": "Kernel Bug"
    },
    "description": "Bug description",
    "ticket_url": "https://bug.link",
    "resolved": False,
    "generic": False
}
MOCKED_ISSUE_REGEX_ID = 2


@unittest.mock.patch('cki.triager.settings.DW_CLIENT', Datawarehouse('http://datawarehouse'))
class TestRegexChecker(unittest.TestCase):
    """Test TestRegexChecker."""

    def _mock_downloaded_regex(self, *, file_name_match="", text_match="",
                               test_name_match="", testresult_name_match="",
                               tree_match="", kpet_tree_name_match="",
                               architecture_match="", package_name_match=""):
        """Set the response for the IssueRegex endpoint with the given matching rules."""
        rules = {
            "file_name_match": file_name_match,
            "text_match": text_match,
            "test_name_match": test_name_match,
            "testresult_name_match": testresult_name_match,
            "tree_match": tree_match,
            "kpet_tree_name_match": kpet_tree_name_match,
            "architecture_match": architecture_match,
            "package_name_match": package_name_match,
        }
        lookup = {"id": MOCKED_ISSUE_REGEX_ID, "issue": MOCKED_ISSUE, **rules}

        responses.upsert(responses.GET, MOCKED_REGEX_URL, json={'results': [lookup]})
        cache.get_issueregexes.cache_clear()  # pylint: disable=protected-access
        compiledregex._get_compiled_issueregexes.cache_clear()  # pylint: disable=protected-access
        self.regexes = compiledregex.get_compiled_issueregexes([])

    def setUp(self):
        checkers.download.cache_clear()
        cache.get_build.cache_clear()
        cache.get_checkout.cache_clear()
        cache.get_issueregexes.cache_clear()
        compiledregex._get_compiled_issueregexes.cache_clear()  # pylint: disable=protected-access
        build_dict = mock_attrs(
            architecture='x86_64', package_name='kernel', kpet_tree_name='c9s', checkout_id=1)
        responses.get(MOCKED_BUILD_URL, json=build_dict)
        checkout_dict = mock_attrs(tree_name='eln-ark')
        responses.get(MOCKED_CHECKOUT_URL, json=checkout_dict)

    def tearDown(self):
        responses.stop()
        responses.reset()

    def _assert_match(self, dw_obj, status, **kwargs):
        """assert that a regex matches in a certain way."""
        self.assertEqual(checkers.match(checkers.LogFile(
            dw_obj, **kwargs, **MOCKED_CONSOLE_LOG_DICT), self.regexes[0]), status)

    def assert_full_match(self, dw_obj, **kwargs):
        """assert that a regex matches fully."""
        self._assert_match(dw_obj, checkers.MatchStatus.FULL_MATCH, **kwargs)

    def assert_partial_match(self, dw_obj, **kwargs):
        """assert that a regex matches on the logfile, but not on the meta data."""
        self._assert_match(dw_obj, checkers.MatchStatus.PARTIAL_MATCH, **kwargs)

    def assert_no_match(self, dw_obj, **kwargs):
        """assert that a regex does not match."""
        self._assert_match(dw_obj, checkers.MatchStatus.NO_MATCH, **kwargs)

    def assert_not_applicable(self, dw_obj, **kwargs):
        """assert that a regex is not applicable to a logfile."""
        self._assert_match(dw_obj, checkers.MatchStatus.NOT_APPLICABLE, **kwargs)

    @responses.activate
    @patch("cki.triager.checkers.download")
    def test_search_text_match(self, mocked_download):
        """Test search works as expected around the text_match rule."""
        self._mock_downloaded_regex(
            text_match=MOCKED_TEXT_MATCH,
        )

        mocked_download.return_value = MOCKED_CONSOLE_LOG_TEXT
        with self.subTest("Returns true if text matches."):
            self.assert_full_match(dwobject.from_attrs('checkout', mock_attrs()))

        mocked_download.return_value = None
        with self.subTest("Returns false if the text is empty."):
            self.assert_no_match(dwobject.from_attrs('checkout', mock_attrs()))

        mocked_download.return_value = "Text with no matches."
        with self.subTest("Returns false if the text doesn't match."):
            self.assert_no_match(dwobject.from_attrs('checkout', mock_attrs()))

        mocked_download.return_value = "a"*1000
        with self.subTest("Logs if regex matching takes long enough."), \
                patch("cki.triager.settings.REGEX_EXPECTED_TIME", timedelta(microseconds=0.1)), \
                self.assertLogs(logger=checkers.LOGGER, level='ERROR') as log_ctx:
            checkers.match(checkers.LogFile(
                dwobject.from_attrs('checkout', mock_attrs()),
                **MOCKED_CONSOLE_LOG_DICT), self.regexes[0])

            expected_log = re.escape(
                f"ERROR:{checkers.LOGGER.name}:"
                f"Regex matching took too long. regex_id={MOCKED_ISSUE_REGEX_ID} elapsed_s="
            ) + r"\d\.\d+"
            self.assertRegex(log_ctx.output[0], expected_log)

        mocked_download.return_value = "a"*1000
        with self.subTest("Halts if regex matching takes too long."), \
                patch("cki.triager.settings.REGEX_TIMEOUT", timedelta(microseconds=0.1)), \
                self.assertRaises(FunctionTimeout):
            checkers.match(checkers.LogFile(
                dwobject.from_attrs('checkout', mock_attrs()),
                **MOCKED_CONSOLE_LOG_DICT), self.regexes[0])

    @responses.activate
    @patch("cki.triager.checkers.download", mock.Mock(return_value=MOCKED_CONSOLE_LOG_TEXT))
    def test_search_test_name(self):
        """Test search with test_name."""
        self._mock_downloaded_regex(
            text_match=MOCKED_TEXT_MATCH,
            test_name_match="test-name",
        )

        # Checkout
        with self.subTest("Returns false if object type is not test|testresult (checkout)"):
            self.assert_not_applicable(dwobject.from_attrs(
                'checkout', mock_attrs(comment="test-name")))

        # Build
        with self.subTest("Returns false if object type is not test|testresult (build)"):
            self.assert_not_applicable(dwobject.from_attrs(
                'build', mock_attrs(checkout_id=1, comment="test-name")))

        # Test
        with self.subTest("Returns false if *comment* doesn't match test_name_match"):
            self.assert_not_applicable(dwobject.from_attrs(
                'test', mock_attrs(build_id=1, comment="without match")))

        with self.subTest("Returns true if *comment* matches test_name_match"):
            self.assert_full_match(dwobject.from_attrs(
                'test', mock_attrs(build_id=1, comment='Test with test-name in the name.')))

        with self.subTest("Returns false if test name (comment) is empty"):
            self.assert_not_applicable(dwobject.from_attrs(
                'test', mock_attrs(build_id=1, comment=None)))

        # TestResult without testresult_name_match
        with self.subTest("Returns false if there's no testresult_name_match, "
                          "but also if *misc/test_name* doesn't match test_name_match"):
            self.assert_not_applicable(dwobject.from_attrs('testresult', mock_attrs(
                comment="TestResult with testresult-name in the name",
                misc={"iid": 1, "test_name": "without match"})
            ), dw_test=dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("Returns true if there's no testresult_name_match, "
                          "but *misc/test_name* matches test_name_match"):
            self.assert_full_match(dwobject.from_attrs('testresult', mock_attrs(
                comment="TestResult with testresult-name in the name",
                misc={"iid": 1, "test_name": 'Test with test-name in the name.'})
            ), dw_test=dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("Returns false if there's no testresult_name_match, "
                          "but also test name (misc/test_name) is empty"):
            self.assert_not_applicable(dwobject.from_attrs('testresult', mock_attrs(
                comment="TestResult with testresult-name in the name",
                misc={"iid": 1, "test_name": None})
            ), dw_test=dwobject.from_attrs('test', mock_attrs(build_id=1)))

        # Test Result with testresult_name_match
        self._mock_downloaded_regex(
            text_match=MOCKED_TEXT_MATCH,
            test_name_match="test-name",
            testresult_name_match="testresult-name",
        )

        with self.subTest("Returns false if *misc/test_name* doesn't match test_name_match"):
            self.assert_not_applicable(dwobject.from_attrs('testresult', mock_attrs(
                comment="TestResult with testresult-name in the name",
                misc={"iid": 1, "test_name": "without match"})
            ), dw_test=dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("Returns true if *misc/test_name* matches test_name_match"):
            self.assert_full_match(dwobject.from_attrs('testresult', mock_attrs(
                comment="TestResult with testresult-name in the name",
                misc={"iid": 1, "test_name": 'Test with test-name in the name.'})
            ), dw_test=dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("Returns false if test name (misc/test_name) is empty"):
            self.assert_not_applicable(dwobject.from_attrs('testresult', mock_attrs(
                comment="TestResult with testresult-name in the name",
                misc={"iid": 1, "test_name": None})
            ), dw_test=dwobject.from_attrs('test', mock_attrs(build_id=1)))

    @responses.activate
    @patch("cki.triager.checkers.download", mock.Mock(return_value=MOCKED_CONSOLE_LOG_TEXT))
    def test_search_testresult_name(self):
        """Test search with testresult_name, if obj_type is not testresult it should be ignored."""
        self._mock_downloaded_regex(
            text_match=MOCKED_TEXT_MATCH,
            testresult_name_match=".*",
        )

        with self.subTest("Returns true for checkout if testresult_name_match == '.*'"):
            self.assert_full_match(dwobject.from_attrs('checkout', mock_attrs()))

        with self.subTest("Returns true for build if testresult_name_match == '.*'"):
            self.assert_full_match(dwobject.from_attrs('build', mock_attrs(checkout_id=1)))

        with self.subTest("Returns true for test if testresult_name_match == '.*'"):
            self.assert_full_match(dwobject.from_attrs('test', mock_attrs(build_id=1)))

        self._mock_downloaded_regex(
            text_match=MOCKED_TEXT_MATCH,
            testresult_name_match="testresult-name",
        )

        with self.subTest("Returns false for checkout if testresult_name_match != '.*'"):
            self.assert_not_applicable(dwobject.from_attrs('checkout', mock_attrs()))

        with self.subTest("Returns false for build if testresult_name_match != '.*'"):
            self.assert_not_applicable(dwobject.from_attrs('build', mock_attrs(checkout_id=1)))

        with self.subTest("Returns false for test if testresult_name_match != '.*'"):
            self.assert_not_applicable(dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("Returns false if *comment* doesn't match testresult_name_match"):
            self.assert_not_applicable(
                dwobject.from_attrs('testresult', mock_attrs(comment="without match")),
                dw_test=dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("Returns true if *comment* matches testresult_name_match"):
            self.assert_full_match(
                dwobject.from_attrs('testresult', mock_attrs(comment="has testresult-name in it")),
                dw_test=dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("Returns false if test name (comment) is empty"):
            self.assert_not_applicable(
                dwobject.from_attrs('testresult', mock_attrs(comment=None)),
                dw_test=dwobject.from_attrs('test', mock_attrs(build_id=1)))

    @responses.activate
    @patch("cki.triager.checkers.download", mock.Mock(return_value=MOCKED_CONSOLE_LOG_TEXT))
    def test_search_file_name(self):
        """Test search with file_name."""
        self._mock_downloaded_regex(
            text_match=MOCKED_TEXT_MATCH,
            file_name_match=MOCKED_FILE_NAME_MATCH,
        )

        log_dict = {**MOCKED_CONSOLE_LOG_DICT, 'name': 'console.nope'}
        with self.subTest("Returns false if log['name'] doesn't match file_name_match"):
            self.assertEqual(checkers.match(checkers.LogFile(
                dwobject.from_attrs('checkout', mock_attrs()),
                **log_dict), self.regexes[0]), checkers.MatchStatus.NOT_APPLICABLE)

        with self.subTest("Returns true if log['name'] matches file_name_match"):
            self.assert_full_match(dwobject.from_attrs('checkout', mock_attrs()))

        log_dict = {"url": MOCKED_CONSOLE_LOG_DICT["url"]}
        with self.subTest("Returns false if log['name'] is empty, despite the URL matching"):
            self.assertEqual(checkers.match(checkers.LogFile(
                dwobject.from_attrs('checkout', mock_attrs()),
                **log_dict),
                self.regexes[0]), checkers.MatchStatus.NOT_APPLICABLE)

    @responses.activate
    @patch("cki.triager.checkers.download")
    def test_search_file_name_and_test_name(self, mocked_download):
        """Test search with file_name and test_name."""
        self._mock_downloaded_regex(
            text_match=MOCKED_TEXT_MATCH,
            file_name_match=MOCKED_FILE_NAME_MATCH,
            test_name_match=r'test-name',
        )

        mocked_download.return_value = MOCKED_CONSOLE_LOG_TEXT

        with self.subTest("test_name and file_name wrong, text ok."):
            self.assert_not_applicable(dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("test_name ok, file_name wrong, text ok."):
            self.assertEqual(checkers.match(checkers.LogFile(
                dwobject.from_attrs(
                    'test', mock_attrs(build_id=1, comment='Test with test-name in the name.')),
                **{'name': 'file.name', 'url': 'http://server/file.name'}),
                self.regexes[0]), checkers.MatchStatus.NOT_APPLICABLE)
        with self.subTest("test_name wrong, file_name ok, text ok."):
            self.assert_not_applicable(dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("All ok."):
            self.assert_full_match(dwobject.from_attrs(
                'test', mock_attrs(build_id=1, comment='Test with test-name in the name.')))

        mocked_download.return_value = "wrong text"

        with self.subTest("test_name ok, file_name ok, text wrong."):
            self.assert_no_match(dwobject.from_attrs(
                'test', mock_attrs(build_id=1, comment='Test with test-name in the name.')))

    @responses.activate
    @patch("cki.triager.checkers.download", mock.Mock(return_value=MOCKED_CONSOLE_LOG_TEXT))
    def test_search_regex_syntax(self):
        """Test search with some regex syntax."""
        self._mock_downloaded_regex(
            text_match=MOCKED_TEXT_MATCH,
            file_name_match=r"some.*thing|console.log",
            test_name_match=r"this-name|other-name",
        )

        with self.subTest("Assert test_name_match matches with set union (a|b)"):
            self.assert_full_match(dwobject.from_attrs(
                'test', mock_attrs(build_id=1, comment='this-name')))

            self.assert_full_match(dwobject.from_attrs(
                'test', mock_attrs(build_id=1, comment='other-name')))

        with self.subTest("Assert file_name_match matches with wildcard and asterisk (.*)"):
            self.assertTrue(checkers.match(checkers.LogFile(
                dwobject.from_attrs('test', mock_attrs(build_id=1, comment='other-name')),
                **{'name': 'something', 'url': 'http://server/something'}),
                self.regexes[0]))

            self.assertTrue(checkers.match(checkers.LogFile(
                dwobject.from_attrs('test', mock_attrs(build_id=1, comment='other-name')),
                **{'name': 'somebleblething', 'url': 'http://server/somebleblething'}),
                self.regexes[0]))

    @responses.activate
    @patch("cki.triager.checkers.download", mock.Mock(return_value=MOCKED_CONSOLE_LOG_TEXT))
    def test_search_with_arch(self):
        """Test search for a given architecture."""
        self._mock_downloaded_regex(
            text_match=MOCKED_TEXT_MATCH,
            architecture_match=r'x86_64',
        )

        # Checkout
        with self.subTest("Return false for checkouts"):
            self.assert_partial_match(dwobject.from_attrs('checkout', mock_attrs()))

        # Build
        with self.subTest("Return false when build has no arch"):
            self.assert_partial_match(dwobject.from_attrs(
                'build',
                mock_attrs(checkout_id=1, architecture=None)
            ))

        with self.subTest("Return false when build has wrong arch"):
            self.assert_partial_match(dwobject.from_attrs(
                'build',
                mock_attrs(checkout_id=1, architecture='s390x')
            ))

        with self.subTest("Return true when build has matching arch"):
            self.assert_full_match(dwobject.from_attrs(
                'build',
                mock_attrs(checkout_id=1, architecture='x86_64')
            ))

        # Test fetching build
        with self.subTest("Return false when *fetched* build has no arch"):
            responses.replace(responses.GET, MOCKED_BUILD_URL,
                              json=mock_attrs(checkout_id=1, architecture=None))
            cache.get_build.cache_clear()
            self.assert_partial_match(dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("Return false when *fetched* build has wrong arch"):
            responses.replace(responses.GET, MOCKED_BUILD_URL,
                              json=mock_attrs(checkout_id=1, architecture='s390x'))
            cache.get_build.cache_clear()
            self.assert_partial_match(dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("Return true when *fetched* build has matching arch"):
            responses.replace(responses.GET, MOCKED_BUILD_URL,
                              json=mock_attrs(checkout_id=1, architecture='x86_64'))
            cache.get_build.cache_clear()
            self.assert_full_match(dwobject.from_attrs('test', mock_attrs(build_id=1)))

    @responses.activate
    @patch("cki.triager.checkers.download", mock.Mock(return_value=MOCKED_CONSOLE_LOG_TEXT))
    def test_search_with_kpet_tree_name(self):
        """Test search for a given kpet tree name (release)."""
        self._mock_downloaded_regex(
            text_match=MOCKED_TEXT_MATCH,
            kpet_tree_name_match=r'foo',
        )

        # Checkout
        with self.subTest("Return false for checkouts"):
            self.assert_partial_match(dwobject.from_attrs('checkout', mock_attrs()))

        # Build
        with self.subTest("Return false when build has no kpet tree"):
            self.assert_partial_match(dwobject.from_attrs(
                'build',
                mock_attrs(checkout_id=1, misc={
                    'iid': 1, 'kpet_tree_name': None})
            ))

        with self.subTest("Return false when build has wrong kpet tree"):
            self.assert_partial_match(dwobject.from_attrs(
                'build',
                mock_attrs(checkout_id=1, misc={
                    'iid': 1, 'kpet_tree_name': 'bar'})
            ))

        with self.subTest("Return true when build has matching kpet tree"):
            self.assert_full_match(dwobject.from_attrs(
                'build',
                mock_attrs(checkout_id=1, misc={
                    'iid': 1, 'kpet_tree_name': 'foo'})
            ))

        # Test fetching build
        with self.subTest("Return false when *fetched* build has no kpet tree"):
            responses.replace(responses.GET, MOCKED_BUILD_URL, json=mock_attrs(
                checkout_id=1, misc={'iid': 1, 'kpet_tree_name': None}))
            cache.get_build.cache_clear()
            self.assert_partial_match(dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("Return false when *fetched* build has wrong kpet tree"):
            responses.replace(responses.GET, MOCKED_BUILD_URL, json=mock_attrs(
                checkout_id=1, misc={'iid': 1, 'kpet_tree_name': 'bar'}))
            cache.get_build.cache_clear()
            self.assert_partial_match(dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("Return true when *fetched* build has matching kpet tree"):
            responses.replace(responses.GET, MOCKED_BUILD_URL, json=mock_attrs(
                checkout_id=1, misc={'iid': 1, 'kpet_tree_name': 'foo'}))
            cache.get_build.cache_clear()
            self.assert_full_match(dwobject.from_attrs('test', mock_attrs(build_id=1)))

    @responses.activate
    @patch("cki.triager.checkers.download", mock.Mock(return_value=MOCKED_CONSOLE_LOG_TEXT))
    def test_search_with_package_name(self):
        """Test search for a given package name."""
        self._mock_downloaded_regex(
            text_match=MOCKED_TEXT_MATCH,
            package_name_match=r'kernel|kernel-foo',  # Note: "kernel" should not match partially!
        )

        # Checkout
        with self.subTest("Return false for checkouts"):
            self.assert_partial_match(dwobject.from_attrs('checkout', mock_attrs()))

        # Build
        with self.subTest("Return false when build has no package name"):
            self.assert_partial_match(dwobject.from_attrs(
                'build',
                mock_attrs(checkout_id=1, misc={
                    'iid': 1, 'package_name': None})
            ))

        with self.subTest("Return false when build has wrong package name"):
            self.assert_partial_match(dwobject.from_attrs(
                'build',
                mock_attrs(checkout_id=1, misc={
                    'iid': 1, 'package_name': 'kernel-bar'})
            ))

        with self.subTest("Return true when build has matching package name"):
            self.assert_full_match(dwobject.from_attrs(
                'build',
                mock_attrs(checkout_id=1, misc={
                    'iid': 1, 'package_name': 'kernel-foo'})
            ))

        # Test fetching build
        with self.subTest("Return false when *fetched* build has no package name"):
            responses.replace(responses.GET, MOCKED_BUILD_URL, json=mock_attrs(
                checkout_id=1, misc={'iid': 1, 'package_name': None}))
            cache.get_build.cache_clear()
            self.assert_partial_match(dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("Return false when *fetched* build has wrong package name"):
            responses.replace(responses.GET, MOCKED_BUILD_URL, json=mock_attrs(
                checkout_id=1, misc={'iid': 1, 'package_name': 'kernel-bar'}))
            cache.get_build.cache_clear()
            self.assert_partial_match(dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("Return true when *fetched* build has matching package name"):
            responses.replace(responses.GET, MOCKED_BUILD_URL, json=mock_attrs(
                checkout_id=1, misc={'iid': 1, 'package_name': 'kernel-foo'}))
            cache.get_build.cache_clear()
            self.assert_full_match(dwobject.from_attrs('test', mock_attrs(build_id=1)))

    @responses.activate
    @patch("cki.triager.checkers.download", mock.Mock(return_value=MOCKED_CONSOLE_LOG_TEXT))
    def test_search_with_tree(self):
        """Test search for a given tree."""
        self._mock_downloaded_regex(
            text_match=MOCKED_TEXT_MATCH,
            tree_match=r'rhel8[46]-z$',
        )

        # Checkout
        with self.subTest("Return false when checkout has no tree"):
            self.assert_partial_match(dwobject.from_attrs('checkout', mock_attrs(tree_name=None)))

        with self.subTest("Return false when checkout has wrong tree"):
            self.assert_partial_match(dwobject.from_attrs('checkout', mock_attrs(tree_name='c9s')))

        with self.subTest("Return true when checkout has matching tree"):
            self.assert_full_match(dwobject.from_attrs(
                'checkout', mock_attrs(tree_name='rhel86-z')))

        # Build fetching checkout
        with self.subTest("Return false when *fetched build's* checkout has no tree"):
            responses.replace(responses.GET, MOCKED_CHECKOUT_URL,
                              json=mock_attrs(tree_name=None))
            cache.get_checkout.cache_clear()
            self.assert_partial_match(dwobject.from_attrs('build', mock_attrs(checkout_id=1)))

        with self.subTest("Return false when *fetched build's* checkout has wrong tree"):
            responses.replace(responses.GET, MOCKED_CHECKOUT_URL,
                              json=mock_attrs(tree_name='c9s'))
            cache.get_checkout.cache_clear()
            self.assert_partial_match(dwobject.from_attrs('build', mock_attrs(checkout_id=1)))

        with self.subTest("Return true when *fetched build's* checkout has matching tree"):
            responses.replace(responses.GET, MOCKED_CHECKOUT_URL,
                              json=mock_attrs(tree_name='rhel86-z'))
            cache.get_checkout.cache_clear()
            self.assert_full_match(dwobject.from_attrs('build', mock_attrs(checkout_id=1)))

        # Test fetching checkout
        with self.subTest("Return false when *fetched test's* checkout has no tree"):
            responses.replace(responses.GET, MOCKED_CHECKOUT_URL,
                              json=mock_attrs(tree_name=None))
            cache.get_checkout.cache_clear()
            self.assert_partial_match(dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("Return false when *fetched test's* checkout has wrong tree"):
            responses.replace(responses.GET, MOCKED_CHECKOUT_URL,
                              json=mock_attrs(tree_name='c9s'))
            cache.get_checkout.cache_clear()
            self.assert_partial_match(dwobject.from_attrs('test', mock_attrs(build_id=1)))

        with self.subTest("Return true when *fetched test's* checkout has matching tree"):
            responses.replace(responses.GET, MOCKED_CHECKOUT_URL,
                              json=mock_attrs(tree_name='rhel86-z'))
            cache.get_checkout.cache_clear()
            self.assert_full_match(dwobject.from_attrs('test', mock_attrs(build_id=1)))

    @responses.activate
    def test_download_skip_bad_regex(self):
        """Assert that a bad regex pattern won't crash the triager."""
        test_name_match = r'test-name'
        bad_pattern = r"(?s)+this should crash with 'nothing to repeat at position 4'"

        rules = {
            "file_name_match": MOCKED_FILE_NAME_MATCH,
            "test_name_match": test_name_match,
            "text_match": bad_pattern,
        }
        lookup = {"id": MOCKED_ISSUE_REGEX_ID, "issue": MOCKED_ISSUE, **rules}

        responses.add(responses.GET, MOCKED_REGEX_URL, json={'results': [lookup]})

        cache.get_issueregexes.cache_clear()
        compiledregex._get_compiled_issueregexes.cache_clear()  # pylint: disable=protected-access
        with patch.object(compiledregex.LOGGER, "error") as mocked_error_logger:
            compiled_issueregexes = compiledregex.get_compiled_issueregexes([])

        mocked_error_logger.assert_called_with(
            "Bad regex pattern %r for %r in issueregex %s: %s",
            bad_pattern, "text_match", MOCKED_ISSUE_REGEX_ID, "nothing to repeat at position 4",
        )

        # The broken regex should not be returned
        self.assertEqual(compiled_issueregexes, [])

    @responses.activate
    def test_download_lookups(self):
        """Test download_lookups."""
        responses.add(responses.GET, MOCKED_REGEX_URL,
                      json={'results': [
                          {"id": 1, "issue": {"id": 1, "description": "d"}},
                          {"id": 2, "issue": {"id": 1, "description": "d"}},
                          {"id": 3, "issue": {"id": 1, "description": "d"}},
                      ]})

        cache.get_issueregexes.cache_clear()
        compiledregex._get_compiled_issueregexes.cache_clear()  # pylint: disable=protected-access
        compiled_issueregexes = compiledregex.get_compiled_issueregexes([])

        self.assertEqual([1, 2, 3], [lookup.id for lookup in compiled_issueregexes])

    @responses.activate
    def test_download_lookups_selective(self):
        """Test download_lookups, only specified ones."""
        responses.add(responses.GET, MOCKED_REGEX_URL + '/1',
                      json={"id": 1, "issue": {"id": 1, "description": "d"}})
        responses.add(responses.GET, MOCKED_REGEX_URL + '/2', status=404)
        responses.add(responses.GET, MOCKED_REGEX_URL + '/3',
                      json={"id": 3, "issue": {"id": 1, "description": "d"}})

        cache.get_issueregexes.cache_clear()
        compiledregex._get_compiled_issueregexes.cache_clear()  # pylint: disable=protected-access
        with self.assertLogs(level='WARN'):
            compiled_issueregexes = compiledregex.get_compiled_issueregexes([1, 2, 3])

        self.assertEqual([1, 3], [lookup.id for lookup in compiled_issueregexes])

    @responses.activate
    def test_download_lookups_selective_other_error(self):
        """Test other errors during download_lookups."""
        responses.add(responses.GET, MOCKED_REGEX_URL + '/1', status=403)

        cache.get_issueregexes.cache_clear()
        compiledregex._get_compiled_issueregexes.cache_clear()  # pylint: disable=protected-access
        with self.assertRaises(requests.exceptions.HTTPError):
            compiledregex.get_compiled_issueregexes([1])

    @responses.activate
    @patch("cki.triager.checkers.download")
    def test_use_of_dw_obj_related_build_and_checkout_attributes(self, mocked_download):
        """Verify that related_build and related_checkout are actually taken from dw_obj.

        The test relies on the fact that mock_build and checkout in mock_DW have
        different architecture and kpet_tree_name, so in case either will be taken from mock_DW,
        there will be no FULL_MATCH. Additionally, the test ensures that no API calls are made
        to the Datawarehouse to get a build or checkout.
        """
        self._mock_downloaded_regex(
            architecture_match='aarch64',
            tree_match='rhel9'
        )
        not_allowed_urls_offline_triaging = re.compile(
            r'http://datawarehouse/api/\d+/kcidb/(checkouts|builds).+')
        not_allowed_urls_offline_triaging_mock = responses.get(not_allowed_urls_offline_triaging)

        with self.subTest("Full match with dw_obj.related_build"):
            build_aarch64_dict = mock_attrs(
                architecture='aarch64', package_name='kernel', kpet_tree_name='rhel9', checkout_id=1
            )
            build_dwobj = dwobject.from_attrs('build', build_aarch64_dict)
            checkout_attrs = mock_attrs(tree_name='rhel9')
            checkout_dwobj = dwobject.from_attrs('checkout', checkout_attrs)
            mock_test_attrs = mock_attrs(
                build_id=1,
                comment='other-name',
                misc={
                    'related_build': build_dwobj,
                    'related_checkout': checkout_dwobj
                }
            )
            log_file = checkers.LogFile(
                dwobject.from_attrs('test', mock_test_attrs),
                **MOCKED_CONSOLE_LOG_DICT
            )
            match_result = checkers.match(log_file, self.regexes[0])
            self.assertEqual(match_result, checkers.MatchStatus.FULL_MATCH)
            # Ensure no data is retrieved from the API about builds or checkouts.
            self.assertEqual(len(not_allowed_urls_offline_triaging_mock.calls), 0)
