"""Test __init__.py."""
import contextlib
import copy
from http import HTTPStatus
from importlib import resources
import json
import re
import tempfile
import unittest
from unittest import mock
from unittest.mock import sentinel

from cki_lib.kcidb.file import KCIDBFile
from datawarehouse import Datawarehouse
from freezegun import freeze_time
import pika
import responses

from cki.triager import checkers
from cki.triager import compiledregex
from cki.triager import dwobject
from cki.triager import settings
from cki.triager import triager

from .. import assets
from ..utils import mock_attrs
from ..utils import tear_down_registry

tear_down_registry()

# Ref. https://datawarehouse.cki-project.org/issue/2446
ISSUE_DICT_2446 = {
    'id': 2446,
    'kind': {
        'id': 1,
        'description': 'Kernel bug',
        'tag': 'Kernel Bug'
    },
    'description': '[rhel9] kunit: regmap - not ok 19 cache_range_window_reg',
    'ticket_url': 'https://issues.redhat.com/browse/RHEL-22735',
    'resolved': True,
    'resolved_at': '2024-02-20T17:25:10.836040Z',
    'policy': {
        'id': 1,
        'name': 'public',
        'read_group': None,
        'write_group': 5
    },
    'first_seen': '2024-01-09T19:10:15.988168Z',
    'last_seen': '2024-01-30T22:23:14.721998Z'
}


class TestMain(unittest.TestCase):
    """Test __init__.py."""

    @mock.patch('cki.triager.triager.callback')
    @mock.patch('pika.BlockingConnection')
    @mock.patch.dict('os.environ', {'DATAWAREHOUSE_TRIAGER_ROUTING_KEYS': 'key'})
    def test_queue(self, connection, callback_mock):
        """Test listening to messages."""
        msg = {
            'object_type': 'checkout',
            'object': {'id': 1},
            'status': 'new',
        }
        mocked_channel = connection().channel()
        mocked_channel.consume.return_value = [(
            mock.Mock(routing_key='routing_key', delivery_tag='delivery_tag'),
            pika.BasicProperties(),
            json.dumps(msg),
        )]

        triager.main([])

        callback_mock.assert_called_once_with(
            body=msg, routing_key='routing_key', headers=None, ack_fn=None, timeout=False,
        )

    @mock.patch('cki.triager.dwobject.from_attrs', mock.Mock(return_value=sentinel.obj))
    def test_callback(self) -> None:
        """Test the callback only triages the correct objects."""
        cases = (
            ('checkout', 'new', True),
            ('checkout', 'updated', True),
            ('checkout', 'needs_triage', True),
            ('checkout', 'invalid', False),
            ('build', 'new', True),
            ('build', 'updated', True),
            ('build', 'needs_triage', True),
            ('build', 'invalid', False),
            ('test', 'needs_triage', True),
            ('test', 'new', True),
            ('test', 'updated', True),
            ('test', 'invalid', False),
            ('testresult', 'needs_triage', False),
            ('testresult', 'new', False),
            ('testresult', 'updated', False),
            ('testresult', 'invalid', False),
        )
        for object_type, status, expected in cases:
            with self.subTest(f'{object_type}-{status}'), \
                    mock.patch('cki.triager.triager.Triager.check') as check:
                triager.callback(body={
                    'object_type': object_type,
                    'object': {'id': 1},
                    'status': status,
                    'misc': {'issueregex_ids': [1, 2]},
                })
                if expected:
                    check.assert_called_once_with(sentinel.obj, [1, 2])
                else:
                    check.assert_not_called()

    @mock.patch('cki.triager.dwobject.from_attrs', mock.Mock(return_value=sentinel.obj))
    def test_callback_misc(self) -> None:
        """Test the callback handles regex ids from misc correctly."""
        cases = (
            ({'misc': {'issueregex_ids': [1, 2]}}, [1, 2]),
            ({'misc': {'issueregex_ids': []}}, []),
            ({'misc': {'issueregex_ids': None}}, []),
            ({'misc': {}}, []),
            ({'misc': None}, []),
            ({}, []),
        )
        for misc, expected in cases:
            with self.subTest(misc), \
                    mock.patch('cki.triager.triager.Triager.check') as check:
                triager.callback(body={
                    'object_type': 'checkout',
                    'object': {'id': 1},
                    'status': 'new',
                    **misc
                })
                check.assert_called_once_with(sentinel.obj, expected)

    @mock.patch("cki.triager.dwobject.from_obj_id", return_value=sentinel.obj)
    @mock.patch("cki.triager.triager.KCIDBFile")
    @mock.patch("cki.triager.triager.triage_from_file", return_value=[{"triage_from_file": None}])
    @mock.patch("cki.triager.triager.Triager.check", return_value=[{"check": None}])
    def test_main(self, mock_check, mock_triage_from_file, mock_kcidb_file, mock_from_obj_id):
        """Test main works as expected."""
        from_file = resources.files(assets) / "kcidb_all_checkout_127491_aarch64_reduced.json"
        kcidb_file = KCIDBFile(from_file)
        mock_kcidb_file.return_value = kcidb_file
        checkout_id = "redhat:1163293372"
        build_id = "redhat:1163293372-x86_64-kernel"
        test_id = "redhat:1163293372-aarch64-kernel_upt_7"
        cases = (
            (
                "no regex",
                ["--checkout-id", "1"],
                [mock.call(sentinel.obj, [], to_dw=False)],
                [],
                [mock.call(obj_type="checkout", obj_id="1")],
                None,
            ),
            (
                "with regex",
                ["--build-id", "redhat:1", "--regex-id", "7"],
                [mock.call(sentinel.obj, ["7"], to_dw=False)],
                [],
                [mock.call(obj_type="build", obj_id="redhat:1")],
                None,
            ),
            (
                "to-dw",
                ["--test-id", "312", "--to-dw"],
                [mock.call(sentinel.obj, [], to_dw=True)],
                [],
                [mock.call(obj_type="test", obj_id="312")],
                None,
            ),
            (
                "from-dw with ID",
                ["--from-dw", "--checkout-id", "1"],
                [mock.call(sentinel.obj, [], to_dw=False)],
                [],
                [mock.call(obj_type="checkout", obj_id="1")],
                None,
            ),
            (
                "from-dw missing ID",
                ["--from-dw"],
                [],
                [],
                [],
                ValueError("Selected --from-dw without a KCIDB object to filter."),
            ),
            (
                "from-file",
                ["--from-file", str(from_file)],
                [],
                [
                    mock.call(
                        kcidb_file=kcidb_file,
                        issueregex_ids=[],
                        to_dw=False,
                        # checkout_id=None,
                        # build_id=None,
                        # test_id=None,
                    )
                ],
                [],
                None,
            ),
            (
                "from-file with checkout-id",
                ["--from-file", str(from_file), "--checkout-id", checkout_id],
                [],
                [
                    mock.call(
                        kcidb_file=kcidb_file,
                        issueregex_ids=[],
                        to_dw=False,
                        checkout_id=checkout_id,
                        # build_id=None,
                        # test_id=None,
                    )
                ],
                [],
                None,
            ),
            (
                "from-file with build-id",
                ["--from-file", str(from_file), "--build-id", build_id],
                [],
                [
                    mock.call(
                        kcidb_file=kcidb_file,
                        issueregex_ids=[],
                        to_dw=False,
                        # checkout_id=None,
                        build_id=build_id,
                        # test_id=None,
                    )
                ],
                [],
                None,
            ),
            (
                "from-file with test-id",
                ["--from-file", str(from_file), "--test-id", test_id],
                [],
                [
                    mock.call(
                        kcidb_file=kcidb_file,
                        issueregex_ids=[],
                        to_dw=False,
                        # checkout_id=None,
                        # build_id=None,
                        test_id=test_id,
                    )
                ],
                [],
                None,
            ),
            (
                "to-file",
                ["--from-file", str(from_file), "--to-file", str(from_file)],
                [],
                [
                    mock.call(
                        kcidb_file=kcidb_file,
                        issueregex_ids=[],
                        to_dw=False,
                        # checkout_id=None,
                        # build_id=None,
                        # test_id=None,
                    )
                ],
                [],
                None,
            ),
        )
        for (
            description,
            args,
            expected_check,
            expected_triage_from_file,
            expected_from_obj_id,
            expected_exception,
        ) in cases:
            with self.subTest(description):
                with (
                    self.assertRaisesRegex(type(expected_exception), str(expected_exception))
                    if expected_exception
                    else contextlib.nullcontext()
                ):
                    triager.main(args)

                self.assertEqual(mock_from_obj_id.mock_calls, expected_from_obj_id)
                self.assertEqual(mock_check.mock_calls, expected_check)
                self.assertEqual(mock_triage_from_file.mock_calls, expected_triage_from_file)

            mock_from_obj_id.reset_mock()
            mock_check.reset_mock()
            mock_triage_from_file.reset_mock()

    @mock.patch('cki.triager.triager.IS_PRODUCTION_OR_STAGING', True)
    def test_report_issue(self) -> None:
        """Test report issue."""
        obj = mock.Mock()
        obj.issues.list = mock.Mock(return_value=[mock.Mock(id=3)])
        issues = [
            checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                mock.Mock(dw_obj=obj), mock.Mock(issue_id=1)),
            checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                mock.Mock(dw_obj=obj), mock.Mock(issue_id=2)),
            checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                mock.Mock(dw_obj=obj), mock.Mock(issue_id=2)),
            checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                mock.Mock(dw_obj=obj), mock.Mock(issue_id=3)),
        ]

        triager.Triager().report_issues(issues)
        self.assertEqual(obj.issues.create.mock_calls, [
            mock.call(issue_id=1),
            mock.call(issue_id=2),
        ])

    @staticmethod
    @mock.patch('cki.triager.triager.IS_PRODUCTION_OR_STAGING', False)
    def test_report_issue_dry() -> None:
        """Test report issue. Dry run."""
        obj = mock.Mock()
        obj.issues.list = mock.Mock(return_value=[mock.Mock(id=1)])
        issues = [
            checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                mock.Mock(dw_obj=obj), mock.Mock(issue_id=1)),
            checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                mock.Mock(dw_obj=obj), mock.Mock(issue_id=2)),
        ]

        triager.Triager().report_issues(issues)
        obj.issues.create.assert_not_called()

    @responses.activate
    @mock.patch('cki.triager.settings.DW_CLIENT', Datawarehouse('https://dw'))
    def test_triager_check(self):
        """Test Triager.check()."""
        # First argument passed to Triager.check()
        dw_obj = dwobject.from_attrs("checkout", attrs=mock_attrs())
        # Mock to be returned as matches from checkers.triage()
        log_file = checkers.LogFile(dw_obj=dw_obj, name="console.log", url="http://server/c.txt")

        default_reported_match = checkers.RegexMatch(
            checkers.MatchStatus.FULL_MATCH,
            log_file,
            mock.Mock(issue={"id": 1}, spec=compiledregex.CompiledIssueRegex),
        )
        default_test_case = {
            # Input
            "issueregex_ids": [],  # Second argument passed to Triager.check()
            "to_dw": True,  # Third argument passed to Triager.check()
            "result": checkers.TriageStatus.SUCCESS,  # Status returned by checkers.triage()
            "matches": [  # Matches returned by checkers.triage()
                default_reported_match,
                checkers.RegexMatch(
                    checkers.MatchStatus.PARTIAL_MATCH,
                    log_file,
                    mock.Mock(issue={"id": 2}, spec=compiledregex.CompiledIssueRegex),
                ),
                checkers.RegexMatch(
                    checkers.MatchStatus.NO_MATCH,
                    log_file,
                    mock.Mock(issue={"id": 3}, spec=compiledregex.CompiledIssueRegex),
                ),
            ],
            "prod": True,  # Whether the deployment_environment should be prod/staging or not
            "status": HTTPStatus.OK,  # Which status code to respond from DW's "triaged" endpoint
            # Output
            "exp_reported": [default_reported_match],  # Only FULL_MATCH is reported
            "exp_triaged": True,
            "exp_raise": False,
            "exp_log": None,
            "exp_result": [
                {
                    "checkout_id": "redhat:1",
                    "build_id": None,
                    "test_id": None,
                    "testresult_id": None,
                    "issue": {"id": 1},
                }
            ],
        }

        cases = {
            "default": {},
            "with default matches and with issueregex ids": {"issueregex_ids": [5]},
            "no matches and no issueregex ids": {
                "issueregex_ids": [],
                "matches": [],
                "exp_reported": [],  # no matches -> nothing to report
                "exp_result": [],  # no matches -> no issueoccurrences
            },
            "no matches and with issueregex ids": {
                "issueregex_ids": [5],
                "matches": [],
                "exp_reported": [],  # no matches -> nothing to report
                "exp_triaged": False,
                "exp_result": [],  # no matches -> no issueoccurrences
                "exp_log": {
                    "level": "INFO",
                    "msg": (
                        f"Not marking as triaged as individual regex without matches obj={dw_obj}"
                    ),
                },
            },
            "incomplete": {
                "result": checkers.TriageStatus.INCOMPLETE,
                "exp_triaged": False,
                "exp_log": {
                    "level": "INFO",
                    "msg": f"Not marking as triaged because incomplete obj={dw_obj}",
                },
            },
            "deployment_environment is not prod|staging": {
                "prod": False,
                "exp_triaged": False,
                "exp_log": {
                    "level": "INFO",
                    "msg": f"Would mark obj={dw_obj} as triaged in prod",
                },
            },
            "object not found in DW while trying to report incident should just log": {
                "status": HTTPStatus.NOT_FOUND,
                "exp_log": {
                    "level": "ERROR",
                    "msg": f"Unable to find {dw_obj!r}, assuming it was deleted",
                },
            },
            "any other HTTP error from DW should raise, which enqueues the payload for retry": {
                "status": HTTPStatus.BAD_REQUEST,
                "exp_raise": True,
                "exp_result": "N/A",  # raise -> no return
            },
            "offline": {
                "to_dw": False,
                "exp_reported": False,
                "exp_triaged": False,
            },
        }
        for description, test_case_modifier in cases.items():
            test_case = {**default_test_case, **test_case_modifier}
            triage_result = checkers.TriageResult(test_case["result"], test_case["matches"])
            with (
                self.subTest(description),
                responses.RequestsMock(assert_all_requests_are_fired=False) as rsps,
                mock.patch("cki.triager.triager.IS_PRODUCTION_OR_STAGING", test_case["prod"]),
                mock.patch("cki.triager.triager.Triager.report_issues") as report_issues,
                mock.patch("cki.triager.checkers.triage", return_value=triage_result) as triage,
            ):
                triaged_id = rsps.post(
                    f"https://dw/api/1/kcidb/checkouts/{dw_obj.id}/actions/triaged",
                    json={},
                    status=test_case["status"],
                )

                assert_raises = (
                    self.assertRaises(Exception)
                    if test_case["exp_raise"]
                    else contextlib.nullcontext()
                )
                assert_logs = (
                    self.assertLogs(logger=triager.LOGGER, level=test_case["exp_log"]["level"])
                    if test_case["exp_log"]
                    else contextlib.nullcontext()
                )

                with assert_raises, assert_logs as log_ctx:
                    # Actual call!
                    result = triager.Triager().check(
                        dw_obj, test_case["issueregex_ids"], to_dw=test_case["to_dw"]
                    )

                    self.assertEqual(result, test_case["exp_result"])

                if exp_log := test_case["exp_log"]:
                    expected_log = f"{exp_log['level']}:{triager.LOGGER.name}:{exp_log['msg']}"
                    self.assertIn(expected_log, log_ctx.output)

                triage.assert_called_with(dw_obj, test_case["issueregex_ids"])
                if test_case["exp_reported"] is False:
                    report_issues.assert_not_called()
                else:
                    report_issues.assert_called_with(test_case["exp_reported"])
                self.assertEqual(triaged_id.call_count, 1 if test_case["exp_triaged"] else 0)


@mock.patch('cki.triager.settings.DW_CLIENT', Datawarehouse('http://datawarehouse'))
class TestTriagerWithRealData(unittest.TestCase):
    def setUp(self):
        MOCKED_AVC_LOG_CONTENT = (resources.files(assets) / 'regex_2396_avc.log').read_text('utf-8')
        MOCKED_TASKOUT_LOG_CONTENT = (resources.files(assets) / 'regex_1866_taskout.log').read_text(
            'utf-8')
        MOCKED_REGMAP_LOG_CONTENT = (resources.files(assets) / 'regex_2413_regmap.log').read_text(
            'utf-8')

        regexes = {
            1866: {
                'regex_api_response': {
                    "id": 1866,
                    "issue": {
                        "id": 1859,
                        "kind": {
                            "id": 3,
                            "description": "Unstable Test",
                            "tag": "Unstable Test"
                        },
                        "description": ("Firmware test suite: mv: cannot stat "
                                        "'dtcompilerparser.tab.c': No such file or directory"),
                        "ticket_url": ("https://gitlab.com/redhat/centos-stream/tests/kernel/"
                                       "kernel-tests/-/issues/1544"),
                        "resolved": True,
                        "resolved_at": "2024-03-26T15:57:10.991968Z",
                        "policy": {
                            "id": 1,
                            "name": "public",
                            "read_group": None,
                            "write_group": 5
                        },
                        "first_seen": "2023-01-12T12:30:56.016809Z",
                        "last_seen": "2024-02-27T12:34:10.154613Z"
                    },
                    "text_match": ("mv: cannot stat 'dtcompilerparser\\.tab\\.c': "
                                   "No such file or directory"),
                    "file_name_match": "taskout\\.log",
                    "test_name_match": "Firmware test suite",
                    "testresult_name_match": ".*",
                    "architecture_match": None,
                    "tree_match": None,
                    "kpet_tree_name_match": "(fedora-latest|rawhide|eln)",
                    "package_name_match": ""
                },
                'logfiles': [{
                    'url': ('https://s3.amazonaws.com/arr-cki-prod-trusted-artifacts/'
                            'trusted-artifacts/1163293372/test_aarch64/6090070673/artifacts/'
                            'run.done.04/job.01/recipes/15504814/tasks/7/logs/taskout.log'),
                    'content': MOCKED_TASKOUT_LOG_CONTENT
                }]
            },
            2396: {
                'regex_api_response': {
                    "id": 2396,
                    "issue": {
                        "id": 2431,
                        "kind": {
                            "id": 7,
                            "description": "Bug in a non-Kernel component",
                            "tag": "Non-Kernel Bug"
                        },
                        "description": ("[rawhide] avc:  denied  { read write } for  pid=12364 "
                                        "comm=\"plymouthd\" name=\"kmsg\" dev=\"devtmpfs\" ino=10 "
                                        "scontext=system_u:system_r:plymouthd_t:s0 "
                                        "tcontext=system_u:object_r:kmsg_device_t:s0 "
                                        "tclass=chr_file permissive=1"),
                        "ticket_url": "https://bugzilla.redhat.com/show_bug.cgi?id=2256442",
                        "resolved": False,
                        "resolved_at": None,
                        "policy": {
                            "id": 1,
                            "name": "public",
                            "read_group": None,
                            "write_group": 5
                        },
                        "first_seen": "2023-12-22T23:23:11.468797Z",
                        "last_seen": "2024-02-27T12:34:10.154613Z"
                    },
                    "text_match": ("avc:  denied  { read write } for  pid=\\d+ comm=\"plymouthd\" "
                                   "name=\"kmsg\" dev=\"devtmpfs\" ino=\\d+ "
                                   "scontext=system_u:system_r:plymouthd_t:s0 "
                                   "tcontext=system_u:object_r:kmsg_device_t:s0 "
                                   "tclass=chr_file permissive=\\d+"),
                    "file_name_match": "avc\\.log",
                    "test_name_match": ".*",
                    "testresult_name_match": "/10_avc_check",
                    "architecture_match": None,
                    "tree_match": None,
                    "kpet_tree_name_match": "(upstream|rawhide|eln)",
                    "package_name_match": ""
                },
                'logfiles': [{
                    'url': ('https://s3.amazonaws.com/arr-cki-prod-trusted-artifacts/'
                            'trusted-artifacts/1163293372/test_aarch64/6090070673/artifacts/'
                            'run.done.04/job.01/recipes/15504814/tasks/8/results/1707005666/'
                            'logs/avc.log'),
                    'content': MOCKED_AVC_LOG_CONTENT
                }],

            },
            2413: {
                'regex_api_response': {
                    "id": 2413,
                    "issue": {
                        "id": 2446,
                        "kind": {
                            "id": 1,
                            "description": "Kernel bug",
                            "tag": "Kernel Bug"
                        },
                        "description": "[rhel9] kunit: regmap - not ok 19 cache_range_window_reg",
                        "ticket_url": "https://issues.redhat.com/browse/RHEL-22735",
                        "resolved": True,
                        "resolved_at": "2024-02-20T17:25:10.836040Z",
                        "policy": {
                            "id": 1,
                            "name": "public",
                            "read_group": None,
                            "write_group": 5
                        },
                        "first_seen": "2024-01-09T19:10:15.988168Z",
                        "last_seen": "2024-01-30T22:23:14.721998Z"
                    },
                    "text_match": ("# cache_range_window_reg: ASSERTION FAILED at "
                                   "drivers\\/base\\/regmap\\/regmap-kunit\\.c:927"),
                    "file_name_match": "regmap\\.log",
                    "test_name_match": "KUNIT",
                    "testresult_name_match": "process-regmap",
                    "architecture_match": None,
                    "tree_match": "(rhel9|c9s)",
                    "kpet_tree_name_match": None,
                    "package_name_match": None
                },
                'logfiles': [{
                    'url': ('https://s3.amazonaws.com/arr-cki-prod-trusted-artifacts/'
                            'trusted-artifacts/1157451134/test_aarch64/6051893367/artifacts/'
                            'run.done.02/job.01/recipes/15469850/tasks/8/logs/regmap.log'),
                    'content': MOCKED_REGMAP_LOG_CONTENT
                }]
            }
        }
        regexes_api_response = {
            'count': '2',
            'next': None,
            'previous': None,
            'results': [regex['regex_api_response'] for regex in regexes.values()]
        }
        responses.get('http://datawarehouse/api/1/issue/-/regex', json=regexes_api_response)
        self.mocked_console_log_dict = {'name': 'console.log', 'url': 'http://server/console.log'}

        for regex_id, regex_structure in regexes.items():
            responses.get(f'http://datawarehouse/api/1/issue/-/regex/{regex_id}',
                          json=regex_structure['regex_api_response'])
            for logfile in regex_structure['logfiles']:
                responses.head(logfile['url'])
                responses.get(logfile['url'], logfile['content'])

        build_aarch64_dict = mock_attrs(
            architecture='aarch64', package_name='kernel', kpet_tree_name='rhel9', checkout_id=1
        )
        self.build_dwobj = dwobject.from_attrs('build', build_aarch64_dict)
        self.checkout_attrs = mock_attrs(tree_name='rhel9')
        self.checkout_dwobj = dwobject.from_attrs('checkout', self.checkout_attrs)
        path_to_kcidb_all_file = (resources.files(assets) /
                                  'kcidb_all_checkout_127491_aarch64_reduced.json')
        self.kcidb_file = KCIDBFile(path_to_kcidb_all_file)

    def tearDown(self):
        responses.stop()
        responses.reset()

    @responses.activate
    def test_triager_check_offline(self):
        """
        Test triager.check function with to_dw=False parameter.

        In this test checkers.triage function is patched with mock to return 2 full matches.
        It is expected that 2 issueoccurrence dictionaries with known attributes will be returned.
        Only select attributes are checked to confirm that function works as expected.
        """
        mock_test_attrs = mock_attrs(
            build_id=1,
            comment='other-name',
            related_build=self.build_dwobj,
            related_checkout=self.checkout_dwobj
        )
        dw_obj_test = dwobject.from_attrs('test', mock_attrs())
        log_file_test = checkers.LogFile(
            dwobject.from_attrs('test', mock_test_attrs),
            **self.mocked_console_log_dict
        )
        regex = settings.DW_CLIENT.issue_regex.get('2413')
        match_obj = checkers.RegexMatch(
            status=checkers.MatchStatus.FULL_MATCH,
            log_file=log_file_test,
            regex=regex
        )
        triage_result_test = checkers.TriageResult(status=checkers.TriageStatus.SUCCESS,
                                                   matches=[match_obj, match_obj])

        with mock.patch('cki.triager.checkers.triage', mock.Mock(return_value=triage_result_test)):
            result = triager.Triager().check(dw_obj_test, [2413], to_dw=False)
            self.assertEqual(len(result), 2)
            self.assertEqual(result[0]['issue']['id'], 2446)
            self.assertEqual(result[0]['issue']['description'],
                             '[rhel9] kunit: regmap - not ok 19 cache_range_window_reg')
            self.assertEqual(result[0]['issue']['ticket_url'],
                             'https://issues.redhat.com/browse/RHEL-22735')
            self.assertEqual(result[0]['test_id'], 'redhat:1')

            self.assertEqual(result[1]['issue']['id'], 2446)
            self.assertEqual(result[1]['test_id'], 'redhat:1')

            # Ensure build_id and checkout_id are not set, this is because only the id of the type
            # of obj processed must be set.
            for result_dict in result:
                self.assertIsNone(result_dict['build_id'])
                self.assertIsNone(result_dict['checkout_id'])
                self.assertIsNone(result_dict['testresult_id'])

    @freeze_time("2024-02-27T12:34:10.154613Z")
    @mock.patch("cki.triager.triager.triage_from_file")
    def test_main_input_output_file(self, triage_from_file_mocked):
        """
        Ensure that issueoccurrences returned by Triager.check are written correctly.

        Processing of --from-file and --to-file arguments is verified as well.
        1. Patch triage_from_file function.
           a. Ensure that the function is called with expected arguments.
        2. Check that all issueoccurrences are written to the output file.
        3. Ensure that no changes are done to kcidb_all, except for issueoccurrences.
        """
        # From triaging checkout ID 127491
        issueoccurrences = [
            {
                "issue": {
                    "id": 1859,
                    "kind": {
                        "id": 3,
                        "description": "Unstable Test",
                        "tag": "Unstable Test"
                    },
                    "description": "Firmware test suite: mv: cannot stat 'dtcompilerparser.tab.c': "
                                   "No such file or directory",
                    "ticket_url": "https://gitlab.com/redhat/centos-stream/tests/kernel/"
                                  "kernel-tests/-/issues/1544",
                    "resolved": False,
                    "resolved_at": None,
                    "policy": {
                        "id": 1,
                        "name": "public",
                        "read_group": None,
                        "write_group": 5
                    },
                    "first_seen": "2023-01-12T12:30:56.016809Z",
                    "last_seen": "2024-02-27T12:34:10.154613Z"
                },
                "build_id": None,
                "checkout_id": None,
                "test_id": "redhat:1163293372-aarch64-kernel_upt_40",
                "testresult_id": None
            },
            {
                "issue": {
                    "id": 1859,
                    "kind": {
                        "id": 3,
                        "description": "Unstable Test",
                        "tag": "Unstable Test"
                    },
                    "description": "Firmware test suite: mv: cannot stat 'dtcompilerparser.tab.c': "
                                   "No such file or directory",
                    "ticket_url": "https://gitlab.com/redhat/centos-stream/tests/kernel/"
                                  "kernel-tests/-/issues/1544",
                    "resolved": False,
                    "resolved_at": None,
                    "policy": {
                        "id": 1,
                        "name": "public",
                        "read_group": None,
                        "write_group": 5
                    },
                    "first_seen": "2023-01-12T12:30:56.016809Z",
                    "last_seen": "2024-02-27T12:34:10.154613Z"
                },
                "build_id": None,
                "checkout_id": None,
                "test_id": "redhat:1163293372-aarch64-kernel_upt_40",
                "testresult_id": "redhat:1163293372-aarch64-kernel_upt_40.1707003710"
            },
            {
                "issue": {
                    "id": 2431,
                    "kind": {
                        "id": 7,
                        "description": "Bug in a non-Kernel component",
                        "tag": "Non-Kernel Bug"
                    },
                    "description": "[rawhide] avc:  denied  { read write } for  pid=12364 "
                                   "comm=\"plymouthd\" name=\"kmsg\" dev=\"devtmpfs\" ino=10 "
                                   "scontext=system_u:system_r:plymouthd_t:s0 "
                                   "tcontext=system_u:object_r:kmsg_device_t:s0 "
                                   "tclass=chr_file permissive=1",
                    "ticket_url": "https://bugzilla.redhat.com/show_bug.cgi?id=2256442",
                    "resolved": False,
                    "resolved_at": None,
                    "policy": {
                        "id": 1,
                        "name": "public",
                        "read_group": None,
                        "write_group": 5
                    },
                    "first_seen": "2023-12-22T23:23:11.468797Z",
                    "last_seen": "2024-02-27T12:34:10.154613Z"
                },
                "build_id": None,
                "checkout_id": None,
                "test_id": "redhat:1163293372-aarch64-kernel_upt_41",
                "testresult_id": "redhat:1163293372-aarch64-kernel_upt_41.1707005666"
            }
        ]

        triage_from_file_mocked.return_value = issueoccurrences

        asset_root = resources.files(assets)
        input_file_location = str(asset_root / "kcidb_all_checkout_127491_aarch64_reduced.json")
        expected_path = asset_root / "kcidb_all_checkout_127491_aarch64_reduced_and_triaged.json"

        with (
            tempfile.NamedTemporaryFile("w+", prefix="test-triager", encoding="utf8") as out_file,
            open(expected_path, encoding="utf8") as expected_result_file,
        ):
            triager.main(["--from-file", input_file_location, "--to-file", out_file.name])

            last_kcidb_file = triage_from_file_mocked.call_args.kwargs["kcidb_file"]
            self.assertEqual(last_kcidb_file.checkout["id"], "redhat:1163293372")
            triage_from_file_mocked.assert_called_once_with(
                kcidb_file=last_kcidb_file, issueregex_ids=[], to_dw=False
            )
            expected_result_dict = json.load(expected_result_file)
            actual_result_dict = json.load(out_file)

        self.assertDictEqual(actual_result_dict, expected_result_dict)

    @freeze_time("2024-02-27T12:34:10.154613Z")
    @responses.activate
    def test_get_issueoccurrence_from_match(self):
        """Given a match, ensure that expected issueoccurrence is returned."""
        mock_test_attrs = mock_attrs(
            build_id=1,
            comment='other-name',
            misc={
                'related_build': self.build_dwobj,
                'related_checkout': self.checkout_dwobj
            }
        )
        test_dwobj = dwobject.from_attrs('test', mock_test_attrs)
        testresult_dwobj = dwobject.from_attrs('testresult', mock_test_attrs.copy())
        log_file_test = checkers.LogFile(
            test_dwobj,
            **self.mocked_console_log_dict
        )
        log_file_testresult = checkers.LogFile(
            testresult_dwobj,
            dw_test=test_dwobj,
            **self.mocked_console_log_dict,
        )
        regex = settings.DW_CLIENT.issue_regex.get('2413')
        match_obj_test = checkers.RegexMatch(
            status=checkers.MatchStatus.FULL_MATCH,
            log_file=log_file_test,
            regex=regex
        )
        match_obj_testresult = checkers.RegexMatch(
            status=checkers.MatchStatus.FULL_MATCH,
            log_file=log_file_testresult,
            regex=regex,
        )
        expected_issue_dict_test = {
            'issue': ISSUE_DICT_2446,
            'checkout_id': None,
            'build_id': None,
            'test_id': 'redhat:1',
            'testresult_id': None
        }
        expected_issue_dict_testresult = expected_issue_dict_test.copy()
        expected_issue_dict_testresult['testresult_id'] = 'redhat:1'

        with self.subTest("Ensure test_id is attached to issueoccurrence dict, but no build_id"
                          "or checkout_id for KCIDBTest"):
            actual_issueoccurrence_dict = checkers.get_issueoccurrence_from_match(match_obj_test)
            self.assertDictEqual(actual_issueoccurrence_dict, expected_issue_dict_test)

        with self.subTest("Ensure test_id and testresult_id are present in the issueoccurrence"
                          "for KCIDBTestResult"):
            actual_issueoccurrence_dict = checkers.get_issueoccurrence_from_match(
                match_obj_testresult
            )
            self.assertDictEqual(actual_issueoccurrence_dict, expected_issue_dict_testresult)

    @freeze_time("2024-02-27T12:34:10.154613Z")
    @responses.activate
    def test_triage_from_file_to_dw_false(self):
        """Test triage_from_file function using kcidb_all file."""
        # During triaging from file, the API shouldn't be reached to get build or checkout
        # information.
        not_allowed_urls = re.compile(r'http://datawarehouse/api/\d+/kcidb/(checkouts|builds).+')
        not_allowed_urls_offline_triaging_mock = responses.get(not_allowed_urls)
        with self.subTest('Test offline triage from file using RegEx with ID 1866'):
            actual_result = triager.triage_from_file(self.kcidb_file, [1866], to_dw=False)
            exp_testresult_id_40 = 'redhat:1163293372-aarch64-kernel_upt_40.1707003710'
            exp_test_issueoccurrence = {
                'issue': {
                    'id': 1859,
                    'kind': {
                        'id': 3,
                        'description': 'Unstable Test',
                        'tag': 'Unstable Test'
                    },
                    'description': ("Firmware test suite: mv: cannot stat "
                                    "'dtcompilerparser.tab.c': No such file or directory"),
                    'ticket_url': ('https://gitlab.com/redhat/centos-stream/tests/kernel/'
                                   'kernel-tests/-/issues/1544'),
                    'resolved': True,
                    'resolved_at': '2024-03-26T15:57:10.991968Z',
                    'policy': {
                        'id': 1,
                        'name': 'public',
                        'read_group': None,
                        'write_group': 5
                    },
                    'first_seen': '2023-01-12T12:30:56.016809Z',
                    'last_seen': '2024-02-27T12:34:10.154613Z'
                },
                'build_id': None,
                'checkout_id': None,
                'test_id': 'redhat:1163293372-aarch64-kernel_upt_40',
                'testresult_id': None
            }
            exp_testresult_issueoccurrence = copy.deepcopy(exp_test_issueoccurrence)
            exp_testresult_issueoccurrence[
                'testresult_id'] = exp_testresult_id_40
            # The expected result should contain 2 issueoccurrences: one for test, another for
            # testresult. The test issueoccurrence must not contain build_id and checkout_id, as
            # the issueoccurrences are produced for test and testresult, not for build or checkout.
            expected_testresult_ids = [None, exp_testresult_id_40]
            actual_testresult_ids = [issueoccurrence['testresult_id'] for issueoccurrence in
                                     actual_result]
            self.assertCountEqual(expected_testresult_ids, actual_testresult_ids)
            self.assertEqual(actual_result,
                             [exp_test_issueoccurrence, exp_testresult_issueoccurrence])

        with self.subTest('Test offline triage from file without specifying RegEx IDs'):
            actual_result = triager.triage_from_file(self.kcidb_file, [], to_dw=False)
            expected_testresult_ids = [None, exp_testresult_id_40,
                                       'redhat:1163293372-aarch64-kernel_upt_41.1707005666']
            actual_testresult_ids = [issueoccurrence['testresult_id'] for issueoccurrence in
                                     actual_result]
            self.assertCountEqual(expected_testresult_ids, actual_testresult_ids)

            # Ensure that build_id and checkout_id are not set
            for issueoccurrence in actual_result:
                self.assertIsNone(issueoccurrence['build_id'])
                self.assertIsNone(issueoccurrence['checkout_id'])

            expected_issueoccurrence_ids = [1859, 1859, 2431]
            actual_issueoccurrence_ids = [issueoccurrence['issue']['id'] for issueoccurrence in
                                          actual_result]
            self.assertCountEqual(expected_issueoccurrence_ids, actual_issueoccurrence_ids)

        # Ensure neither build nor checkout has been retrieved from the API, as information about
        # the build or checkout must be taken from the kcidb_all file.
        self.assertEqual(len(not_allowed_urls_offline_triaging_mock.calls), 0)

    @mock.patch("cki.triager.triager.Triager.check")
    def test_triage_from_file_mocking_check(self, mock_check):
        """Test triage_from_file function mocking Triager.check()."""
        issueregex_ids = sentinel.issueregex_ids
        to_dw = sentinel.to_dw
        mock_check.side_effect = lambda dw_obj, issueregex_ids, to_dw: [
            (dw_obj.id, issueregex_ids, to_dw)
        ]
        result = triager.triage_from_file(self.kcidb_file, issueregex_ids, to_dw=to_dw)

        expected_dw_obj_ids = (
            [c["id"] for c in self.kcidb_file.data["checkouts"]]
            + [b["id"] for b in self.kcidb_file.data["builds"]]
            + [t["id"] for t in self.kcidb_file.data["tests"]]
        )
        self.assertEqual([r[0] for r in result], expected_dw_obj_ids)
        self.assertTrue(all(r[1] == issueregex_ids for r in result))
        self.assertTrue(all(r[2] == to_dw for r in result))

    @mock.patch("cki.triager.dwobject.filter_data", wraps=dwobject.filter_data)
    @mock.patch("cki.triager.triager.Triager.check")
    def test_triage_from_file_filtered(self, mock_check, mock_filter_data):
        """Test triage_from_file function mocking Triager.check()."""
        issueregex_ids = sentinel.issueregex_ids
        to_dw = sentinel.to_dw
        mock_check.side_effect = lambda dw_obj, issueregex_ids, to_dw: [
            (dw_obj.id, issueregex_ids, to_dw)
        ]

        checkout_id = "redhat:1163293372"
        build_id = "redhat:1163293372-x86_64-kernel"
        test_id = "redhat:1163293372-aarch64-kernel_upt_7"

        cases = [
            (
                {"checkout_id": checkout_id},
                (
                    [checkout_id]
                    + [
                        b["id"]
                        for b in self.kcidb_file.data["builds"]
                        if b["checkout_id"] == checkout_id
                    ]
                    + [
                        t["id"]
                        for t in self.kcidb_file.data["tests"]
                        if t["id"].startswith(checkout_id)  # heuristic that works with the fixture
                    ]
                ),
            ),
            (
                {"build_id": build_id},
                (
                    ["redhat:1163293372", build_id]
                    + [t["id"] for t in self.kcidb_file.data["tests"] if t["build_id"] == build_id]
                ),
            ),
            (
                {"test_id": test_id},
                ["redhat:1163293372", "redhat:1163293372-aarch64-kernel", test_id],
            ),
        ]
        for kcidb_filter, expected_dw_obj_ids in cases:
            with self.subTest(kcidb_filter):
                result = triager.triage_from_file(
                    self.kcidb_file, issueregex_ids, to_dw=to_dw, **kcidb_filter
                )

                self.assertEqual([r[0] for r in result], expected_dw_obj_ids)
                self.assertTrue(all(r[1] == issueregex_ids for r in result))
                self.assertTrue(all(r[2] == to_dw for r in result))

                mock_filter_data.assert_called_once_with(self.kcidb_file.data, **kcidb_filter)
            mock_filter_data.reset_mock()

    def test_input_file_does_not_exist(self):
        """Ensure main raises FileNotFoundError when input file can't be found on disk."""
        with self.assertRaises(FileNotFoundError):
            triager.main(["--from-file", "the_file_which_does_not_exist.json"])
