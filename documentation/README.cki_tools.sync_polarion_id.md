---
title: cki_tools.sync_polarion_id
linkTitle: sync_polarion_id
description: Sync the Polarion ID from test TMT metadata to kpet-db
---

## Requirements

1. The test needs to have tmt [metadata](https://tmt.readthedocs.io/).
2. The test name listed by tmt should starts with the same name as the location on [kpet-db](https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db).
3. The TMT test name that matches the location in kpet-db and
also matches the environment variables defined will be used.

    Example:

    | kpet-db location                    | kpet-db environment                     | TMT test name                                | TMT test environment |
    |-------------------------------------|-----------------------------------------|----------------------------------------------|----------------------|
    | distribution/ltp/lite/lite          |                                         | /distribution/ltp/lite/lite                  |                      |
    | kunit                               | FALSESTRINGS: "BUG: KASAN\|BUG: KFENCE" | /kunit                                       |                      |
    | general/ebpf-tracing/selftest_bpf   |                                         | /general/ebpf-tracing/selftest_bpf/complete  |                      |

4. The test needs to have `id` field defined in the metadata.
5. The ID value has to be on uuid4 format.

## Usage

```shell
usage: python3 -m cki_tools.sync_polarion_id --kpet-db-path ../kpet-db

Creates the external.j2 file on ../kpet-db directory

options:
  -h, --help                show this help message and exit
  --kpet-db-path kpet-db    path to kpet-db directory
```

## Configuration via environment variables

The following variables need to be defined:

| Name                           | Type   | Secret | Required | Description                                                                                                |
|--------------------------------|--------|--------|----------|------------------------------------------------------------------------------------------------------------|
| `CKI_DEPLOYMENT_ENVIRONMENT`   | string | no     | no       | Define the deployment environment (production/staging)                                                     |
| `CKI_LOGGING_LEVEL`            | string | no     | no       | logging level for CKI modules, defaults to WARN; to get meaningful output on the command line, set to INFO |
| `SENTRY_DSN`                   | url    | yes    | no       | Sentry DSN                                                                                                 |
