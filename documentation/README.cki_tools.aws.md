---
title: cki_tools.aws
linkTitle: aws
description: Helpers for AWS
aliases: [/l/aws-helper-docs]
---

The `aws` tool allows to log into AWS via SAML.

```text
$ python3 -m cki_tools.aws --help
usage: python3 -m cki_tools.aws [-h] {login} ...

Helpers for AWS

positional arguments:
  {login}
    login     Log into AWS via SAML

options:
  -h, --help  show this help message and exit
```

The following CLI aliases are provided:

- `cki_aws_login` == `python3 -m cki_tools.aws login`

## Environment variables

| Name                          | Secret | Required | Description                                                        |
|-------------------------------|--------|----------|--------------------------------------------------------------------|
| `AWS_IDP_URL`                 | no     | yes      | IdP URL for AWS SAML login                                         |
| `AWS_SHARED_CREDENTIALS_FILE` | no     | no       | Path to the AWS credentials file, defaults to `~/.aws/credentials` |

## `cki_aws_login`

```text
$ python3 -m cki_tools.aws login --help
usage: python3 -m cki_tools.aws login [-h]
                    [--profile PROFILE]
                    [--account ACCOUNT]
                    [--role ROLE]
                    [--duration DURATION]

Log into AWS via SAML

options:
  -h, --help           show this help message and exit
  --profile PROFILE    AWS configuration profile
  --account ACCOUNT    AWS account
  --role ROLE          role to assume
  --duration DURATION  session duration
```

Log into AWS via SAML.

A Kerberos ticket is necessary to get past SSO.

If successful and a profile name is provided, the credentials will be stored in
`AWS_SHARED_CREDENTIALS_FILE`.
