---
title: cki.triager
linkTitle: triager
description: Tag known build and test failures in DataWarehouse
---

This project aims to automatically triage common pipeline failures.
Using simple checks and regexes, datawarehouse-triager is able to
report failures on the DataWarehouse.

There are 8 filters available, and they can be grouped in 4 categories.

Filters for log files:

- text_match
- file_name_match

Filters for test and results. If `testresult_name_match != ".*"` it will match
only testresults, otherwise, `testresult_name_match == ".*"` will match both
testresults and all other objects. An empty `testresult_name_match` is equivalent to `.*`.

- test_name_match
- testresult_name_match

Filter for build:

- architecture_match
- kpet_tree_name_match
- package_name_match: **This is the single filter using exact match**

Filter for checkout:

- tree_match

## Configuration

| Environment variable                 | Secret | Required | Description                                                                                                |
|--------------------------------------|--------|----------|------------------------------------------------------------------------------------------------------------|
| `DATAWAREHOUSE_URL`                  | no     | yes      | URL of Datawarehouse.                                                                                      |
| `DATAWAREHOUSE_TOKEN_TRIAGER`        | yes    | yes      | Token for Datawarehouse authentication.                                                                    |
| `RABBITMQ_HOST`                      | no     | no       | AMQP host                                                                                                  |
| `RABBITMQ_PORT`                      | no     | no       | AMQP port, TLS is used for port 443                                                                        |
| `RABBITMQ_VIRTUAL_HOST`              | no     | no       | AMQP vhost, defaults to /                                                                                  |
| `RABBITMQ_USER`                      | no     | no       | AMQP user                                                                                                  |
| `RABBITMQ_PASSWORD`                  | yes    | no       | AMQP password                                                                                              |
| `RABBITMQ_CAFILE`                    | no     | no       | AMQP CA file path                                                                                          |
| `WEBHOOK_RECEIVER_EXCHANGE`          | no     | no       | AMQP exchange to receive messages                                                                          |
| `DATAWAREHOUSE_TRIAGER_QUEUE`        | no     | no       | AMQP queue name to use on the exchange                                                                     |
| `DATAWAREHOUSE_TRIAGER_ROUTING_KEYS` | no     | no       | AMQP routing keys for the messages sent to the queue                                                       |
| `CKI_DEPLOYMENT_ENVIRONMENT`         | no     | no       | Define the deployment environment (production/staging)                                                     |
| `CKI_LOGGING_LEVEL`                  | no     | no       | logging level for CKI modules, defaults to WARN; to get meaningful output on the command line, set to INFO |
| `CKI_METRICS_ENABLED`                | no     | no       | Enable prometheus metrics. Default: false                                                                  |
| `CKI_METRICS_PORT`                   | no     | no       | Port where prometheus metrics are exposed. Default: 8000                                                   |
