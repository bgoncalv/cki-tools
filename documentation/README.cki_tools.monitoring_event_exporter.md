---
title: cki_tools.monitoring_event_exporter
linkTitle: monitoring_event_exporter
description: Kubernetes event exporter
---

Listen for [Kubernetes Events] in the namespace and print them to stdout.

This service is intended to run together with some other service such as
[Promtail] to capture the output and forward it to a persistent storage.

## Configuration

A valid Kubernetes context from [service account credentials] is required.

No extra configuration is necessary.

[Kubernetes Events]: https://kubernetes.io/docs/reference/kubernetes-api/cluster-resources/event-v1/
[Promtail]: https://grafana.com/docs/loki/latest/clients/promtail/
[service account credentials]: https://kubernetes.io/docs/tasks/run-application/access-api-from-pod/
