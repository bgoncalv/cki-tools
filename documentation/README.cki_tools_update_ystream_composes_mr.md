---
title: cki_tools_update_ystream_composes_mr.sh
description: Create MRs for updated ystream composes
aliases: [/l/update-ystream-composes-mr-docs]
---

```shell
cki_tools_update_ystream_composes_mr.sh
```

The underlying functionality is implemented in `cki_tools.update_ystream_composes`.

## Environment variables

Next to the variables for the [Python module][update_ystream_composes], the
following variables need to be defined:

| Field                | Type   | Required | Description                                        |
|----------------------|--------|----------|----------------------------------------------------|
| `GITLAB_PROJECT_URL` | url    | yes      | Git repo URL without protocol                      |
| `GITLAB_FORK_URL`    | url    | yes      | URL of the fork of the Git repo without protocol   |
| `GITLAB_TOKEN`       | string | yes      | GitLab private token with access to the Git repo   |
| `BRANCH_NAME`        | string | yes      | Branch name in the fork of the git repo            |
| `SOURCE_BRANCH_NAME` | string | no       | Source branch name to checkout, defaults to `main` |
| `GIT_USER_NAME`      | string | yes      | Git user name                                      |
| `GIT_USER_EMAIL`     | string | yes      | Git user email                                     |

[update_ystream_composes]: README.cki_tools.update_ystream_composes.md
