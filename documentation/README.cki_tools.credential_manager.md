---
title: cki_tools.credential_manager
linkTitle: credential_manager
description: Manage CKI service account secrets across services
aliases: [/l/credential-manager-docs]
---

Manage credentials for service accounts and their meta data as stored in CKI secrets.

## Life cycle

In general, tokens that can expire (`rotatable`) have multiple versions that go
through the following life-cycle:

| description                                        | `active` | `deployed` |
|----------------------------------------------------|----------|------------|
| created and deployed                               | `true`   | `true`     |
| superseded and no longer deployed, but still valid | `true`   | `false`    |
| revoked and no longer valid                        | `false`  | `false`    |

For rotatable tokens, the `validate` commands check the following invariants
about the versions of a token:

- all versions have `active` and `deployed` fields
- at least two versions are marked as `active`
- only one version is marked as `deployed`

## Metrics

Usage:

```bash
python -m cki_tools.credential_manager metrics
```

This will output the following Prometheus metrics related to the stored credentials:

| name                   | labels                       | description                |
|------------------------|------------------------------|----------------------------|
| `cki_token_created_at` | `name`, `active`, `deployed` | token creation timestamp   |
| `cki_token_expires_at` | `name`, `active`, `deployed` | token expiration timestamp |

## GitLab

For various kinds of GitLab tokens, the tool supports

- creation (`create`)

  ```bash
  python -m cki_tools.credential_manager gitlab create --token TOKEN_SECRET_NAME
  ```

- destruction (`destroy`)

  ```bash
  python -m cki_tools.credential_manager gitlab destroy --token TOKEN_SECRET_NAME
  ```

- rotation (`rotate`)

  ```bash
  python -m cki_tools.credential_manager gitlab rotate
                          [--token TOKEN] [--dry-run] [--force]

  options:
    --token TOKEN         Only rotate a single token
    --dry-run             Do not modify secrets or create tokens
    --force               Force token rotation even if new enough
  ```

- meta data update (`update`)

  ```bash
  python -m cki_tools.credential_manager gitlab update
  ```

- validation of deployed tokens (`validate`)

  ```bash
  python -m cki_tools.credential_manager gitlab validate
  ```

### Project access tokens

These tokens are rotatable.

See the [API description][project-api] for details.

| name           | create   | destroy  | rotate   | update   | validate | description                          |
|----------------|----------|----------|----------|----------|----------|--------------------------------------|
| (secret)       | updated  |          | updated  |          | required | secret token                         |
| `token_type`   | required | required | required | required | required | `gitlab_project_token`               |
| `project_url`  | required | required | required | required | required | Project URL                          |
| `scopes`       | required |          | updated  | updated  |          | Access scope                         |
| `access_level` | required |          | updated  | updated  |          | Access levels                        |
| `token_name`   | required |          | updated  | updated  |          | Name of the token                    |
| `token_id`     | updated  | required | required | required |          | Project access token ID              |
| `created_at`   | updated  |          | updated  | updated  |          | ISO8601 timestamp of creation        |
| `expires_at`   | updated  |          | updated  | updated  |          | ISO8601 expiry date                  |
| `revoked`      | updated  |          | updated  | updated  |          | Whether the token is already revoked |
| `active`       | updated  | updated  | updated  | updated  | required | Whether the token is still active    |
| `user_id`      | updated  |          | updated  | updated  |          | ID of associated user                |
| `user_name`    | updated  |          | updated  | updated  |          | Name of associated user              |
| `deployed`     | updated  | required | updated  |          | required | Whether the token is actually used   |

### Group access tokens

These tokens are rotatable.

See the [API description][group-api] for details.

| name           | create   | destroy  | rotate   | update   | validate | description                          |
|----------------|----------|----------|----------|----------|----------|--------------------------------------|
| (secret)       | updated  |          | updated  |          | required | secret token                         |
| `token_type`   | required | required | required | required | required | `gitlab_group_token`                 |
| `group_url`    | required | required | required | required | required | Group URL                            |
| `scopes`       | required |          | updated  | updated  |          | Access scope                         |
| `access_level` | required |          | updated  | updated  |          | Access levels                        |
| `token_name`   | required |          | updated  | updated  |          | Name of the token                    |
| `token_id`     | updated  | required | required | required |          | Group access token ID                |
| `created_at`   | updated  |          | updated  | updated  |          | ISO8601 timestamp of creation        |
| `expires_at`   | updated  |          | updated  | updated  |          | ISO8601 expiry date                  |
| `revoked`      | updated  |          | updated  | updated  |          | Whether the token is already revoked |
| `active`       | updated  | updated  | updated  | updated  | required | Whether the token is still active    |
| `user_id`      | updated  |          | updated  | updated  |          | ID of associated user                |
| `user_name`    | updated  |          | updated  | updated  |          | Name of associated user              |
| `deployed`     | updated  | required | updated  |          | required | Whether the token is actually used   |

### Personal access tokens

These tokens are rotatable.

See the [API description][personal-api] for details.

Token creation is not supported.

| name           | destroy  | rotate   | update   | validate | description                          |
|----------------|----------|----------|----------|----------|--------------------------------------|
| (secret)       | required | updated  |          | required | secret token                         |
| `token_type`   | required | required | required | required | `gitlab_personal_token`              |
| `instance_url` | required | required | required | required | GitLab instance URL                  |
| `scopes`       |          | updated  | updated  |          | Access scope                         |
| `token_name`   |          | updated  | updated  |          | Name of the token                    |
| `token_id`     |          | required | required |          | Access token ID                      |
| `created_at`   |          | updated  | updated  |          | ISO8601 timestamp of creation        |
| `expires_at`   |          | updated  | updated  |          | ISO8601 expiry date                  |
| `revoked`      |          | updated  | updated  |          | Whether the token is already revoked |
| `active`       | updated  | updated  | updated  | required | Whether the token is still active    |
| `user_id`      |          | required | required |          | ID of associated user                |
| `user_name`    |          | updated  | updated  |          | Name of associated user              |
| `deployed`     | required | updated  |          | required | Whether the token is actually used   |

### Project deploy tokens

See the [API description][deploy-api] for details.

Token rotation is not supported.

| name          | create   | destroy  | update   | validate | description                          |
|---------------|----------|----------|----------|----------|--------------------------------------|
| (secret)      | updated  |          |          |          | secret token                         |
| `token_type`  | required | required | required | required | `gitlab_project_deploy_token`        |
| `project_url` | required | required | required | required | Project URL                          |
| `scopes`      | required |          | updated  |          | Access scope                         |
| `token_name`  | required |          | updated  |          | Name of the token                    |
| `token_id`    | updated  | required | required | required | Project deploy token ID              |
| `created_at`  | updated  |          |          |          | ISO8601 timestamp of creation        |
| `expires_at`  | optional |          | updated  |          | ISO8601 expiry date                  |
| `revoked`     | updated  |          | updated  |          | Whether the token is already revoked |
| `active`      | updated  | updated  | updated  |          | Whether the token is still active    |
| `user_name`   | updated  |          | updated  |          | Associated user name                 |
| `deployed`    | updated  | required |          |          | Whether the token is actually used   |

### Group deploy tokens

See the [API description][deploy-api] for details.

Token rotation is not supported.

| name         | create   | destroy  | update   | validate | description                          |
|--------------|----------|----------|----------|----------|--------------------------------------|
| (secret)     | updated  |          |          |          | secret token                         |
| `token_type` | required | required | required | required | `gitlab_group_deploy_token`          |
| `group_url`  | required | required | required | required | Group URL                            |
| `scopes`     | required |          | updated  |          | Access scope                         |
| `token_name` | required |          | updated  |          | Name of the token                    |
| `token_id`   | updated  | required | required | required | Group deploy token ID                |
| `created_at` | updated  |          |          |          | ISO8601 timestamp of creation        |
| `expires_at` | optional |          | updated  |          | ISO8601 expiry date                  |
| `revoked`    | updated  |          | updated  |          | Whether the token is already revoked |
| `active`     | updated  | updated  | updated  |          | Whether the token is still active    |
| `user_name`  | updated  |          | updated  |          | Associated user name                 |
| `deployed`   | updated  | required |          |          | Whether the token is actually used   |

### Runner authentication tokens

See the [API description][runner-api] for details.

Token creation and rotation is not supported.

| name           | update   | validate | description                          |
|----------------|----------|----------|--------------------------------------|
| (secret)       | required | required | secret token                         |
| `token_type`   | required | required | `gitlab_runner_authentication_token` |
| `instance_url` | required | required | GitLab instance URL                  |
| `token_id`     | updated  |          | Group token ID                       |
| `expires_at`   | updated  |          | ISO8601 expiry date (optional)       |
| `active`       | updated  |          | Whether the token is still active    |
| `deployed`     |          |          | Whether the token is actually used   |

## LDAP

For LDAP keytabs and passwords, the tool supports

- update (`update`)

  ```bash
  python -m cki_tools.credential_manager ldap update [--token TOKEN]

  options:
    --token TOKEN         Only update a single token
  ```

- validation (`validate`)

  ```bash
  python -m cki_tools.credential_manager ldap validate [--token TOKEN]

  options:
    --token TOKEN         Only validate a single token
  ```

### LDAP keytabs

Keytab validation is not supported.

| name              | update   | description             |
|-------------------|----------|-------------------------|
| (secret)          |          | keytab, base64-encoded  |
| `token_type`      | required | `ldap_keytab`           |
| `ldap_server`     | required | LDAP server             |
| `dn`              | required | LDAP distinguished name |
| (LDAP attributes) | updated  | LDAP object attributes  |

### LDAP passwords

| name              | update   | validate | description             |
|-------------------|----------|----------|-------------------------|
| (secret)          |          | required | password                |
| `token_type`      | required | required | `ldap_password`         |
| `ldap_server`     | required | required | LDAP server             |
| `dn`              | required | required | LDAP distinguished name |
| (LDAP attributes) | updated  |          | LDAP object attributes  |

## AWS

For AWS secret access keys, the tool supports

- update (`update`)

  ```bash
  python -m cki_tools.credential_manager aws update --token TOKEN_SECRET_NAME

  options:
    --token TOKEN         Only update a single token
  ```

| name            | update   | description                        |
|-----------------|----------|------------------------------------|
| (secret)        |          | secret access key                  |
| `token_type`    | required | `aws_secret_access_key`            |
| `access_key_id` | required | access key ID                      |
| `endpoint_url`  | required | endpoint URL if not AWS            |
| `account`       | updated  | AWS account number                 |
| `user_name`     | updated  | service account user name          |
| `arn`           | updated  | service account user ARN           |
| `created_at`    | updated  | ISO8601 timestamp of creation      |
| `active`        | updated  | Whether the token is still active  |
| `deployed`      |          | Whether the token is actually used |

## Dogtag

For Dogtag certificates, the tool supports

- creation (`create`)

  ```bash
  python -m cki_tools.credential_manager dogtag create --token TOKEN_SECRET_NAME
  ```

- rotation (`rotate`)

  ```bash
  python -m cki_tools.credential_manager dogtag rotate
                          [--token TOKEN] [--dry-run] [--force]

  options:
    --token TOKEN         Only operate on a single token
    --dry-run             Do not modify secrets or create tokens
    --force               Force token rotation even if new enough
  ```

- update (`update`)

  ```bash
  python -m cki_tools.credential_manager dogtag update [--token TOKEN]

  options:
    --token TOKEN         Only operate on a single token
  ```

- validation of deployed tokens (`validate`)

  ```bash
  python -m cki_tools.credential_manager dogtag validate [--token TOKEN]

  options:
    --token TOKEN         Only operate on a single token
  ```

| name                 | create   | rotate   | update   | validate | description                              |
|----------------------|----------|----------|----------|----------|------------------------------------------|
| (secret):private_key | updated  | updated  |          | required | secret key                               |
| (secret):certificate | updated  | updated  | updated  | required | certificate                              |
| `token_type`         | required | required | required | required | `dogtag_certificate`                     |
| `server_url`         | required | required | required |          | Dogtag server URL                        |
| `serial_number`      | updated  | updated  | required |          | Certificate serial number                |
| `issuer_dn`          | updated  | updated  | updated  |          | Issuer DN                                |
| `subject_dn`         | required | required | updated  |          | Subject DN                               |
| `created_at`         | updated  | updated  | updated  |          | ISO8601 timestamp of creation            |
| `expires_at`         | updated  | updated  | updated  |          | ISO8601 timestamp of expiry              |
| `active`             | updated  | updated  | updated  |          | Whether the certificate is still valid   |
| `deployed`           | updated  | updated  |          |          | Whether the certificate is actually used |

## Splunk

For Splunk HEC tokens, the tool supports

- validation (`validate`)

  ```bash
  python -m cki_tools.credential_manager splunk validate [--token TOKEN]

  options:
    --token TOKEN         Only validate a single token
  ```

| name           | validate | description                  |
|----------------|----------|------------------------------|
| (secret)       | required | HEC token                    |
| `token_type`   | required | `splunk_hec_token`           |
| `endpoint_url` | required | Splunk HEC endpoint base URL |
| `index`        | optional | Splunk index                 |

## SSH keys

For SSH keys, the tool supports

- creation (`create`)

  ```bash
  python -m cki_tools.credential_manager ssh create --token TOKEN_SECRET_NAME
  ```

- rotation (`rotate`)

  ```bash
  python -m cki_tools.credential_manager ssh rotate
                          [--token TOKEN] [--dry-run] [--force]

  options:
    --token TOKEN         Only operate on a single token
    --dry-run             Do not modify secrets or create tokens
    --force               Force token rotation even if new enough
  ```

- update (`update`)

  ```bash
  python -m cki_tools.credential_manager ssh update [--token TOKEN]

  options:
    --token TOKEN         Only operate on a single token
  ```

- validation of deployed SSH keys (`validate`)

  ```bash
  python -m cki_tools.credential_manager ssh validate [--token TOKEN]

  options:
    --token TOKEN         Only operate on a single token
  ```

| name                 | create   | rotate   | update   | validate | description                      |
|----------------------|----------|----------|----------|----------|----------------------------------|
| (secret):private_key | updated  | updated  |          | required | secret key                       |
| (secret):public_key  | updated  | updated  | updated  | required | certificate                      |
| `token_type`         | required | required | required | required | `ssh_private_key`                |
| `comment`            | required | required | required |          | Public key comment (name)        |
| `key_size`           | required | required | updated  |          | RSA key size                     |
| `created_at`         | updated  | updated  | updated  |          | ISO8601 timestamp of creation    |
| `active`             | updated  | updated  |          | required | Whether the key is still valid   |
| `deployed`           | updated  | updated  |          | required | Whether the key is actually used |

## Generic passwords

For generic passwords created via [diceware], the tool supports

- creation (`create`)

  ```bash
  python -m cki_tools.credential_manager password create --token TOKEN_SECRET_NAME
  ```

- rotation (`rotate`)

  ```bash
  python -m cki_tools.credential_manager password rotate
                          [--token TOKEN] [--dry-run] [--force]

  options:
    --token TOKEN         Only operate on a single token
    --dry-run             Do not modify secrets or create tokens
    --force               Force token rotation even if new enough
  ```

- validation (`validate`)

  ```bash
  python -m cki_tools.credential_manager password validate [--token TOKEN]

  options:
    --token TOKEN         Only operate on a single token
  ```

| name         | create   | rotate   | update   | validate | description                      |
|--------------|----------|----------|----------|----------|----------------------------------|
| (secret)     | updated  | updated  |          | required | password                         |
| `token_type` | required | required | required | required | `password`                       |
| `created_at` | updated  | updated  | updated  |          | ISO8601 timestamp of creation    |
| `active`     | updated  | updated  |          | required | Whether the key is still valid   |
| `deployed`   | updated  | updated  |          | required | Whether the key is actually used |

## Configuration via environment variables

| Name                | Secret | Required | Description                                                                                                |
|---------------------|--------|----------|------------------------------------------------------------------------------------------------------------|
| `GITLAB_TOKENS`     | no     | yes      | URL/environment variable pairs of GitLab instances and private tokens                                      |
| `GITLAB_TOKEN`      | yes    | yes      | GitLab private tokens as configured in `GITLAB_TOKENS` above                                               |
| `CKI_LOGGING_LEVEL` | no     | no       | logging level for CKI modules, defaults to WARN; to get meaningful output on the command line, set to INFO |

[diceware]: https://github.com/ulif/diceware
[project-api]: https://docs.gitlab.com/ee/api/project_access_tokens.html
[group-api]: https://docs.gitlab.com/ee/api/group_access_tokens.html
[personal-api]: https://docs.gitlab.com/ee/api/personal_access_tokens.html
[deploy-api]: https://docs.gitlab.com/ee/api/deploy_tokens.html
[runner-api]: https://docs.gitlab.com/ee/api/runners.html
