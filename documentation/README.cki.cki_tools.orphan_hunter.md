---
title: cki.cki_tools.orphan_hunter
linkTitle: orphan_hunter
description: Delete Pods spawned by gitlab-runner of already finished jobs
---

Check whether the GitLab jobs for Pods spawned by `gitlab-runner` are still
running. Pods of already finished jobs are deleted.

## Environment variables

| Name                | Secret | Required | Description                                                           |
|---------------------|--------|----------|-----------------------------------------------------------------------|
| `GITLAB_TOKENS`     | no     | yes      | URL/environment variable pairs of GitLab instances and private tokens |
| `GITLAB_TOKEN`      | yes    | yes      | GitLab private tokens as configured in `gitlab_tokens` above          |
| `CHATBOT_URL`       | no     | no       | chat bot endpoint                                                     |
| `ACTION`            | no     | no       | `report` (default) or `delete` Pods                                   |

The orphan hunter service account need permissions equivalent to the following Role:

```yaml
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: orphan-hunter
rules:
  - apiGroups: [""]
    resources: [pods]
    verbs: [get, list, delete]
```
